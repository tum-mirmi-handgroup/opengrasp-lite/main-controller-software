/**
* \file sam5_dc_motor_controller.c
*
* sam5_dc_motor_controller for controlling BLDC motors with microcontrollers of the samd5/e5 family
* Copyright (C) 2022  Michael Ratzel

* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.

* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.

* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "sam5_feedback_motor_impl.h"
#include "sam5_bmp3_wrapper.h"
#include <utils.h>
#include <atmel_start.h>

static uint32_t loop_count = 0;
static uint32_t vibration_end = 0;
static bool last_iteration_contact = false;

void initialize_feedback_motor()
{
	// Output
	///* Initialize pins
	/// Configure feedback motor pin as GPIO output pin with initial level LOW and no special function
	gpio_set_pin_level(FEEDBACK_MOTOR_PIN, false);
	gpio_set_pin_direction(FEEDBACK_MOTOR_PIN, GPIO_DIRECTION_OUT);
	gpio_set_pin_function(FEEDBACK_MOTOR_PIN, GPIO_PIN_FUNCTION_OFF);
	
	///* Enable APB SERCOM units APB communication
	hri_mclk_set_APBCMASK_TC5_bit(MCLK);
	hri_gclk_write_PCHCTRL_reg(GCLK, TC5_GCLK_ID, GCLK_PCHCTRL_GEN_GCLK0_Val | (1 << GCLK_PCHCTRL_CHEN_Pos));
	
	// Check if no software reset is ongoing
	if(!hri_tc_is_syncing(TC5, TC_SYNCBUSY_SWRST)){
		// If TC5 is enable -> disable it
		if(hri_tc_get_CTRLA_ENABLE_bit(TC5)){
			hri_tc_clear_CTRLA_ENABLE_bit(TC5);
		}
		// Start software reset
		hri_tc_set_CTRLA_SWRST_bit(TC5);
	}
	// Wait for software to finish
	hri_tc_wait_for_sync(TC5, TC_SYNCBUSY_SWRST);
	
	hri_tc_write_CTRLA_reg(TC5,
	0 << TC_CTRLA_CAPTEN0_Pos
	| 0 << TC_CTRLA_CAPTEN1_Pos
	| 0x5 << TC_CTRLA_PRESCALER_Pos
	| 0x0 << TC_CTRLA_MODE_Pos);
	
	hri_tc_write_CTRLB_reg(TC5,
	0 << TC_CTRLBSET_ONESHOT_Pos
	| 0 << TC_CTRLBSET_DIR_Pos);
	
	hri_tc_write_INTEN_reg(TC5,
	0 << TC_INTENSET_MC0_Pos
	| 0 << TC_INTENSET_MC1_Pos
	| 1 << TC_INTENSET_ERR_Pos
	| 1 << TC_INTENSET_OVF_Pos);
	
	hri_tc_write_WAVE_reg(TC5,
	0x1 << TC_WAVE_WAVEGEN_Pos);
	
	// Prescaler of 64, CPU frequency of 100 MHz -> 50 Hz frequency
	hri_tccount16_write_CC_CC_bf(TC5, 0, CONF_CPU_FREQUENCY / 6400);
	
	NVIC_SetPriority(TC5_IRQn, NVIC_PRIORITY_FEEDBACK_MOTOR_TC);
	
	NVIC_DisableIRQ(TC5_IRQn);
	NVIC_ClearPendingIRQ(TC5_IRQn);
	NVIC_EnableIRQ(TC5_IRQn);
	hri_tc_set_CTRLA_ENABLE_bit(TC5);
}

void vibrate_motor(uint32_t vibrate_duration_20ms)
{
	gpio_set_pin_level(FEEDBACK_MOTOR_PIN, true);
	vibration_end = loop_count + vibrate_duration_20ms;
}

void TC5_Handler(){
	bool contact = false;
	for(uint8_t i = 0; i < SAM5_BMP3_MAX_NUMBER_SENSORS; ++i){
		if(sensor_array[i].available && sensor_array[i].force_newton > SAM5_BMP3_FORCE_CONTACT){
			contact = true;
			break;
		}
	}
	
	gpio_set_pin_level(FEEDBACK_MOTOR_PIN, false);	
	if(loop_count >= vibration_end){		
		gpio_set_pin_level(FEEDBACK_MOTOR_PIN, false);
	}else{
		if(contact && !last_iteration_contact){
			vibrate_motor(20); // 50 Hz frequency -> roughly 400 ms
		}
	}
	
	last_iteration_contact = contact;
	++loop_count;
	uint8_t flags = hri_tc_get_INTFLAG_reg(TC5, TC_INTFLAG_MASK);
	hri_tc_clear_INTFLAG_reg(TC5, flags);
}