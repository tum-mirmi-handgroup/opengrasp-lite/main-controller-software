/*
* sam5_ws2812.c
*
* Created: 11.01.2023
*  Author: Michael Ratzel
*/
#include "sam5_ws2812.h"

#include <peripheral_clk_config.h>

// Either Oscilloscope is off by exactly factor of two or something else is kinda fishy -> With this hack the right timing is achieved
//
// Reading from the inspiration (https://github.com/cpldcpu/light_ws2812/blob/master/light_ws2812_ARM/light_ws2812_cortex.h)
// This might be due, to active memory waitstates
#define F_CPU CONF_CPU_FREQUENCY / 2

// Target period of 1,25 us, with high time for 0 bit of 0.375 us, and high time for 1 bit of 1,25 us - 0,65 us
#define ws2812_ctot	(((F_CPU/1000)*1250)/1000000)
#define ws2812_t1	(((F_CPU/1000)*375 )/1000000)		// floor
#define ws2812_t2	(((F_CPU/1000)*650+500000)/1000000)	// ceil

#define w1 (ws2812_t1-2)				// Calculate cycles to wait 1
#define w2 (ws2812_t2-ws2812_t1-2)		// Calculate cycles to wait 2
#define w3 (ws2812_ctot-ws2812_t2-5)	// Calculate cycles to wait 3
// The program will behave like that:
// Writing 1:
//		HIGH |--------------------|              |
//		LOW  |                    |--------------|
//		     |       w1 + w2      |     w3       |
// Writing 0:
//		HIGH |---------------|                   |
//		LOW  |               |-------------------|
//		     |       w1      |       w2 + w3     |

// Macros to define wait times for 1, 2, 4, 8, 16, 32, 64 cycles respectively
#define ws2812_DEL1 "	nop		\n\t"
#define ws2812_DEL2 "	b	.+2	\n\t"
#define ws2812_DEL4 ws2812_DEL2 ws2812_DEL2
#define ws2812_DEL8 ws2812_DEL4 ws2812_DEL4
#define ws2812_DEL16 ws2812_DEL8 ws2812_DEL8
#define ws2812_DEL32 ws2812_DEL16 ws2812_DEL16
#define ws2812_DEL64 ws2812_DEL32 ws2812_DEL32


void sam5_ws2812_init(uint32_t pin)
{
	gpio_set_pin_level(pin, false);
	gpio_set_pin_direction(pin, GPIO_DIRECTION_OUT);
	gpio_set_pin_function(pin, GPIO_PIN_FUNCTION_OFF);
}

void sam5_ws2812_sendarray(const uint8_t *data, int datlen, uint32_t pin)
{
	uint32_t mask = 1U << GPIO_PIN(pin);
	volatile uint32_t *set = &(((Port*) PORT)->Group[GPIO_PORT(pin)].OUTSET.reg);
	volatile uint32_t *clr = &(((Port*) PORT)->Group[GPIO_PORT(pin)].OUTCLR.reg);
	uint32_t i = 0;
	uint32_t curbyte;

	CRITICAL_SECTION_ENTER();
	while (datlen--) {
		curbyte=*data++;
		asm volatile(
		"		lsl %[dat],#24				\n\t"	// Left shift data by 24 (8 bits in uint32_t)
		"		movs %[ctr],#8				\n\t"	// Write 8 in ctr
		"ilop%=:							\n\t"	// Set goto marker ilop
		"		lsls %[dat], #1				\n\t"	// Left shif data by one (Store bit shifted out in condition flag C)
		"		str %[mask], [%[set]]		\n\t"	// Store mask in (*set)
		#if (w1&1)
		ws2812_DEL1		// wait 1 cycle
		#endif
		#if (w1&2)
		ws2812_DEL2		// wait 2 cylces
		#endif
		#if (w1&4)
		ws2812_DEL4		// wait 4 cylces
		#endif
		#if (w1&8)
		ws2812_DEL8		// wait 8 cylces
		#endif
		#if (w1&16)
		ws2812_DEL16	// wait 16 cylces
		#endif
		#if (w1&32)
		ws2812_DEL32	// wait 32 cylces
		#endif
		#if (w1&64)
		ws2812_DEL64	// wait 64 cylces
		#endif
		"		bcs one%=					\n\t"	// If flag C is 1 jump to Goto marker one
		"		str %[mask], [%[clr]]		\n\t"	// store mask in (*clr) (Only reached if previous command does not jump to one marker
		"one%=:								\n\t"	// Set goto marker one
		#if (w2&1)
		ws2812_DEL1		// wait 1 cylce
		#endif
		#if (w2&2)
		ws2812_DEL2		// wait 2 cylces
		#endif
		#if (w2&4)
		ws2812_DEL4		// wait 4 cylces
		#endif
		#if (w2&8)
		ws2812_DEL8		// wait 8 cylces
		#endif
		#if (w2&16)
		ws2812_DEL16	// wait 16 cylces
		#endif
		#if (w2&32)
		ws2812_DEL32	// wait 32 cylces
		#endif
		#if (w2&64)
		ws2812_DEL64	// wait 64 cylces
		#endif
		"		subs %[ctr], #1				\n\t"	// Subtract 1 from ctr (If ctr is 0 set Z flag to 1)
		"		str %[mask], [%[clr]]		\n\t"	// store mask in (*clr)
		#if (w3&1)
		ws2812_DEL1		// wait 1 cylce
		#endif
		#if (w3&2)
		ws2812_DEL2		// wait 2 cylces
		#endif
		#if (w3&4)
		ws2812_DEL4		// wait 4 cylces
		#endif
		#if (w3&8)
		ws2812_DEL8		// wait 8 cylces
		#endif
		#if (w3&16)
		ws2812_DEL16	// wait 16 cylces
		#endif
		#if (w3&32)
		ws2812_DEL32	// wait 32 cylces
		#endif
		#if (w3&64)
		ws2812_DEL64	// wait 64 cylces
		#endif
		"		beq	end%=					\n\t"	// If Z flag is 1 jump to Goto marker end
		"		b 	ilop%=					\n\t"	// Jump to Goto marker ilop
		"end%=:								\n\t"	// Set Goto marker end
		:	[ctr] "+r" (i)																// Iteration variable
		:	[dat] "r" (curbyte), [set] "r" (set), [clr] "r" (clr), [mask] "r" (mask)	// Map local variables to registers: curbyte -> data, set -> set, clr -> clr, mask -> mask
		);
	}
	CRITICAL_SECTION_LEAVE();
}