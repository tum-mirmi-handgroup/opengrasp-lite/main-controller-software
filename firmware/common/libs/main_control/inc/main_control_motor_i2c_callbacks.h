#ifndef MAIN_CONTROL_MOTOR_I2C_CALLBACKS_H_
#define MAIN_CONTROL_MOTOR_I2C_CALLBACKS_H_

#include "sam5_i2c_master.h"

void main_control_motor_i2c_callback_simple_success(bool success, const sam5_i2c_master_com_job_t* job);
void main_control_motor_i2c_callback_activate_motor(bool success, const sam5_i2c_master_com_job_t* job);
void main_control_motor_i2c_callback_read_combined_state_flags(bool success, const sam5_i2c_master_com_job_t* job);
void main_control_motor_i2c_callback_read_combined_state(bool success, const sam5_i2c_master_com_job_t* job);
void main_control_motor_i2c_callback_read_state(bool success, const sam5_i2c_master_com_job_t* job);
void main_control_motor_i2c_callback_read_limits(bool success, const sam5_i2c_master_com_job_t* job);

#endif // MAIN_CONTROL_MOTOR_I2C_CALLBACKS_H_