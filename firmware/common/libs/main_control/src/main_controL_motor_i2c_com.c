#include "main_control_impl.h"
#include "motor_protocol.h"
#include "job_system.h"

#include <string.h>
#include <stdio.h>

static void motor_failed();

void motor_i2c_send_control_mode(const main_control_motor_t* motor, uint8_t control_mode, uint8_t stop_at_contact, uint8_t use_aggressive_contact_detection){
	dc_motor_controller_input_flags_t flags = {
		.control_mode = control_mode,
		.continue_at_end = 0,
		.stop_at_contact = stop_at_contact,
		.use_endstop_for_end = MAIN_CONTROL_USE_ENDSTOP_FOR_END,
		.use_endstop_for_refine = MAIN_CONTROL_USE_ENDSTOP_FOR_REFINE,
		.use_aggressive_contact_detection = use_aggressive_contact_detection,
	};
	sam5_i2c_master_com_job_t job = {
		.command = MOTOR_PROTOCOL_BUILD_COMMAND(motor->motor_index, MOTOR_PROTOCOL_WRITE_INPUT_FLAGS),
		.slave_address = motor->i2c_address,
		.direction = I2C_MASTER_DIR_MASTER_TO_SLAVE,
		.num_bytes_to_transfer = 1,
		.callback = control_mode == MOTOR_CM_STOPPED ?  main_control_motor_i2c_callback_simple_success : main_control_motor_i2c_callback_activate_motor,
		.period_ms = 0,
	};
	job.buffer[0] = *((uint8_t*) &flags);
	sam5_i2c_master_register_com_job(&job);
}

void motor_i2c_send_control_mode_stop(const main_control_motor_t* motor){
	motor_i2c_send_control_mode(motor, MOTOR_CM_STOPPED, 0, 0);
}

void motor_i2c_send_closing_current_setpoint(const main_control_motor_t* motor, float32_t setpoint){
	float32_t buf = motor->open_at_positive == 1 ? -setpoint : setpoint;
	sam5_i2c_master_com_job_t job = {
		.command = MOTOR_PROTOCOL_BUILD_COMMAND(motor->motor_index, MOTOR_PROTOCOL_WRITE_INPUT_CURRENT),
		.slave_address = motor->i2c_address,
		.direction = I2C_MASTER_DIR_MASTER_TO_SLAVE,
		.num_bytes_to_transfer = sizeof(float32_t),
		.callback = main_control_motor_i2c_callback_simple_success,
		.period_ms = 0
	};
	memcpy((uint8_t*) job.buffer, &buf, sizeof(buf));
	sam5_i2c_master_register_com_job(&job);
}

void motor_i2c_send_setpoint(const main_control_motor_t* motor, uint8_t command, float32_t setpoint){
	sam5_i2c_master_com_job_t job = {
		.command = MOTOR_PROTOCOL_BUILD_COMMAND(motor->motor_index, command),
		.slave_address = motor->i2c_address,
		.direction = I2C_MASTER_DIR_MASTER_TO_SLAVE,
		.num_bytes_to_transfer = sizeof(float32_t),
		.callback = main_control_motor_i2c_callback_simple_success,
		.period_ms = 0
	};
	memcpy((uint8_t*) job.buffer, &setpoint, sizeof(setpoint));
	sam5_i2c_master_register_com_job(&job);
}

void motor_i2c_send_complete_input(const main_control_motor_t* motor, dc_motor_controller_inputs_t* input){
	sam5_i2c_master_com_job_t job = {
		.command = MOTOR_PROTOCOL_BUILD_COMMAND(motor->motor_index, MOTOR_PROTOCOL_WRITE_INPUT),
		.slave_address = motor->i2c_address,
		.direction = I2C_MASTER_DIR_MASTER_TO_SLAVE,
		.num_bytes_to_transfer = sizeof(dc_motor_controller_inputs_t),
		.callback = input->flags.control_mode == MOTOR_CM_STOPPED ?  main_control_motor_i2c_callback_simple_success : main_control_motor_i2c_callback_activate_motor,
		.period_ms = 0
	};
	memcpy((uint8_t*) job.buffer, input, sizeof(dc_motor_controller_inputs_t));
	sam5_i2c_master_register_com_job(&job);
}


void main_control_motor_i2c_callback_simple_success(bool success, const sam5_i2c_master_com_job_t* job)
{
	if(main_control_control_flags.run_motor_controllers == 0){
		return;
	}
	if(!success){
		motor_failed();
		return;
	}
}

void main_control_motor_i2c_callback_activate_motor(bool success, const sam5_i2c_master_com_job_t* job)
{
	if(main_control_control_flags.run_motor_controllers == 0){
		return;
	}
	if(!success){
		motor_failed();
		return;
	}
	for(uint8_t i = 0; i < MAIN_CONTROL_NUM_MOTORS; ++i){
		main_control_motor_t* motor = &g_main_control.motors[i];
		if(job->slave_address == motor->i2c_address && MOTOR_PROTOCOL_GET_MOTR_INDEX(job->command) == motor->motor_index){
			motor->state.flags_a.motor_stopped = 0;
			motor->state.flags_a.target_angle_reached = 0;
			g_main_control.flags.all_stopped_or_reached = 0;
		}
	}
}

void main_control_motor_i2c_callback_read_combined_state_flags(bool success, const sam5_i2c_master_com_job_t* job)
{
	ASSERT(job->command == MOTOR_PROTOCOL_COMMON_READ_COMBINED_STATE_FLAGS);
	if(main_control_control_flags.run_motor_controllers == 0){
		return;
	}
	if(!success){
		motor_failed();
		return;
	}
	
	const dc_motor_controller_combined_state_flags_t* new_flags = (dc_motor_controller_combined_state_flags_t*) &job->buffer;
	const uint8_t motor_controller_index = job->slave_address - MOTOR_PROTOCOL_BASE_ADDRESS;
	ASSERT(motor_controller_index < MAIN_CONTROL_NUM_MOTOR_CONTROLLER);
	
	bool all_stopped_or_reached = true;
	
	for(uint8_t i = 0; i < MAIN_CONTROL_NUM_MOTORS; ++i){
		main_control_motor_t* const motor = &g_main_control.motors[i];
		if(motor->controller_index == motor_controller_index){
			motor->state.flags_a = new_flags->flags_a[motor->motor_index];
			motor->state.flags_b = new_flags->flags_b[motor->motor_index];
		}
		all_stopped_or_reached = all_stopped_or_reached && (motor->state.flags_a.motor_stopped || motor->state.flags_a.target_angle_reached);
	}
	if(g_main_control.flags.all_stopped_or_reached == false && all_stopped_or_reached == true){
		g_main_control.flags.all_stopped_or_reached = all_stopped_or_reached;
		main_control_dispatch(MAIN_CONTROL_EVT_SIG_ALL_STOPPED_OR_REACHED, &g_main_control);
	}
	else{
		g_main_control.flags.all_stopped_or_reached = all_stopped_or_reached;
	}
}

void main_control_motor_i2c_callback_read_combined_state(bool success, const sam5_i2c_master_com_job_t* job){
	ASSERT(job->command == MOTOR_PROTOCOL_COMMON_READ_COMBINED_STATE);
	if(main_control_control_flags.run_motor_controllers == 0){
		return;
	}
	if(!success){
		motor_failed();
		return;
	}
	const uint8_t motor_controller_index = job->slave_address - MOTOR_PROTOCOL_BASE_ADDRESS;
	const dc_motor_controller_combined_state_t* new_state = (const dc_motor_controller_combined_state_t*) job->buffer;
	for(uint8_t i = 0; i < MAIN_CONTROL_NUM_MOTORS; ++i){
		main_control_motor_t* const motor = &g_main_control.motors[i];
		if(motor->controller_index == motor_controller_index){
			motor->state.angle = new_state->angle[motor->motor_index];
			motor->state.current = new_state->current[motor->motor_index];
		}
	}
}

void main_control_motor_i2c_callback_read_state(bool success, const sam5_i2c_master_com_job_t* job)
{
	ASSERT(MOTOR_PROTOCOL_GET_COMMAND_TYPE(job->command) == MOTOR_PROTOCOL_READ_STATE);
	if(main_control_control_flags.run_motor_controllers == 0){
		return;
	}
	if(!success){
		motor_failed();
		return;
	}
	for(uint8_t i = 0; i < MAIN_CONTROL_NUM_MOTORS; ++i){
		main_control_motor_t* const motor = &g_main_control.motors[i];
		if(motor->i2c_address == job->slave_address && motor->motor_index == MOTOR_PROTOCOL_GET_MOTR_INDEX(job->command)){
			CRITICAL_SECTION_ENTER()
			memcpy(&motor->state, job->buffer, sizeof(dc_motor_controller_state_t));
			CRITICAL_SECTION_LEAVE()
			return;
		}
	}
}

void main_control_motor_i2c_callback_read_limits(bool success, const sam5_i2c_master_com_job_t* job)
{
	ASSERT(MOTOR_PROTOCOL_GET_COMMAND_TYPE(job->command) == MOTOR_PROTOCOL_READ_LIMITS);
	if(main_control_control_flags.run_motor_controllers == 0){
		return;
	}
	if(!success){
		motor_failed();
		return;
	}
	for(uint8_t i = 0; i < MAIN_CONTROL_NUM_MOTORS; ++i){
		main_control_motor_t* const motor = &g_main_control.motors[i];
		if(motor->i2c_address == job->slave_address && motor->motor_index == MOTOR_PROTOCOL_GET_MOTR_INDEX(job->command)){
			CRITICAL_SECTION_ENTER()
			memcpy(&motor->limits, job->buffer, sizeof(dc_motor_controller_limits_t));
			CRITICAL_SECTION_LEAVE()
			return;
		}
	}
}

static void motor_failed(){
	if(g_main_control.flags.accept_reset_requests){
		printf("I2C Error: Motor\r\n");
	}
	main_control_dispatch(MAIN_CONTROL_EVT_SIG_ERROR, &g_main_control);
	request_reset_motors(&g_main_control);
}
