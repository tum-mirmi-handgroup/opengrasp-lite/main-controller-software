#include "main_control_impl.h"
#include "motor_protocol.h"

#include <stdio.h>
#include <string.h>

static void activate_motor_controller(uint8_t reset_pin, uint8_t target_address);

static void change_address_success_callback(bool success, const uint8_t *data);
static void read_state_success_callback(bool success, const sam5_i2c_master_com_job_t * job);
static void read_limits_success_callback(bool success, const sam5_i2c_master_com_job_t * job);
static int8_t change_address_success = 0;
static int8_t read_success[MAIN_CONTROL_NUM_MOTORS];

void reset_motors_if_necessary(main_control_t* control){
	printf("Checking if Motor reset is necessary\r\n");
	main_control_control_flags.run_motor_controllers = 0;
	control->flags.all_stopped_or_reached = 0;
	memset(read_success, 0, sizeof(read_success));
	
	//####################################################################################################
	uint8_t success = 0;
	for(uint8_t try = 0; try < 3; ++try){
		for(uint8_t i = 0; i < MAIN_CONTROL_NUM_MOTORS; ++i){
			if(read_success[i] == 1){
				continue;
			}
			const sam5_i2c_master_com_job_t job = {
				.command = MOTOR_PROTOCOL_BUILD_COMMAND(control->motors[i].motor_index, MOTOR_PROTOCOL_READ_STATE),
				.slave_address = control->motors[i].i2c_address,
				.direction = I2C_MASTER_DIR_SLAVE_TO_MASTER,
				.num_bytes_to_transfer = sizeof(dc_motor_controller_state_t),
				.callback = &read_state_success_callback,
				.period_ms = 0
			};
			read_success[i] = 0;
			sam5_i2c_master_register_com_job(&job);
		}
		success = 1;
		for(uint8_t i = 0; i < MAIN_CONTROL_NUM_MOTORS; ++i){
			for(uint16_t t = 0; t < 50; ++t){
				if(read_success[i] != 0){
					break;
				}
				delay_ms(1);
			}
			success = success && read_success[i] == 1;
		}
	}
	for(uint8_t i = 0; i < MAIN_CONTROL_NUM_MOTORS; ++i){
		if(read_success[i] != 1){
			printf("State transmission with finger %d.%d %s\r\n", control->motors[i].controller_index + 1, control->motors[i].motor_index + 1, read_success[i] == -1 ? "failed" : "timed out");
			success = false;
		}
		else if(control->motors[i].state.flags_b.calib_stage != CALIB_STAGE_FINISHED){
			printf("Finger %d.%d did not finish calibration\r\n", control->motors[i].controller_index + 1, control->motors[i].motor_index + 1);
			success = false;
		}
	}
	
	
	//####################################################################################################
	memset(read_success, 0, sizeof(read_success));
	if(success){
		for(uint8_t try = 0; try < 3; ++try){
			for(uint8_t i = 0; i < MAIN_CONTROL_NUM_MOTORS; ++i){
				if(read_success[i] == 1){
					continue;
				}
				const sam5_i2c_master_com_job_t job = {
					.command = MOTOR_PROTOCOL_BUILD_COMMAND(control->motors[i].motor_index, MOTOR_PROTOCOL_READ_LIMITS),
					.slave_address = control->motors[i].i2c_address,
					.direction = I2C_MASTER_DIR_SLAVE_TO_MASTER,
					.num_bytes_to_transfer = sizeof(dc_motor_controller_limits_t),
					.callback = &read_limits_success_callback,
					.period_ms = 0
				};
				read_success[i] = 0;
				sam5_i2c_master_register_com_job(&job);
			}
			success = 1;
			for(uint8_t i = 0; i < MAIN_CONTROL_NUM_MOTORS; ++i){
				for(uint16_t t = 0; t < 50; ++t){
					if(read_success[i] != 0){
						break;
					}
					delay_ms(1);
				}
				success = success && read_success[i] == 1;
			}
		}
		for(uint8_t i = 0; i < MAIN_CONTROL_NUM_MOTORS; ++i){
			if(read_success[i] != 1){
				printf("Limits transmission with finger %d.%d %s\r\n", control->motors[i].controller_index + 1, control->motors[i].motor_index + 1, read_success[i] == -1 ? "failed" : "timed out");
				success = false;
			}
		}
	}
	
	//####################################################################################################
	if(success){
		printf("All Motors finished calibration and are working - Skip resetting\r\n");
		main_control_control_flags.run_motor_controllers = 1;
	}
	else{
		printf("Reset Necessary\r\n");
		reset_motors(control);
	}
}

void reset_motors(main_control_t* control){
	printf("Resetting Motors\r\n");
	main_control_control_flags.run_motor_controllers = 0;
	control->flags.doing_reset = 1;
	#ifdef MAIN_CONTROL_MC_1_RESET
	gpio_set_pin_level(MAIN_CONTROL_MC_1_RESET, 0);
	#endif
	#ifdef MAIN_CONTROL_MC_2_RESET
	gpio_set_pin_level(MAIN_CONTROL_MC_2_RESET, 0);
	#endif
	#ifdef MAIN_CONTROL_MC_3_RESET
	gpio_set_pin_level(MAIN_CONTROL_MC_3_RESET, 0);
	#endif
	#ifdef MAIN_CONTROL_MC_4_RESET
	gpio_set_pin_level(MAIN_CONTROL_MC_4_RESET, 0);
	#endif
	
	delay_ms(10);
	
	#if defined(MAIN_CONTROL_MC_1_RESET) && MAIN_CONTROL_NUM_MOTOR_CONTROLLER > 0
	activate_motor_controller(MAIN_CONTROL_MC_1_RESET, 40);
	#endif
	#if defined(MAIN_CONTROL_MC_2_RESET) && MAIN_CONTROL_NUM_MOTOR_CONTROLLER > 1
	activate_motor_controller(MAIN_CONTROL_MC_2_RESET, 41);
	#endif
	#if defined(MAIN_CONTROL_MC_3_RESET) && MAIN_CONTROL_NUM_MOTOR_CONTROLLER > 2
	activate_motor_controller(MAIN_CONTROL_MC_3_RESET, 42);
	#endif
	#if defined(MAIN_CONTROL_MC_4_RESET) && MAIN_CONTROL_NUM_MOTOR_CONTROLLER > 3
	activate_motor_controller(MAIN_CONTROL_MC_4_RESET, 43);
	#endif
	
	control->flags.all_stopped_or_reached = 0;
	for(uint8_t i = 0; i < MAIN_CONTROL_NUM_MOTORS; ++i){
		memset(&control->motors[i].state.flags_a, 0, sizeof(dc_motor_controller_state_flags_a_t));
		memset(&control->motors[i].state.flags_b, 0, sizeof(dc_motor_controller_state_flags_b_t));
	}
	main_control_control_flags.run_motor_controllers = 1;
	control->flags.all_stopped_or_reached = 0;
	control->active_executor = &calib_executor;
	if(calib_executor.start != NULL){
		calib_executor.start(control);
	}
}

static void activate_motor_controller(uint8_t reset_pin, uint8_t target_address){
	gpio_set_pin_level(reset_pin, 1);
	for(uint8_t i = 0; i < 250; ++i){
		change_address_success = 0;
		sam5_i2c_master_change_slave_address(49, target_address, change_address_success_callback);
		while(change_address_success == 0) {}
		if(change_address_success == 1){
			break;
		}
		delay_ms(5);
	}
	if(change_address_success == 1){
		printf("%d - Reset successful\r\n", target_address);
	}
	else{
		printf("%d - Reset failed\r\n", target_address);
	}
	// Check protocol identification
	change_address_success = 0;
	sam5_i2c_protocol_identification_t id = {
		.type = I2C_SLAVE_PROTOCOL_TYPE_MOTOR,
		.major_version = MOTOR_PROTOCOL_VERSION_MAJOR,
		.minor_version = MOTOR_PROTOCOL_VERSION_MINOR,
	};
	sam5_i2c_master_check_protocol_identification(target_address, &id);
}

static void change_address_success_callback(bool success, const uint8_t *data){
	change_address_success = success ? 1 : -1;
}

static void read_state_success_callback(bool success, const sam5_i2c_master_com_job_t * job){
	ASSERT(MOTOR_PROTOCOL_GET_COMMAND_TYPE(job->command) == MOTOR_PROTOCOL_READ_STATE);
	for(uint8_t i = 0; i < MAIN_CONTROL_NUM_MOTORS; ++i){
		main_control_motor_t* const motor = &g_main_control.motors[i];
		if(motor->i2c_address == job->slave_address && motor->motor_index == MOTOR_PROTOCOL_GET_MOTR_INDEX(job->command)){
			if(success){
				memcpy(&motor->state, job->buffer, sizeof(dc_motor_controller_state_t));
				read_success[i] = 1;
			}
			else{
				read_success[i] = -1;
			}
			return;
		}
	}
}

static void read_limits_success_callback(bool success, const sam5_i2c_master_com_job_t * job){
	for(uint8_t i = 0; i < MAIN_CONTROL_NUM_MOTORS; ++i){
		main_control_motor_t* const motor = &g_main_control.motors[i];
		if(motor->i2c_address == job->slave_address && motor->motor_index == MOTOR_PROTOCOL_GET_MOTR_INDEX(job->command)){
			if(success){
				memcpy(&motor->limits, job->buffer, sizeof(dc_motor_controller_limits_t));
				read_success[i] = 1;
			}
			else{
				read_success[i] = -1;
			}
			return;
		}
	}
}