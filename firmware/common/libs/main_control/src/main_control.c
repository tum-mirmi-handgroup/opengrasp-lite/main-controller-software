/*
* main_control.c
*
* Created: 27.01.2023 19:01:00
*  Author: Michael Ratzel
*/

#include "main_control_impl.h"
#include "motor_protocol.h"
#include "job_system.h"

#include <string.h>
#include <stdio.h>

static uint8_t are_inputs_equal(const main_control_input_t* input1, const main_control_input_t* input2);

control_flags_t main_control_control_flags;
dc_motor_controller_combined_state_flags_t main_control_combined_flags[MAIN_CONTROL_NUM_MOTOR_CONTROLLER];
dc_motor_controller_combined_state_t main_control_combined_state[MAIN_CONTROL_NUM_MOTOR_CONTROLLER];


void main_control_init(){
	// Init state machine before resetting
	g_main_control.flags.doing_startup = 1;
	g_main_control.active_input = NULL;
	g_main_control.active_executor = NULL;
	
	ASSERT(check_all_grasps() == 0);
	float32_t lowest_gear_ratio = 10000000;
	g_main_control.grasp_executor.highest_gear_ratio = 0;
	for(uint8_t i = 0; i < MAIN_CONTROL_NUM_MOTORS; ++i){
		if(g_main_control.motors[i].gear_ratio > g_main_control.grasp_executor.highest_gear_ratio){
			g_main_control.grasp_executor.highest_gear_ratio = g_main_control.motors[i].gear_ratio;
		}
		if(g_main_control.motors[i].gear_ratio < lowest_gear_ratio){
			lowest_gear_ratio = g_main_control.motors[i].gear_ratio;
		}
	}
	g_main_control.grasp_executor.min_velocity_factor = (MAIN_CONTROL_VELOCITY_LOWER_LIMIT * (g_main_control.grasp_executor.highest_gear_ratio / lowest_gear_ratio)) / MAIN_CONTROL_VELOCITY_LIMIT;
	
	#ifdef MAIN_CONTROL_MC_1_RESET
	gpio_set_pin_level(MAIN_CONTROL_MC_1_RESET, 1);
	gpio_set_pin_direction(MAIN_CONTROL_MC_1_RESET, GPIO_DIRECTION_OUT);
	gpio_set_pin_function(MAIN_CONTROL_MC_1_RESET, GPIO_PIN_FUNCTION_OFF);
	if(MAIN_CONTROL_MC_1_SWCLK && MAIN_CONTROL_MC_1_SWDIO){
		gpio_set_pin_direction(MAIN_CONTROL_MC_1_SWCLK, GPIO_DIRECTION_IN);
		gpio_set_pin_pull_mode(MAIN_CONTROL_MC_1_SWCLK, GPIO_PULL_OFF);
		gpio_set_pin_function(MAIN_CONTROL_MC_1_SWCLK, GPIO_PIN_FUNCTION_OFF);
		gpio_set_pin_direction(MAIN_CONTROL_MC_1_SWDIO, GPIO_DIRECTION_IN);
		gpio_set_pin_pull_mode(MAIN_CONTROL_MC_1_SWDIO, GPIO_PULL_OFF);
		gpio_set_pin_function(MAIN_CONTROL_MC_1_SWDIO, GPIO_PIN_FUNCTION_OFF);
	}
	#endif
	#ifdef MAIN_CONTROL_MC_2_RESET
	gpio_set_pin_level(MAIN_CONTROL_MC_2_RESET, 1);
	gpio_set_pin_direction(MAIN_CONTROL_MC_2_RESET, GPIO_DIRECTION_OUT);
	gpio_set_pin_function(MAIN_CONTROL_MC_2_RESET, GPIO_PIN_FUNCTION_OFF);
	if(MAIN_CONTROL_MC_2_SWCLK && MAIN_CONTROL_MC_2_SWDIO){
		gpio_set_pin_direction(MAIN_CONTROL_MC_2_SWCLK, GPIO_DIRECTION_IN);
		gpio_set_pin_pull_mode(MAIN_CONTROL_MC_2_SWCLK, GPIO_PULL_OFF);
		gpio_set_pin_function(MAIN_CONTROL_MC_2_SWCLK, GPIO_PIN_FUNCTION_OFF);
		gpio_set_pin_direction(MAIN_CONTROL_MC_2_SWDIO, GPIO_DIRECTION_IN);
		gpio_set_pin_pull_mode(MAIN_CONTROL_MC_2_SWDIO, GPIO_PULL_OFF);
		gpio_set_pin_function(MAIN_CONTROL_MC_2_SWDIO, GPIO_PIN_FUNCTION_OFF);
	}
	#endif
	#ifdef MAIN_CONTROL_MC_3_RESET
	gpio_set_pin_level(MAIN_CONTROL_MC_3_RESET, 1);
	gpio_set_pin_direction(MAIN_CONTROL_MC_3_RESET, GPIO_DIRECTION_OUT);
	gpio_set_pin_function(MAIN_CONTROL_MC_3_RESET, GPIO_PIN_FUNCTION_OFF);
	if(MAIN_CONTROL_MC_3_SWCLK && MAIN_CONTROL_MC_3_SWDIO){
		gpio_set_pin_direction(MAIN_CONTROL_MC_3_SWCLK, GPIO_DIRECTION_IN);
		gpio_set_pin_pull_mode(MAIN_CONTROL_MC_3_SWCLK, GPIO_PULL_OFF);
		gpio_set_pin_function(MAIN_CONTROL_MC_3_SWCLK, GPIO_PIN_FUNCTION_OFF);
		gpio_set_pin_direction(MAIN_CONTROL_MC_3_SWDIO, GPIO_DIRECTION_IN);
		gpio_set_pin_pull_mode(MAIN_CONTROL_MC_3_SWDIO, GPIO_PULL_OFF);
		gpio_set_pin_function(MAIN_CONTROL_MC_3_SWDIO, GPIO_PIN_FUNCTION_OFF);
	}
	#endif
	#ifdef MAIN_CONTROL_MC_4_RESET
	gpio_set_pin_level(MAIN_CONTROL_MC_4_RESET, 1);
	gpio_set_pin_direction(MAIN_CONTROL_MC_4_RESET, GPIO_DIRECTION_OUT);
	gpio_set_pin_function(MAIN_CONTROL_MC_4_RESET, GPIO_PIN_FUNCTION_OFF);
	if(MAIN_CONTROL_MC_4_SWCLK && MAIN_CONTROL_MC_4_SWDIO){
		gpio_set_pin_direction(MAIN_CONTROL_MC_4_SWCLK, GPIO_DIRECTION_IN);
		gpio_set_pin_pull_mode(MAIN_CONTROL_MC_4_SWCLK, GPIO_PULL_OFF);
		gpio_set_pin_function(MAIN_CONTROL_MC_4_SWCLK, GPIO_PIN_FUNCTION_OFF);
		gpio_set_pin_direction(MAIN_CONTROL_MC_4_SWDIO, GPIO_DIRECTION_IN);
		gpio_set_pin_pull_mode(MAIN_CONTROL_MC_4_SWDIO, GPIO_PULL_OFF);
		gpio_set_pin_function(MAIN_CONTROL_MC_4_SWDIO, GPIO_PIN_FUNCTION_OFF);
	}
	#endif
	
	g_main_control.flags.all_stopped_or_reached = 0;
	reset_motors_if_necessary(&g_main_control);
	
	// Set input depending on the available hardware
	#if MAIN_CONTROL_EMG_AVAILABLE
	g_main_control.active_input = &g_main_control.inputs[CONTROL_INPUT_SOURCE_EMG];
	g_main_control.flags.accept_reset_requests = 1;
	#elif MAIN_CONTROL_USART_AVAILABLE
	g_main_control.active_input = &g_main_control.inputs[CONTROL_INPUT_SOURCE_USART];
	g_main_control.flags.accept_reset_requests = 1;
	#else
	g_main_control.active_input = &g_main_control.inputs[CONTROL_INPUT_SOURCE_CONSOLE];
	g_main_control.flags.accept_reset_requests = 0;
	#endif
	
	for(uint8_t i = 0; i < CONTROL_NUM_INPUT_SOURCES; ++i){
		g_main_control.inputs[i].type = CONTROL_INPUT_TYPE_STOP;
	}
	
	for(uint8_t i = 0; i < MAIN_CONTROL_NUM_MOTOR_CONTROLLER; ++i){
		const sam5_i2c_master_com_job_t job = {
			.command = MOTOR_PROTOCOL_COMMON_READ_COMBINED_STATE_FLAGS,
			.slave_address = MOTOR_PROTOCOL_BASE_ADDRESS + i,
			.direction = I2C_MASTER_DIR_SLAVE_TO_MASTER,
			.num_bytes_to_transfer = sizeof(dc_motor_controller_combined_state_flags_t),
			.callback = &main_control_motor_i2c_callback_read_combined_state_flags,
			.period_ms = 10,
		};
		sam5_i2c_master_register_com_job(&job);
	}
	g_main_control.flags.doing_startup = 0;
}

void main_control_heartbeat()
{
	main_control_control_flags.heartbeat = !main_control_control_flags.heartbeat;
	#ifdef STATUS_LED_PIN_7
	gpio_set_pin_level(STATUS_LED_PIN_7, main_control_control_flags.heartbeat);
	#endif
	
	for(uint8_t i = 0; i < MAIN_CONTROL_NUM_MOTOR_CONTROLLER; ++i){
		const sam5_i2c_master_com_job_t job = {
			.command = MOTOR_PROTOCOL_COMMON_WRITE_CONTROL_FLAGS,
			.slave_address = MOTOR_PROTOCOL_BASE_ADDRESS + i,
			.direction = I2C_MASTER_DIR_MASTER_TO_SLAVE,
			.num_bytes_to_transfer = sizeof(main_control_control_flags),
			.callback = NULL,
			.period_ms = 0
		};
		memcpy((uint8_t*) job.buffer, &main_control_control_flags, sizeof(main_control_control_flags));
		sam5_i2c_master_register_com_job(&job);
	}
	if(g_main_control.error_counter > 0){
		--g_main_control.error_counter;
	}
}

void main_control_loop(){
	main_control_dispatch(MAIN_CONTROL_EVT_SIG_LOOP, &g_main_control);
	if(g_main_control.flags.doing_reset && g_main_control.flags.calibration_finished == 1){
		g_main_control.flags.doing_reset = 0;
	}
}

void request_reset_motors(main_control_t* control){
	++control->error_counter;
	if(control->error_counter > 5){
		control->flags.motor_reset_requested = 1;
	}
}

void main_control_loop_low_priority(){
	uint8_t motor_flag;
	CRITICAL_SECTION_ENTER();
	motor_flag = g_main_control.flags.motor_reset_requested;
	CRITICAL_SECTION_LEAVE();
	
	if(periodic_counter >= g_main_control.no_reset_timestamp){
		if(motor_flag && g_main_control.flags.accept_reset_requests){
			reset_motors(&g_main_control);
		}
		// No reset in the next 1000 ms
		g_main_control.no_reset_timestamp = periodic_counter + 1000;
		g_main_control.flags.motor_reset_requested = 0;
		g_main_control.error_counter = 0;
	}
}

void main_control_dispatch(simple_state_machine_signal_t signal, main_control_t* control){
	main_control_executor_t* executor;
	if(control->flags.doing_reset){
		executor = control->flags.calib_executor_running ? &calib_executor : NULL;
	}
	else{
		if(control->active_input == NULL){
			executor = NULL;
		}
		switch(control->active_input->type){
			case CONTROL_INPUT_TYPE_STOP: executor = &stop_executor; break;
			case CONTROL_INPUT_TYPE_GRASP: executor = &grasp_executor; break;
			case CONTROL_INPUT_TYPE_DIRECT: executor = &direct_executor; break;
			case CONTROL_INPUT_TYPE_CALIB: executor = &calib_executor; break;
			default: executor = NULL; break;
		}
	}
	if(control->active_executor != executor){
		if(control->active_executor != NULL && control->active_executor->stop != NULL){
			control->active_executor->stop(control);
		}
		if(executor != NULL && executor->start != NULL){
			executor->start(control);
		}
		control->active_executor = executor;
	}
	if(executor != NULL && executor->execute != NULL){
		simple_state_machine_event_t event = { .signal_ = signal };
		executor->execute(&event, control);
	}
}

void main_control_set_active_input(main_control_input_source_e new_active_input){
	if(g_main_control.flags.doing_startup){
		return;
	}
	if(g_main_control.active_input != &g_main_control.inputs[new_active_input]){
		const main_control_input_t* old_input = g_main_control.active_input;
		g_main_control.active_input = &g_main_control.inputs[new_active_input];
		if(!are_inputs_equal(old_input, g_main_control.active_input)){
			main_control_dispatch(MAIN_CONTROL_EVT_SIG_NEW_INPUT, &g_main_control);
		}
	}
}

void main_control_set_input(main_control_input_source_e source, main_control_input_t* new_input){
	if(g_main_control.flags.doing_startup){
		return;
	}
	ASSERT(new_input != NULL);
	uint8_t inputs_differ = g_main_control.active_input == &g_main_control.inputs[source] && !are_inputs_equal(g_main_control.active_input, new_input);
	memcpy(&g_main_control.inputs[source], new_input, sizeof(main_control_input_t));
	
	if(inputs_differ){
		main_control_dispatch(MAIN_CONTROL_EVT_SIG_NEW_INPUT, &g_main_control);
	}
}

float32_t main_control_convert_percentage_to_position(const main_control_motor_t* motor, float32_t percentage){
	if(motor->open_at_positive){
		return motor->limits.angle_max + percentage * (motor->limits.angle_min - motor->limits.angle_max);
	}
	else{
		return motor->limits.angle_min + percentage * (motor->limits.angle_max - motor->limits.angle_min);
	}
}

float32_t main_control_convert_position_to_percentage(const main_control_motor_t* motor, float32_t position){
	if(motor->open_at_positive){
		return (position - motor->limits.angle_max) / (motor->limits.angle_min - motor->limits.angle_max);
	}
	else{
		return (position - motor->limits.angle_min) / (motor->limits.angle_max - motor->limits.angle_min);
	}
}

static uint8_t are_inputs_equal(const main_control_input_t* input1, const main_control_input_t* input2){
	if(input1 == input2){
		return 1;
	}
	if(input1 == NULL || input2 == NULL){
		return 0;
	}
	if(input1->type != input2->type){
		return 0;
	}
	switch(input1->type){
		case CONTROL_INPUT_TYPE_GRASP:
		return input1->grasp_index == input2->grasp_index && input1->grasp_speed_factor == input2->grasp_speed_factor && input1->grasp_contact_current_factor == input2->grasp_contact_current_factor;
		
		case CONTROL_INPUT_TYPE_DIRECT:{
			if(input1->direct_finger_index != input2->direct_finger_index){
				return 0;
			}
			if(input1->direct_input.flags.control_mode != input2->direct_input.flags.control_mode){
				return 0;
			}
			if(input1->direct_input.flags.stop_at_contact != input2->direct_input.flags.stop_at_contact){
				return 0;
			}
			if(input1->direct_input.flags.continue_at_end != input2->direct_input.flags.continue_at_end){
				return 0;
			}
			if(input1->direct_input.flags.use_aggressive_contact_detection != input2->direct_input.flags.use_aggressive_contact_detection){
				return 0;
			}
			switch(input1->direct_input.flags.control_mode){
				case MOTOR_CM_POSITION:
				if(input1->direct_input.angle !=  input2->direct_input.angle){
					return 0;
				}
				case MOTOR_CM_VELOCITY:
				if(input1->direct_input.velocity !=  input2->direct_input.velocity){
					return 0;
				}
				case MOTOR_CM_CURRENT:
				if(input1->direct_input.current !=  input2->direct_input.current){
					return 0;
				}
				case MOTOR_CM_VOLTAGE:
				if(input1->direct_input.voltage !=  input2->direct_input.voltage){
					return 0;
				}
				break;
				
				case MOTOR_CM_STOPPED:
				case MOTOR_CM_CALIB_PREP_POS:
				case MOTOR_CM_CALIB_PREP_NEG:
				case MOTOR_CM_CALIBRATION:
				return 1;
				
				default:
				return 0;
				
			}
		}
		
		case CONTROL_INPUT_TYPE_STOP:
		case CONTROL_INPUT_TYPE_CALIB:
		return 1;
		
		default:
		return 0;
	}
}