/*
* main_control_impl.h
*
* Created: 29.01.2023 19:36:01
*  Author: Michael Ratzel
*/

#ifndef MAIN_CONTROL_IMPL_H_
#define MAIN_CONTROL_IMPL_H_

#include "main_control.h"
#include "sam5_i2c_master.h"
#include "hardware_defines.h"
#include "motor_protocol.h"


extern main_control_executor_t calib_executor;
extern main_control_executor_t direct_executor;
extern main_control_executor_t grasp_executor;
extern main_control_executor_t stop_executor;


void main_control_dispatch(simple_state_machine_signal_t signal_t, main_control_t* control);

void motor_i2c_send_control_mode(const main_control_motor_t* motor, uint8_t control_mode, uint8_t stop_at_contact, uint8_t use_aggressive_contact_detection);
void motor_i2c_send_control_mode_stop(const main_control_motor_t* motor);
void motor_i2c_send_setpoint(const main_control_motor_t* motor, uint8_t command, float32_t setpoint);
void motor_i2c_send_closing_current_setpoint(const main_control_motor_t* motor, float32_t setpoint);
void motor_i2c_send_complete_input(const main_control_motor_t* motor, dc_motor_controller_inputs_t* input);

void reset_motors_if_necessary(main_control_t* control);
void reset_motors(main_control_t* control);
void request_reset_motors(main_control_t* control);

uint8_t check_all_grasps();

#if MAIN_CONTROL_SPEED_FACTOR_NUM_STEPS < 3
#error "Main control speed granularity must be at least 3 steps"
#endif

#if MAIN_CONTROL_THUMB_MOTOR_INDEX < 0 || MAIN_CONTROL_THUMB_MOTOR_INDEX > 3
#error "Invalid number specified for MAIN_CONTROL_THUMB_MOTOR_INDEX"
#endif
#if MAIN_CONTROL_INDEX_MOTOR_INDEX < 0 || MAIN_CONTROL_INDEX_MOTOR_INDEX > 3
#error "Invalid number specified for MAIN_CONTROL_INDEX_MOTOR_INDEX"
#endif
#if MAIN_CONTROL_MIDDLE_MOTOR_INDEX < 0 || MAIN_CONTROL_MIDDLE_MOTOR_INDEX > 3
#error "Invalid number specified for MAIN_CONTROL_MIDDLE_MOTOR_INDEX"
#endif
#if MAIN_CONTROL_RING_MOTOR_INDEX < 0 || MAIN_CONTROL_RING_MOTOR_INDEX > 3
#error "Invalid number specified for MAIN_CONTROL_RING_MOTOR_INDEX"
#endif
#if MAIN_CONTROL_PINKY_MOTOR_INDEX < 0 || MAIN_CONTROL_PINKY_MOTOR_INDEX > 3
#error "Invalid number specified for MAIN_CONTROL_PINKY_MOTOR_INDEX"
#endif
#if MAIN_CONTROL_MISC_MOTOR_INDEX < 0 || MAIN_CONTROL_MISC_MOTOR_INDEX > 3
#error "Invalid number specified for MAIN_CONTROL_MISC_MOTOR_INDEX"
#endif


#if MAIN_CONTROL_NUM_MOTOR_CONTROLLER > 4
#error "More than four motor controllers are currently not supported"

#elif MAIN_CONTROL_NUM_MOTOR_CONTROLLER == 4

#ifndef MAIN_CONTROL_MC_1_RESET
#error "MAIN_CONTROL_MC_1_RESET is not defined"
#endif
#ifndef MAIN_CONTROL_MC_2_RESET
#error "MAIN_CONTROL_MC_2_RESET is not defined"
#endif
#ifndef MAIN_CONTROL_MC_3_RESET
#error "MAIN_CONTROL_MC_3_RESET is not defined"
#endif
#ifndef MAIN_CONTROL_MC_4_RESET
#error "MAIN_CONTROL_MC_4_RESET is not defined"
#endif
#if MAIN_CONTROL_THUMB_MOTOR_CONTROLLER < 0 || MAIN_CONTROL_THUMB_MOTOR_CONTROLLER > 4
#error "MAIN_CONTROL_THUMB_MOTOR_CONTROLLER has invalid number"
#endif
#if MAIN_CONTROL_INDEX_MOTOR_CONTROLLER < 0 || MAIN_CONTROL_INDEX_MOTOR_CONTROLLER > 4
#error "MAIN_CONTROL_INDEX_MOTOR_CONTROLLER has invalid number"
#endif
#if MAIN_CONTROL_MIDDLE_MOTOR_CONTROLLER < 0 || MAIN_CONTROL_MIDDLE_MOTOR_CONTROLLER > 4
#error "MAIN_CONTROL_MIDDLE_MOTOR_CONTROLLER has invalid number"
#endif
#if MAIN_CONTROL_RING_MOTOR_CONTROLLER < 0 || MAIN_CONTROL_RING_MOTOR_CONTROLLER > 4
#error "MAIN_CONTROL_RING_MOTOR_CONTROLLER has invalid number"
#endif
#if MAIN_CONTROL_PINKY_MOTOR_CONTROLLER < 0 || MAIN_CONTROL_PINKY_MOTOR_CONTROLLER > 4
#error "MAIN_CONTROL_PINKY_MOTOR_CONTROLLER has invalid number"
#endif
#if MAIN_CONTROL_MISC_MOTOR_CONTROLLER < 0 || MAIN_CONTROL_MISC_MOTOR_CONTROLLER > 4
#error "MAIN_CONTROL_MISC_MOTOR_CONTROLLER has invalid number"
#endif

#elif MAIN_CONTROL_NUM_MOTOR_CONTROLLER == 3

#ifndef MAIN_CONTROL_MC_1_RESET
#error "MAIN_CONTROL_MC_1_RESET is not defined"
#endif
#ifndef MAIN_CONTROL_MC_2_RESET
#error "MAIN_CONTROL_MC_2_RESET is not defined"
#endif
#ifndef MAIN_CONTROL_MC_3_RESET
#error "MAIN_CONTROL_MC_3_RESET is not defined"
#endif
#if MAIN_CONTROL_THUMB_MOTOR_CONTROLLER < 0 || MAIN_CONTROL_THUMB_MOTOR_CONTROLLER > 3
#error "MAIN_CONTROL_THUMB_MOTOR_CONTROLLER has invalid number"
#endif
#if MAIN_CONTROL_INDEX_MOTOR_CONTROLLER < 0 || MAIN_CONTROL_INDEX_MOTOR_CONTROLLER > 3
#error "MAIN_CONTROL_INDEX_MOTOR_CONTROLLER has invalid number"
#endif
#if MAIN_CONTROL_MIDDLE_MOTOR_CONTROLLER < 0 || MAIN_CONTROL_MIDDLE_MOTOR_CONTROLLER > 3
#error "MAIN_CONTROL_MIDDLE_MOTOR_CONTROLLER has invalid number"
#endif
#if MAIN_CONTROL_RING_MOTOR_CONTROLLER < 0 || MAIN_CONTROL_RING_MOTOR_CONTROLLER > 3
#error "MAIN_CONTROL_RING_MOTOR_CONTROLLER has invalid number"
#endif
#if MAIN_CONTROL_PINKY_MOTOR_CONTROLLER < 0 || MAIN_CONTROL_PINKY_MOTOR_CONTROLLER > 3
#error "MAIN_CONTROL_PINKY_MOTOR_CONTROLLER has invalid number"
#endif
#if MAIN_CONTROL_MISC_MOTOR_CONTROLLER < 0 || MAIN_CONTROL_MISC_MOTOR_CONTROLLER > 3
#error "MAIN_CONTROL_MISC_MOTOR_CONTROLLER has invalid number"
#endif

#elif MAIN_CONTROL_NUM_MOTOR_CONTROLLER == 2

#ifndef MAIN_CONTROL_MC_1_RESET
#error "MAIN_CONTROL_MC_1_RESET is not defined"
#endif
#ifndef MAIN_CONTROL_MC_2_RESET
#error "MAIN_CONTROL_MC_2_RESET is not defined"
#endif
#if MAIN_CONTROL_THUMB_MOTOR_CONTROLLER < 0 || MAIN_CONTROL_THUMB_MOTOR_CONTROLLER > 2
#error "MAIN_CONTROL_THUMB_MOTOR_CONTROLLER has invalid number"
#endif
#if MAIN_CONTROL_INDEX_MOTOR_CONTROLLER < 0 || MAIN_CONTROL_INDEX_MOTOR_CONTROLLER > 2
#error "MAIN_CONTROL_INDEX_MOTOR_CONTROLLER has invalid number"
#endif
#if MAIN_CONTROL_MIDDLE_MOTOR_CONTROLLER < 0 || MAIN_CONTROL_MIDDLE_MOTOR_CONTROLLER > 2
#error "MAIN_CONTROL_MIDDLE_MOTOR_CONTROLLER has invalid number"
#endif
#if MAIN_CONTROL_RING_MOTOR_CONTROLLER < 0 || MAIN_CONTROL_RING_MOTOR_CONTROLLER > 2
#error "MAIN_CONTROL_RING_MOTOR_CONTROLLER has invalid number"
#endif
#if MAIN_CONTROL_PINKY_MOTOR_CONTROLLER < 0 || MAIN_CONTROL_PINKY_MOTOR_CONTROLLER > 2
#error "MAIN_CONTROL_PINKY_MOTOR_CONTROLLER has invalid number"
#endif
#if MAIN_CONTROL_MISC_MOTOR_CONTROLLER < 0 || MAIN_CONTROL_MISC_MOTOR_CONTROLLER > 2
#error "MAIN_CONTROL_MISC_MOTOR_CONTROLLER has invalid number"
#endif

#elif MAIN_CONTROL_NUM_MOTOR_CONTROLLER == 1

#ifndef MAIN_CONTROL_MC_1_RESET
#error "MAIN_CONTROL_MC_1_RESET is not defined"
#endif
#if MAIN_CONTROL_THUMB_MOTOR_CONTROLLER < 0 || MAIN_CONTROL_THUMB_MOTOR_CONTROLLER > 1
#error "MAIN_CONTROL_THUMB_MOTOR_CONTROLLER has invalid number"
#endif
#if MAIN_CONTROL_INDEX_MOTOR_CONTROLLER < 0 || MAIN_CONTROL_INDEX_MOTOR_CONTROLLER > 1
#error "MAIN_CONTROL_INDEX_MOTOR_CONTROLLER has invalid number"
#endif
#if MAIN_CONTROL_MIDDLE_MOTOR_CONTROLLER < 0 || MAIN_CONTROL_MIDDLE_MOTOR_CONTROLLER > 1
#error "MAIN_CONTROL_MIDDLE_MOTOR_CONTROLLER has invalid number"
#endif
#if MAIN_CONTROL_RING_MOTOR_CONTROLLER < 0 || MAIN_CONTROL_RING_MOTOR_CONTROLLER > 1
#error "MAIN_CONTROL_RING_MOTOR_CONTROLLER has invalid number"
#endif
#if MAIN_CONTROL_PINKY_MOTOR_CONTROLLER < 0 || MAIN_CONTROL_PINKY_MOTOR_CONTROLLER > 1
#error "MAIN_CONTROL_PINKY_MOTOR_CONTROLLER has invalid number"
#endif
#if MAIN_CONTROL_MISC_MOTOR_CONTROLLER < 0 || MAIN_CONTROL_MISC_MOTOR_CONTROLLER > 1
#error "MAIN_CONTROL_MISC_MOTOR_CONTROLLER has invalid number"
#endif

#elif MAIN_CONTROL_NUM_MOTOR_CONTROLLER == 0

#warning "MAIN_CONTROL no motor controller specified"

#else
#error "Please specify a valid number for MAIN_CONTROL_NUM_MOTOR_CONTROLLER"
#endif

#define MAIN_CONTROL_IS_GEAR_FLIPPING_DIRECTION(gear_ratio)	\
	(gear_ratio == MOTOR_GEAR_RATIO_5 ?		0 : \
	(gear_ratio == MOTOR_GEAR_RATIO_10 ?	0 : \
	(gear_ratio == MOTOR_GEAR_RATIO_15 ?	1 : \
	(gear_ratio == MOTOR_GEAR_RATIO_30 ?	0 : \
	(gear_ratio == MOTOR_GEAR_RATIO_50 ?	0 : \
	(gear_ratio == MOTOR_GEAR_RATIO_75 ?	0 : \
	(gear_ratio == MOTOR_GEAR_RATIO_100 ?	0 : \
	(gear_ratio == MOTOR_GEAR_RATIO_150 ?	1 : \
	(gear_ratio == MOTOR_GEAR_RATIO_210 ?	1 : \
	(gear_ratio == MOTOR_GEAR_RATIO_250 ?	1 : \
	(gear_ratio == MOTOR_GEAR_RATIO_298 ?	1 : \
	(gear_ratio == MOTOR_GEAR_RATIO_380 ?	1 : \
	(gear_ratio == MOTOR_GEAR_RATIO_1000 ?	0 : \
	0)))))))))))))


#define MAIN_CONTROL_GET_GEAR_RATIO(gear_ratio)																						\
	(gear_ratio == MOTOR_GEAR_RATIO_5 ?		((27.f/20.f)*(37.f/10.f)) :																\
	(gear_ratio == MOTOR_GEAR_RATIO_10 ?	((35.f/13.f)*(37.f/10.f)) :																\
	(gear_ratio == MOTOR_GEAR_RATIO_15 ?	((25.f/12.f)*(34.f/9.f)*(31.f/16.f)) :													\
	(gear_ratio == MOTOR_GEAR_RATIO_30 ?	((31.f/16.f)*(33.f/14.f)*(35.f/13.f)*(34.f/14.f)) :										\
	(gear_ratio == MOTOR_GEAR_RATIO_50 ?	((32.f/15.f)*(33.f/14.f)*(35.f/13.f)*(38.f/10.f)) :										\
	(gear_ratio == MOTOR_GEAR_RATIO_75 ?	((34.f/13.f)*(34.f/12.f)*(35.f/13.f)*(38.f/10.f)) :										\
	(gear_ratio == MOTOR_GEAR_RATIO_100 ?	((35.f/12.f)*(37.f/11.f)*(35.f/13.f)*(38.f/10.f)) :										\
	(gear_ratio == MOTOR_GEAR_RATIO_150 ?	((25.f/12.f)*(32.f/11.f)*(34.f/14.f)*(35.f/13.f)*(38.f/10.f)) :							\
	(gear_ratio == MOTOR_GEAR_RATIO_210 ?	((25.f/12.f)*(34.f/9.f)*(34.f/13.f)*(35.f/13.f)*(38.f/10.f)) :							\
	(gear_ratio == MOTOR_GEAR_RATIO_250 ?	((25.f/12.f)*(34.f/10.f)*(37.f/10.f)*(35.f/14.f)*(38.f/10.f)) :							\
	(gear_ratio == MOTOR_GEAR_RATIO_298 ?	((25.f/12.f)*(34.f/9.f)*(37.f/10.f)*(35.f/13.f)*(38.f/10.f)) :							\
	(gear_ratio == MOTOR_GEAR_RATIO_380 ?	((25.f/12.f)*(35.f/9.f)*(39.f/9.f)*(36.f/13.f)*(39.f/10.f)) :							\
	(gear_ratio == MOTOR_GEAR_RATIO_1000 ?	((25.f/12.f)*(34.f/9.f)*(35.f/12.f)*(34.f/14.f)*(34.f/14.f)*(34.f/14.f)*(27.f/9.f)) :	\
	0)))))))))))))

#endif /* GRASPING_CONTROL_IMPL_H_ */