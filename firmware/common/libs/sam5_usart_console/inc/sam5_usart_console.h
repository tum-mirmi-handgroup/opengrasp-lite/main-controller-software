#ifndef _SAM5_USART_CONSOLE_H_
#define _SAM5_USART_CONSOLE_H_

#include <stdint.h>

void sam5_usart_console_output_init();
void sam5_usart_console_input_init();

void sam5_usart_console_register_command(const char* command_name, const char* description, const char* help_text, uint8_t (*callback)(const int, const char **));

void sam5_usart_console_await_next_command();

#define SAM5_USART_CONSOLE_MAX_PARAMETER_LENGTH 20
#define SAM5_USART_CONSOLE_MAX_NUM_COMMANDS 16
#define SAM5_USART_CONSOLE_BUFFER_LENGTH 200
#define SAM5_USART_CONSOLE_MAX_NUM_PARAMETERS 16


#endif //_SAM5_USART_CONSOLE_H_