#ifndef _SAM5_USART_CONSOLE_IMPL_H_
#define _SAM5_USART_CONSOLE_IMPL_H_

#include "sam5_usart_console.h"

#include <arm_neon.h>

struct console_command {
	const char name[SAM5_USART_CONSOLE_MAX_PARAMETER_LENGTH];
	const char* description;
	const char* help_text;
	uint8_t (* callback)(const int, const char **);
};

void rx_callback(char received);

uint8_t execute_command();
uint32_t find_first_parameter(const uint32_t max_parameter_length, uint32_t *search_start_index, char *parameter);

struct console_command* find_matching_command(const char* name);

uint8_t parse_bool_parameter(const char* parameter, uint8_t* value);
uint8_t parse_integer_parameter(const char* parameter, int32_t* value);
uint8_t parse_float_parameter(const char* parameter, float32_t* value);

void print_float(const char *pre_string, float32_t value, const char *post_string);

extern struct console_command g_commands[SAM5_USART_CONSOLE_MAX_NUM_COMMANDS];
extern uint32_t g_num_commands;

#endif //_SAM5_USART_CONSOLE_IMPL_H_