#include "sam5_usart_console_commands.h"
#include "../sam5_usart_console_impl.h"

#include <atmel_start.h>
#include <stdio.h>
#include <string.h>

const char sam5_usart_console_reset_desc[] = "reset the microcontroller";
const char sam5_usart_console_reset_help[] =
"Usage:\r\n"
"reset\r\n"
"  Reset the microcontroller and restart the software\r\n"
;

uint8_t sam5_usart_console_reset(const int argc, const char** argv){
	printf("Resetting...\r\n");
	__NVIC_SystemReset();
	return 0;
}

const char sam5_usart_console_s_desc[] = "switch to manual mode and stop the main loop";
const char sam5_usart_console_s_help[] =
"Usage:\r\n"
"stop\r\n"
"  Short for main stop - see \"help main\" for more information\r\n"
;
uint8_t sam5_usart_console_s(const int argc, const char** argv){
	const int argc_new = 2;
	const char * arg0 = "main";
	const char * arg1 = "stop";
	const char * argv_new[] = {
		arg0,
		arg1
	};
	return sam5_usart_console_main(argc_new, argv_new);
}

const char sam5_usart_console_stop_desc[] = "switch to manual mode and stop the main loop";
const char sam5_usart_console_stop_help[] =
"Usage:\r\n"
"stop\r\n"
"  Short for main stop - see \"help main\" for more information\r\n"
;
uint8_t sam5_usart_console_stop(const int argc, const char** argv){
	const int argc_new = 2;
	const char * arg0 = "main";
	const char * arg1 = "stop";
	const char * argv_new[] = {
		arg0,
		arg1
	};
	return sam5_usart_console_main(argc_new, argv_new);
}
