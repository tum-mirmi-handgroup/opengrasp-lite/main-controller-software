#include "sam5_usart_console_commands.h"
#include "../sam5_usart_console_impl.h"
#include "sam5_bmp3_wrapper.h"

#include <stdio.h>
#include <string.h>

const char sam5_usart_console_bmp3_desc[] = "Control the BMP3 sensor subsystem";
const char sam5_usart_console_bmp3_help[] =
"Usage:\r\n"
"bmp3 search\r\n"
"  Search for available bmp3 sensors\r\n"
"bmp3 status\r\n"
"  Same as \"status bmp3\" - Status of the BMP3XX pressure sensors in the fingertips and palm\r\n"
;

static void bmp3_simple_finished_callback();

uint8_t sam5_usart_console_bmp3(const int argc, const char** argv){
	if(argc == 1){
		printf("Please specify an action to be executed\r\n");
		printf("Use \"help bmp3\" for usage tips\r\n");
		return 0;
	}
	if(strcmp("search", argv[1]) == 0){
		sam5_bmp3_wrapper_request_search_sensors(bmp3_simple_finished_callback);
		return 1;
	}
	else if(strcmp("status", argv[1]) == 0){
		sam5_bmp3_wrapper_request_print_status(bmp3_simple_finished_callback);
		return 1;
	}
	printf("Unrecognized bmp3 action %s\r\n", argv[1]);
	printf("Use \"help bmp3\" for usage tips\r\n");
	return 0;
}

static void bmp3_simple_finished_callback(){
	sam5_usart_console_await_next_command();
}