#include "sam5_usart_console_commands.h"
#include "../sam5_usart_console_impl.h"
#include "main_control.h"
#include "motor_protocol.h"



#include <stdio.h>
#include <string.h>


const char sam5_usart_console_finger_desc[] = "control fingers directly";
const char sam5_usart_console_finger_help[] =
"Usages:\r\n"
"finger <index>... pos <position>\r\n"
"  Move finger(s) to position\r\n"
"  <index>... One or multiple indexes of fingers (0-Thumb, 1-Index, 2-Middle, 3-Ring, 4-Pinky, 5-Misc)\r\n"
"  <position> Target position between 0 and 1\r\n"
"\r\n"
"finger <index>... vel <velocity>\r\n"
"  Move finger(s) with velocity\r\n"
"  <index>... One or multiple indexes of fingers\r\n"
"  <velocity> Velocity in [rad/s]\r\n"
"\r\n"
"finger <index>... cur <current>\r\n"
"  Move finger(s) with current\r\n"
"  <index>... One or multiple indexes of fingers\r\n"
"  <current> Current in [A]\r\n"
"\r\n"
"finger config <vel, cur, contact, aggressive> <value>\r\n"
"  Configure limits that will be used for position / velocity control\r\n"
"  <value> In [rad/s], [A], [bool], [bool] respectively\r\n"
"\r\n"
"finger config\r\n"
"  See the current configuration\r\n"
"\r\n"
"finger reset\r\n"
"  Change configurable options back to defaults\r\n"
"\r\n"
;

static main_control_input_t finger_input;
static float32_t configured_vel = 1500;
static float32_t configured_cur = 0.3;
static uint8_t configured_stop_at_contact = 1;
static uint8_t configured_use_aggressive = 1;

uint8_t sam5_usart_console_finger(const int argc, const char** argv){
	if(argc < 2){
		printf("Invalid syntax - check \"help finger\"\r\n");
		return 0;
	}
	if(strcmp("reset", argv[1]) == 0){
		configured_vel = 1500;
		configured_cur = 0.3;
		configured_stop_at_contact = 1;
		configured_use_aggressive = 1;
		return 0;
	}
	if(strcmp("config", argv[1]) == 0){
		if(argc < 4){
			printf("Configured values:\r\n");
			print_float("    Velocity: ", configured_vel, " rad/s\r\n");
			print_float("     Current: ", configured_cur, " A\r\n");
			printf("  At Contact: %s\r\n", configured_stop_at_contact ? "stop" : "continue");
			printf("   Detection: %s\r\n", configured_use_aggressive ? "aggressive" : "normal");
			return 0;
		}
		float32_t value_f;
		uint8_t value_b;
		if(strcmp("vel", argv[2]) == 0){
			if(parse_float_parameter(argv[3], &value_f) == 0){
				printf("Could not parse value to configure from \"%s\"\r\n", argv[3]);
				return 0;
			}
			configured_vel = value_f;
		}
		else if(strcmp("cur", argv[2]) == 0){
			if(parse_float_parameter(argv[3], &value_f) == 0){
				printf("Could not parse value to configure from \"%s\"\r\n", argv[3]);
				return 0;
			}
			configured_cur = value_f;
		}
		else if(strcmp("contact", argv[2]) == 0){
			if(parse_bool_parameter(argv[3], &value_b) == 0){
				printf("Could not parse value to configure from \"%s\"\r\n", argv[3]);
				return 0;
			}
			configured_stop_at_contact = value_b;
		}
		else if(strcmp("aggressive", argv[2]) == 0){
			if(parse_bool_parameter(argv[3], &value_b) == 0){
				printf("Could not parse value to configure from \"%s\"\r\n", argv[3]);
				return 0;
			}
			configured_use_aggressive = value_b;
		}
		else {
			printf("\"%s\" is not a value that can be configured\r\n", argv[2]);
		}
		return 0;
	}
	finger_input.type = CONTROL_INPUT_TYPE_DIRECT;

	finger_input.direct_input.flags.continue_at_end = 0;
	finger_input.direct_input.flags.stop_at_contact = configured_stop_at_contact != 0;
	finger_input.direct_input.flags.use_endstop_for_end = 0;
	finger_input.direct_input.flags.use_endstop_for_refine = 0;
	finger_input.direct_input.flags.use_aggressive_contact_detection = configured_use_aggressive != 0;
	
	finger_input.direct_input.angle = 0;
	finger_input.direct_input.velocity = configured_vel;
	finger_input.direct_input.current = configured_cur;
	finger_input.direct_input.voltage = 6;
	
	uint8_t finger_index = 0xFF;
	for(uint8_t i = 0; i < MAIN_CONTROL_NUM_MOTORS + 1; ++i){
		if(i + 2 >= argc){
			printf("Invalid syntax - check \"help finger\"\r\n");
			return 0;
		}
		
		int32_t new_finger;
		float32_t value;
		
		if(strcmp("pos", argv[i + 1]) == 0){
			if(finger_index == 0xFF){
				printf("No valid finger specified - Ignoring command\r\n");
				return 0;
			}
			if(parse_float_parameter(argv[i + 2], &value) == 0){
				printf("Failed reading position value from \"%s\"\r\n", argv[i+2]);
				return 0;
			}
			finger_input.direct_finger_index = finger_index;
			finger_input.direct_input.flags.control_mode = MOTOR_CM_POSITION;
			finger_input.direct_input.angle = value;
			main_control_set_input(CONTROL_INPUT_SOURCE_CONSOLE, &finger_input);
			return 0;
		}
		else if(strcmp("vel", argv[i + 1]) == 0){
			if(finger_index == 0xFF){
				printf("No valid finger specified - Ignoring command\r\n");
				return 0;
			}
			if(parse_float_parameter(argv[i + 2], &value) == 0){
				printf("Failed reading velocity value from \"%s\"\r\n", argv[i+2]);
				return 0;
			}
			finger_input.direct_finger_index = finger_index;
			finger_input.direct_input.flags.control_mode = MOTOR_CM_VELOCITY;
			finger_input.direct_input.velocity = value;
			main_control_set_input(CONTROL_INPUT_SOURCE_CONSOLE, &finger_input);
			return 0;
		}
		else if(strcmp("cur", argv[i + 1]) == 0){
			if(finger_index == 0xFF){
				printf("No valid finger specified - Ignoring command\r\n");
				return 0;
			}
			if(parse_float_parameter(argv[i + 2], &value) == 0){
				printf("Failed reading current value from \"%s\"\r\n", argv[i+2]);
				return 0;
			}
			finger_input.direct_finger_index = finger_index;
			finger_input.direct_input.flags.control_mode = MOTOR_CM_CURRENT;
			finger_input.direct_input.current = value;
			main_control_set_input(CONTROL_INPUT_SOURCE_CONSOLE, &finger_input);
			return 0;
		}
		else if(parse_integer_parameter(argv[i + 1], &new_finger) != 0){
			if(finger_index != 0xFF){
				printf("Overwriting finger number - Previous %d will now be ignored\r\n", finger_index);
			}
			if(0 <= new_finger && new_finger <= 5){
				finger_index = new_finger;
			}
			else{
				printf("Warning - Finger index %ld out of bounds - Will be ignored\r\n", new_finger);
			}
		}
		else{
			printf("Could not parse \"%s\"\r\n", argv[i + 1]);
		}
	}
	printf("Invalid syntax - check \"help finger\"\r\n");
	return 0;
}