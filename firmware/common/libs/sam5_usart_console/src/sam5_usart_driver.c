#include "sam5_usart_driver.h"
#include <atmel_start.h>
#include <errno.h>


/************************************************************************/
/* Ringbuffer Implementation adapted from atmel start utils              */
/************************************************************************/

struct sam5_ringbuffer {
	uint8_t buf[USART_TRANSMIT_BUFFER_SIZE];	// Buffer
	uint32_t size;								// Buffer size - 1
	uint32_t read_index;						// Buffer read index
	uint32_t write_index;						//Buffer write index
};

struct sam5_ringbuffer rb;

void sam5_ringbuffer_init()
{
	if ((USART_TRANSMIT_BUFFER_SIZE & (USART_TRANSMIT_BUFFER_SIZE - 1)) != 0) {
		return;
	}
	rb.size        = USART_TRANSMIT_BUFFER_SIZE - 1;
	rb.read_index  = 0;
	rb.write_index = 0;
}

uint8_t sam5_ringbuffer_get()
{
	return rb.buf[rb.read_index++ & rb.size];
}

void sam5_ringbuffer_put(uint8_t data)
{
	rb.buf[rb.write_index & rb.size] = data;
	if ((rb.write_index - rb.read_index) > rb.size) {
		rb.read_index = rb.write_index - rb.size;
	}
	rb.write_index++;
}

uint32_t sam5_ringbuffer_num()
{
	return rb.write_index - rb.read_index;
}

void sam5_ringbuffer_flush()
{
	rb.read_index = 0;
	rb.write_index = 0;
}


void usart_driver_init(){
	sam5_ringbuffer_init();
	
	///* Initialize pins
	///* Deactivate GPIO_PULL_UP and GPIO_PULL_DOWN on the RX pin
	gpio_set_pin_pull_mode(USART_CONSOLE_RX_PIN, GPIO_PULL_OFF);
	///* Deactivate GPIO_PULL_UP and GPIO_PULL_DOWN on the TX pin
	gpio_set_pin_pull_mode(USART_CONSOLE_TX_PIN, GPIO_PULL_OFF);
	///* Set function of USART_RX_PIN to USART_RX_PIN_FUNCTION
	gpio_set_pin_function(USART_CONSOLE_RX_PIN, USART_CONSOLE_RX_PIN_FUNCTION);
	///* Set function of USART_TX_PIN to USART_TX_PIN_FUNCTION
	gpio_set_pin_function(USART_CONSOLE_TX_PIN, USART_CONSOLE_TX_PIN_FUNCTION);
	
	///* Clock settings
	//* Enable APB SERCOM units APB communication
	USART_APB_MCLK_MASK_SETTER(MCLK);
	//* Set GCLK
	hri_gclk_write_PCHCTRL_reg(GCLK, USART_SERCOM_GCLK_ID_CORE, USART_CONSOLE_GLCK_CORE_GEN | (1 << GCLK_PCHCTRL_CHEN_Pos));
	
	///* Initialize I2C_MASTER_SERCOM as I2C master
	///* Check if no software reset of I2C_MASTER_SERCOM is in progress
	if (!hri_sercomusart_is_syncing(USART_SERCOM, SERCOM_USART_SYNCBUSY_SWRST))
	{
		///* Perform a software reset of I2C_MASTER_SERCOM
		hri_sercomusart_write_CTRLA_reg(USART_SERCOM, SERCOM_USART_CTRLA_SWRST);
	}
	///* Wait until the software reset is finished
	hri_sercomusart_wait_for_sync(USART_SERCOM, SERCOM_USART_SYNCBUSY_SWRST);
	
	
	hri_sercomusart_set_CTRLA_reg(USART_SERCOM,
	1 << SERCOM_USART_CTRLA_DORD_Pos
	| 0 << SERCOM_USART_CTRLA_CMODE_Pos
	| 0x0 << SERCOM_USART_CTRLA_FORM_Pos
	| 0x0 << SERCOM_USART_CTRLA_SAMPA_Pos
	| 0x1 << SERCOM_USART_CTRLA_RXPO_Pos
	| 0x0 << SERCOM_USART_CTRLA_TXPO_Pos
	| 0x0 << SERCOM_USART_CTRLA_SAMPR_Pos
	| 0 << SERCOM_USART_CTRLA_RXINV_Pos
	| 0 << SERCOM_USART_CTRLA_TXINV_Pos
	| 0 << SERCOM_USART_CTRLA_IBON_Pos
	| 0 << SERCOM_USART_CTRLA_RUNSTDBY_Pos
	| 0x1 << SERCOM_USART_CTRLA_MODE_Pos);
	
	hri_sercomusart_set_CTRLB_reg(USART_SERCOM,
	0x0 << SERCOM_USART_CTRLB_LINCMD_Pos
	| 1 << SERCOM_USART_CTRLB_RXEN_Pos
	| 1 << SERCOM_USART_CTRLB_TXEN_Pos
	| 0 << SERCOM_USART_CTRLB_SBMODE_Pos
	| 0x0 << SERCOM_USART_CTRLB_CHSIZE_Pos);
	
	hri_sercomusart_set_CTRLC_reg(USART_SERCOM,
	0x0 << SERCOM_USART_CTRLC_DATA32B_Pos);
	
	hri_sercomusart_set_BAUD_BAUD_bf(USART_SERCOM, USART_BAUD);
	
	hri_sercomusart_set_INTEN_reg(USART_SERCOM,
	1 << SERCOM_USART_INTENSET_ERROR_Pos
	| 1 << SERCOM_USART_INTENSET_RXC_Pos
	| 0 << SERCOM_USART_INTENSET_DRE_Pos);
	
	hri_sercomusart_set_LENGTH_reg(USART_SERCOM,
	0x0 << SERCOM_USART_LENGTH_LENEN_Pos
	| 0x0);
	
	hri_sercomusart_set_DBGCTRL_reg(USART_SERCOM,
	0x0 << SERCOM_USART_DBGCTRL_DBGSTOP_Pos);
	
	
	NVIC_SetPriority(SERCOM2_0_IRQn, NVIC_PRIORITY_USART_CONSOLE_RX);
	NVIC_SetPriority(SERCOM2_1_IRQn, NVIC_PRIORITY_USART_CONSOLE_TX);
	NVIC_SetPriority(SERCOM2_2_IRQn, NVIC_PRIORITY_USART_CONSOLE_RX);
	NVIC_SetPriority(SERCOM2_3_IRQn, NVIC_PRIORITY_USART_CONSOLE_ERROR);
}
void usart_driver_enable(){
	sam5_ringbuffer_flush();
	
	///* Enable DRE interrupt
	NVIC_DisableIRQ(USART_DRE_IRQn);
	NVIC_ClearPendingIRQ(USART_DRE_IRQn);
	NVIC_EnableIRQ(USART_DRE_IRQn);
	///* Enable SB interrupt
	NVIC_DisableIRQ(USART_RXC_IRQn);
	NVIC_ClearPendingIRQ(USART_RXC_IRQn);
	NVIC_EnableIRQ(USART_RXC_IRQn);
	///* Enable ERROR interrupt
	NVIC_DisableIRQ(USART_ERROR_IRQn);
	NVIC_ClearPendingIRQ(USART_ERROR_IRQn);
	NVIC_EnableIRQ(USART_ERROR_IRQn);
	
	///* Enable TC OVF interrupt
	NVIC_DisableIRQ(TC3_IRQn);
	NVIC_ClearPendingIRQ(TC3_IRQn);
	NVIC_EnableIRQ(TC3_IRQn);
	
	hri_sercomusart_set_CTRLA_ENABLE_bit(USART_SERCOM);
}

void send_bytes(const uint8_t* buf, uint32_t count){
	#if USART_CONSOLE_AVAILABLE
	CRITICAL_SECTION_ENTER();
	for(uint32_t i = 0; i < count; ++i){
		sam5_ringbuffer_put(*(buf++));
	}
	if(sam5_ringbuffer_num() > 0){
		hri_sercomusart_set_INTEN_DRE_bit(USART_SERCOM);
	}
	CRITICAL_SECTION_LEAVE();
	#endif
}

static void (*_receive_callback)(char) = NULL;
void set_receive_callback(void (*receive_callback)(char)){
	_receive_callback = receive_callback;
}

void USART_DRE_HANDLER(void){
	CRITICAL_SECTION_ENTER();
	if(sam5_ringbuffer_num() > 0){
		char buf = sam5_ringbuffer_get();
		((Sercom *)USART_SERCOM)->USART.DATA.reg = 0x000000FF & buf;
	}
	else{
		hri_sercomusart_clear_INTEN_DRE_bit(USART_SERCOM);
	}
	CRITICAL_SECTION_LEAVE();
	hri_sercomusart_clear_INTFLAG_DRE_bit(USART_SERCOM);
}

void USART_RXC_HANDLER(void){
	uint8_t data = hri_sercomusart_read_DATA_DATA_bf(USART_SERCOM);
	if(_receive_callback!=NULL){
		_receive_callback(data);
	}
}

void USART_ERROR_HANDLER(void){
	hri_sercomusart_clear_INTFLAG_ERROR_bit(USART_SERCOM);
}

// _write implementation to make printf work with debug usart
_ssize_t _write(int fd, const void *buf, size_t count)
{
	if (fd == 1 || fd == 2)
	{
		send_bytes(buf, count);
		return (_ssize_t)count;
	}
	errno = EBADF;
	return -1;
}

// _read implementation to make printf work with debug usart
_ssize_t _read(int fd, void *buf, size_t count)
{
	if (fd == 0)
	{
		return 0;
	}
	errno = EBADF;
	return -1;
}