#include "sam5_usart_console_impl.h"
#include "commands/sam5_usart_console_commands.h"
#include "sam5_usart_driver.h"
#include "hardware_defines.h"

#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <arm_neon.h>

struct console_command g_commands[SAM5_USART_CONSOLE_MAX_NUM_COMMANDS];
uint32_t g_num_commands;

static char last_buffer[SAM5_USART_CONSOLE_BUFFER_LENGTH + 1];
static char buffer[SAM5_USART_CONSOLE_BUFFER_LENGTH + 1];
static uint32_t next_buffer_pos;
static uint8_t output_enabled = 0;

void sam5_usart_console_output_init(){
	if(output_enabled){
		return;
	}
	usart_driver_init();
	usart_driver_enable();
	output_enabled = 1;
}

#define SAM5_USART_CONSOLE_COMMAND_(name) sam5_usart_console_register_command(#name, sam5_usart_console_##name##_desc, sam5_usart_console_##name##_help, sam5_usart_console_##name);
#define SAM5_USART_CONSOLE_COMMAND(name) SAM5_USART_CONSOLE_COMMAND_(name)

void sam5_usart_console_input_init(){
	sam5_usart_console_output_init();
	set_receive_callback(rx_callback);
	next_buffer_pos = 0;
	buffer[SAM5_USART_CONSOLE_BUFFER_LENGTH] = 0; // Set very last char to zero to avoid any out of bound read/write
	
	SAM5_USART_CONSOLE_COMMAND(help)
	SAM5_USART_CONSOLE_COMMAND(reset)
	SAM5_USART_CONSOLE_COMMAND(stop)
	SAM5_USART_CONSOLE_COMMAND(s)
	SAM5_USART_CONSOLE_COMMAND(status)
	#if BMP3_AVAILABLE
	SAM5_USART_CONSOLE_COMMAND(bmp3)
	#endif
	SAM5_USART_CONSOLE_COMMAND(i2c)
	SAM5_USART_CONSOLE_COMMAND(main)
	SAM5_USART_CONSOLE_COMMAND(finger)
	SAM5_USART_CONSOLE_COMMAND(grasp)
	SAM5_USART_CONSOLE_COMMAND(motor)
	
	printf("USART console initialized - Use \"help\" command for usage information\r\n");
}

void sam5_usart_console_register_command(const char* command_name, const char* description, const char* help_text, uint8_t (*callback)(const int, const char **)){
	if(g_num_commands >= SAM5_USART_CONSOLE_MAX_NUM_COMMANDS){
		printf("Error registering new console command \"%s\" - Maximum number of commands reached\r\n", command_name);
		return;
	}
	if(strlen(command_name) > SAM5_USART_CONSOLE_MAX_PARAMETER_LENGTH){
		printf("Error registering new console command \"%s\" - Command name to long\r\n", command_name);
		return;
	}
	for(uint32_t i = 0; i < g_num_commands; ++i){
		if(strcmp(g_commands[i].name, command_name) == 0){
			printf("Error registering new console command \"%s\" - Duplicate name \r\n", command_name);
			return;
		}
	}
	struct console_command* cmd = g_commands + g_num_commands;
	g_num_commands += 1;
	strcpy((char *)cmd->name, command_name);
	cmd->description = description;
	cmd->help_text = help_text;
	cmd->callback = callback;
}

static uint8_t arrow_stage = 0;

void rx_callback(char received){
	switch(arrow_stage){
		default:
		switch(received){
			
			// Ctrl + L
			case 12:
			send_bytes((uint8_t *) &received, 1);
			send_bytes((const uint8_t *)"\r\n>", 3);
			send_bytes((uint8_t *) &buffer, next_buffer_pos);
			break;
			
			// Different multibyte special symbols like arrow keys
			case 27:
			arrow_stage = 1;
			break;
			
			// Ctrl + C and Ctrl + D
			case 3:
			case 4:
			sam5_usart_console_await_next_command();
			break;
			
			// New Line
			case '\r':
			case '\n':{
				for(uint32_t i = 0; i < SAM5_USART_CONSOLE_BUFFER_LENGTH; ++i){
					last_buffer[i] = buffer[i];
				}
				if(!execute_command()){
					sam5_usart_console_await_next_command();
				}
				break;
			}
			
			// Delete
			case 127: {
				if(next_buffer_pos > 0){
					send_bytes((uint8_t *) &received, 1);
					--next_buffer_pos;
					buffer[next_buffer_pos] = 0;
				}
				break;
			}
			
			// Normal Character
			default: {
				if(next_buffer_pos < SAM5_USART_CONSOLE_BUFFER_LENGTH){
					send_bytes((uint8_t *) &received, 1);
					if(received >= 'A' && received <= 'Z'){
						buffer[next_buffer_pos] = received + 32; // Switch capital letters to lower case
					}
					else{
						buffer[next_buffer_pos] = received;
					}
					++next_buffer_pos;
				}
				break;
			}
		}
		break;
		case 1:
		{
			if(received == 91){
				arrow_stage = 2;
				break;
			}
			else{
				char first_stage = 27;
				send_bytes((uint8_t *) &first_stage, 1);
				send_bytes((uint8_t *) &received, 1);
				arrow_stage = 0;
			}
		}
		break;
		case 2:
		{
			switch(received){
				case 65: // Up
				for(uint32_t i = 0; i < next_buffer_pos; ++i){
					// Delete currently written stuff
					char tmp = 127;
					send_bytes((uint8_t *) &tmp, 1);
				}
				next_buffer_pos = UINT32_MAX;
				for(uint32_t i = 0; i < SAM5_USART_CONSOLE_BUFFER_LENGTH; ++i){
					buffer[i] = last_buffer[i];
					if(next_buffer_pos == UINT32_MAX && buffer[i] == 0){
						next_buffer_pos = i;
					}
				}
				send_bytes((uint8_t *)buffer, next_buffer_pos);
				break;
				case 66: // Down
				case 67: // Right
				case 68: // Left
				// Suppress down/right/left cursor movements
				break;
				default:
				{
					char tmp[3] = {27, 91, received};
					send_bytes((uint8_t *) &tmp, 3);
				}
				break;
			}
			arrow_stage = 0;
			break;
		}
	}
}

uint8_t execute_command(){
	printf("\r\n");
	char parameters[SAM5_USART_CONSOLE_MAX_NUM_PARAMETERS + 1][SAM5_USART_CONSOLE_MAX_PARAMETER_LENGTH] = {0};
	uint32_t argc = 0;
	uint32_t search_start_index = 0;
	for (uint32_t i = 0; i < 10; ++i){
		uint32_t len = find_first_parameter(20, &search_start_index, parameters[i]);
		if (len > 0){
			++argc;
		}
		else {
			break;
		}
	}
	struct console_command* cmd = find_matching_command(parameters[0]);

	if(cmd == NULL){
		printf("Can't find command with name \"%s\"\r\n>", parameters[0]);
		return 0;
	}
	const char *argv[SAM5_USART_CONSOLE_MAX_NUM_PARAMETERS + 1];
	for (int i = 0; i < SAM5_USART_CONSOLE_MAX_NUM_PARAMETERS; ++i){
		argv[i] = parameters[i];
	}
	return cmd->callback(argc, argv);
}

void sam5_usart_console_await_next_command(){
	next_buffer_pos = 0;
	for(int i = 0; i < SAM5_USART_CONSOLE_BUFFER_LENGTH; ++i){
		buffer[i] = 0;
	}
	send_bytes((const uint8_t *)"\r\n>", 3);
}

uint32_t find_first_parameter(const uint32_t max_parameter_length, uint32_t *search_start_index, char *parameter){
	uint32_t parameter_start = *search_start_index;
	for (; parameter_start < next_buffer_pos; ++parameter_start){
		if (buffer[parameter_start] != ' ' && buffer[parameter_start] != '\t'){
			break;
		}
	}

	const uint32_t parameter_last_end = next_buffer_pos > parameter_start + max_parameter_length
	? parameter_start + max_parameter_length
	: next_buffer_pos;
	uint32_t parameter_end = parameter_start;
	for (; parameter_end < parameter_last_end; ++parameter_end){
		if (buffer[parameter_end] == ' ' || buffer[parameter_end] == '\t'){
			break;
		}
	}
	*search_start_index = parameter_end;
	for (uint32_t i = parameter_start; i < parameter_end; ++i){
		parameter[i - parameter_start] = buffer[i];
	}
	return parameter_end - parameter_start;
}

struct console_command* find_matching_command(const char* name){
	for(uint32_t i = 0; i < g_num_commands; ++i){
		if(strcmp(name, g_commands[i].name) == 0){
			return &(g_commands[i]);
		}
	}
	return NULL;
}

uint8_t parse_bool_parameter(const char* parameter, uint8_t* value){
	if(strlen(parameter) <= 0){
		return 0;
	}
	if(strcmp(parameter, "true") == 0){
		*value = 1;
		return 1;
	}
	else if(strcmp(parameter, "True") == 0){
		*value = 1;
		return 1;
	}
	else if(strcmp(parameter, "1") == 0){
		*value = 1;
		return 1;
	}
	else if(strcmp(parameter, "false") == 0){
		*value = 0;
		return 1;
	}
	else if(strcmp(parameter, "False") == 0){
		*value = 0;
		return 1;
	}
	else if(strcmp(parameter, "0") == 0){
		*value = 0;
		return 1;
		}else{
		return 0;
	}
}

uint8_t parse_integer_parameter(const char* parameter, int32_t* value){
	char * str_end;
	*value = strtol(parameter, &str_end, 0);
	return str_end == parameter + strlen(parameter);
}

uint8_t parse_float_parameter(const char* parameter, float32_t* value){
	char * str_end;
	*value = strtof(parameter, &str_end);
	return str_end == parameter + strlen(parameter);
}

void print_float(const char *pre_string, float32_t value, const char *post_string){
	char sign = ' ';
	if(value < 0){
		value *= -1;
		sign = '-';
	}
	int32_t full = (int32_t) value;
	uint32_t decimal = (int32_t)((value - full) * 1000 * 1000);
	if(decimal > 100 * 1000){
		printf("%s%c%ld.%lu%s", pre_string, sign, full, decimal, post_string);
	}
	else if(decimal > 10 * 1000){
		printf("%s%c%ld.0%lu%s", pre_string, sign, full, decimal, post_string);
	}
	else if(decimal > 1000){
		printf("%s%c%ld.00%lu%s", pre_string, sign, full, decimal, post_string);
	}
	else if(decimal > 100){
		printf("%s%c%ld.000%lu%s", pre_string, sign, full, decimal, post_string);
	}
	else if(decimal > 10){
		printf("%s%c%ld.0000%lu%s", pre_string, sign, full, decimal, post_string);
	}
	else{
		printf("%s%c%ld.00000%lu%s", pre_string, sign, full, decimal, post_string);
	}
}
