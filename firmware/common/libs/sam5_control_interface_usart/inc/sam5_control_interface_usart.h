#ifndef SAM5_USART_CONTROL_INTERFACE_H_
#define SAM5_USART_CONTROL_INTERFACE_H_

void sam5_usart_control_interface_init();
void sam5_usart_control_interface_start();

#endif //SAM5_USART_CONTROL_INTERFACE_H_