#include "sam5_control_interface_usart_impl.h"
#include "motor_protocol.h"
#include "main_control.h"
#include "sam5_i2c_master.h"
#include "sam5_bmp3_wrapper.h"

#include <atmel_start.h>
#include <string.h>
#include <stdio.h>

#define MAX_PAYLOAD_LENGTH	64

static void cb_rx_usart_control(const struct usart_async_descriptor *const io_descr);
static const uint8_t reset_freq[] = {0xff, 0x00, 0xfe, 0x01, 0xfd, 0x02, 0xfc, 0x03};
static uint8_t reset_pos = 0;
static struct io_descriptor *usart_io;
static main_control_input_t input;
static uint8_t rx_payload_buffer[MAX_PAYLOAD_LENGTH];
static uint8_t tx_buffer[MAX_PAYLOAD_LENGTH + 2]; // Make space for the header
static uint8_t* const tx_command = &tx_buffer[0];
static uint8_t* const tx_payload_length = &tx_buffer[1];
static uint8_t* const tx_payload_buffer = &tx_buffer[2];
static sam5_usart_control_request_e current_request;
static uint8_t remaining_payload_length;
static uint8_t received_bytes;


void sam5_usart_control_interface_init(){
	USART_CONTROL_init();
	
	usart_async_register_callback(&USART_CONTROL, USART_ASYNC_RXC_CB, cb_rx_usart_control);
	usart_async_get_io_descriptor(&USART_CONTROL, &usart_io);

	for(uint8_t i = 0; i < MAIN_CONTROL_NUM_MOTORS; ++i){
		const main_control_motor_t* motor = &g_main_control.motors[i];
		const sam5_i2c_master_com_job_t job = {
			.command = MOTOR_PROTOCOL_BUILD_COMMAND(motor->motor_index, MOTOR_PROTOCOL_READ_STATE),
			.slave_address = motor->i2c_address,
			.direction = I2C_MASTER_DIR_SLAVE_TO_MASTER,
			.num_bytes_to_transfer = sizeof(dc_motor_controller_state_t),
			.callback = &main_control_motor_i2c_callback_read_state,
			.period_ms = 50,
		};
		sam5_i2c_master_register_com_job(&job);
	}
	input.type = CONTROL_INPUT_TYPE_STOP;
	current_request = USART_CONTROL_REQ_NONE;
}

void sam5_usart_control_interface_start(){
	usart_async_enable(&USART_CONTROL);
}

static void cb_rx_usart_control(const struct usart_async_descriptor *const io_descr)
{
	uint8_t data;
	if(io_read(usart_io, &data, 1) > 0){
		if(data == reset_freq[reset_pos]){
			++reset_pos;
		}
		else if (data == reset_freq[0]){
			reset_pos = 1;
		}
		else {
			reset_pos = 0;
		}
		if(reset_pos == sizeof(reset_freq)){
			current_request = USART_CONTROL_REQ_NONE;
			remaining_payload_length = 0;
			received_bytes = 0;
			reset_pos = 0;
			return;
		}
		if(current_request == USART_CONTROL_REQ_NONE){
			current_request = data;
			return;
		}
		if(remaining_payload_length == 0){
			remaining_payload_length = data;
		}
		else{
			rx_payload_buffer[received_bytes++] = data;
			--remaining_payload_length;
		}
		if(remaining_payload_length == 0){
			*tx_command = USART_CONTROL_ASW_FROM_REQ(current_request);
			uint8_t success = 1;
			switch(current_request){
				case USART_CONTROL_REQ_STATE:{
					memset(tx_payload_buffer, 0, 20);
					*tx_payload_length = 20;
				}
				break;
				case USART_CONTROL_REQ_TACTILE:{
					if(received_bytes != 1){
						success = 0;
						break;
					}
					if(rx_payload_buffer[0] < SAM5_BMP3_MAX_NUMBER_SENSORS){
						memcpy(tx_payload_buffer, (uint8_t*)&sensor_array[rx_payload_buffer[0]].force_newton, sizeof(float32_t));
						*tx_payload_length = sizeof(float32_t);
					}
					else{
						for(uint8_t i = 0; i < SAM5_BMP3_MAX_NUMBER_SENSORS; ++i){
							memcpy(&tx_payload_buffer[i * sizeof(float32_t)], (uint8_t*)&sensor_array[i].force_newton, sizeof(float32_t));
						}
						*tx_payload_length = SAM5_BMP3_MAX_NUMBER_SENSORS * sizeof(float32_t);
					}
				}
				break;
				case USART_CONTROL_REQ_NUM_GRASPS:{
					if(received_bytes != 0){
						success = 0;
						break;
					}
					*tx_payload_length = 1;
					tx_payload_buffer[0] = main_control_num_grasps;
				}
				break;
				case USART_CONTROL_REQ_GRASP_NAME:{
					if(received_bytes != 1){
						success = 0;
						break;
					}
					const main_control_grasp_t* grasp = main_control_get_grasp(rx_payload_buffer[0]);
					if(grasp == NULL){
						success = 0;
						break;
					}
					*tx_payload_length = strlen(grasp->name);
					memcpy(tx_payload_buffer, grasp->name, *tx_payload_length);
				}
				break;
				case USART_CONTROL_REQ_GEAR_RATIO:{
					if(received_bytes != 1){
						success = 0;
						break;
					}
					if(rx_payload_buffer[0] < MAIN_CONTROL_NUM_MOTORS){
						memcpy(tx_payload_buffer, (uint8_t*)&g_main_control.motors[rx_payload_buffer[0]].gear_ratio, sizeof(float32_t));
						*tx_payload_length = sizeof(float32_t);
					}
					else{
						for(uint8_t i = 0; i < MAIN_CONTROL_NUM_MOTORS; ++i){
							memcpy(&tx_payload_buffer[i * sizeof(float32_t)], (uint8_t*)&g_main_control.motors[i].gear_ratio, sizeof(float32_t));
						}
						*tx_payload_length = MAIN_CONTROL_NUM_MOTORS * sizeof(float32_t);
					}
				}
				break;
				case USART_CONTROL_REQ_POSITION:{
					if(received_bytes != 1){
						success = 0;
						break;
					}
					if(rx_payload_buffer[0] < MAIN_CONTROL_NUM_MOTORS){
						const main_control_motor_t*const motor = &g_main_control.motors[rx_payload_buffer[0]];
						float32_t buf = main_control_convert_position_to_percentage(motor, motor->state.angle);
						memcpy(tx_payload_buffer, &buf, sizeof(float32_t));
						*tx_payload_length = sizeof(float32_t);
					}
					else{
						for(uint8_t i = 0; i < MAIN_CONTROL_NUM_MOTORS; ++i){
							const main_control_motor_t*const motor = &g_main_control.motors[i];
							float32_t buf = main_control_convert_position_to_percentage(motor, motor->state.angle);
							memcpy(tx_payload_buffer, &buf, sizeof(float32_t));
						}
						*tx_payload_length = MAIN_CONTROL_NUM_MOTORS * sizeof(float32_t);
					}
				}
				break;
				case USART_CONTROL_REQ_VELOCITY:{
					if(received_bytes != 1){
						success = 0;
						break;
					}
					if(rx_payload_buffer[0] < MAIN_CONTROL_NUM_MOTORS){
						memcpy(tx_payload_buffer, (uint8_t*)&g_main_control.motors[rx_payload_buffer[0]].state.velocity, sizeof(float32_t));
						*tx_payload_length = sizeof(float32_t);
					}
					else{
						for(uint8_t i = 0; i < MAIN_CONTROL_NUM_MOTORS; ++i){
							memcpy(&tx_payload_buffer[i * sizeof(float32_t)], (uint8_t*)&g_main_control.motors[i].state.velocity, sizeof(float32_t));
						}
						*tx_payload_length = MAIN_CONTROL_NUM_MOTORS * sizeof(float32_t);
					}
				}
				break;
				case USART_CONTROL_REQ_CURRENT:{
					if(received_bytes != 1){
						success = 0;
						break;
					}
					if(rx_payload_buffer[0] < MAIN_CONTROL_NUM_MOTORS){
						memcpy(tx_payload_buffer, (uint8_t*)&g_main_control.motors[rx_payload_buffer[0]].state.current, sizeof(float32_t));
						*tx_payload_length = sizeof(float32_t);
					}
					else{
						for(uint8_t i = 0; i < MAIN_CONTROL_NUM_MOTORS; ++i){
							memcpy(&tx_payload_buffer[i * sizeof(float32_t)], (uint8_t*)&g_main_control.motors[i].state.current, sizeof(float32_t));
						}
						*tx_payload_length = MAIN_CONTROL_NUM_MOTORS * sizeof(float32_t);
					}
				}
				break;
				case USART_CONTROL_REQ_VOLTAGE:{
					if(received_bytes != 1){
						success = 0;
						break;
					}
					if(rx_payload_buffer[0] < MAIN_CONTROL_NUM_MOTORS){
						memcpy(tx_payload_buffer, (uint8_t*)&g_main_control.motors[rx_payload_buffer[0]].state.voltage, sizeof(float32_t));
						*tx_payload_length = sizeof(float32_t);
					}
					else{
						for(uint8_t i = 0; i < MAIN_CONTROL_NUM_MOTORS; ++i){
							memcpy(&tx_payload_buffer[i * sizeof(float32_t)], (uint8_t*)&g_main_control.motors[i].state.voltage, sizeof(float32_t));
						}
						*tx_payload_length = MAIN_CONTROL_NUM_MOTORS * sizeof(float32_t);
					}
				}
				break;
				case USART_CONTROL_REQ_TEMPERATURE:{
					if(received_bytes != 1){
						success = 0;
						break;
					}
					if(rx_payload_buffer[0] < MAIN_CONTROL_NUM_MOTORS){
						memcpy(tx_payload_buffer, (uint8_t*)&g_main_control.motors[rx_payload_buffer[0]].state.temperature, sizeof(float32_t));
						*tx_payload_length = sizeof(float32_t);
					}
					else{
						for(uint8_t i = 0; i < MAIN_CONTROL_NUM_MOTORS; ++i){
							memcpy(&tx_payload_buffer[i * sizeof(float32_t)], (uint8_t*)&g_main_control.motors[i].state.temperature, sizeof(float32_t));
						}
						*tx_payload_length = MAIN_CONTROL_NUM_MOTORS * sizeof(float32_t);
					}
				}
				break;
				case USART_CONTROL_REQ_GRASP:{
					if(received_bytes != 4){
						success = 0;
						break;
					}
					*tx_payload_length = 0;
					input.type = CONTROL_INPUT_TYPE_GRASP;
					input.grasp_index = rx_payload_buffer[0];
					input.grasp_speed_factor = rx_payload_buffer[1];
					input.grasp_contact_current_factor = rx_payload_buffer[2];
					input.grasp_no_auto_continue = rx_payload_buffer[3] != 0;
					main_control_set_input(CONTROL_INPUT_SOURCE_USART, &input);
					printf("Executing Grasp\r\n");
				}
				break;
				case USART_CONTROL_REQ_FINGER:{
					if(received_bytes != 14){
						success = 0;
						break;
					}
					*tx_payload_length = 0;
					input.type = CONTROL_INPUT_TYPE_DIRECT;
					input.direct_finger_index = rx_payload_buffer[0];
					memcpy(&input.direct_input.angle, &rx_payload_buffer[1], sizeof(float32_t));
					memcpy(&input.direct_input.velocity, &rx_payload_buffer[5], sizeof(float32_t));
					memcpy(&input.direct_input.current, &rx_payload_buffer[9], sizeof(float32_t));
					if(rx_payload_buffer[13] & (1 << 1)){
						input.direct_input.flags.control_mode = MOTOR_CM_CURRENT;
					}
					else if(rx_payload_buffer[13] & (1 << 0)){
						input.direct_input.flags.control_mode = MOTOR_CM_VELOCITY;
					}
					else {
						input.direct_input.flags.control_mode = MOTOR_CM_POSITION;
					}
					input.direct_input.flags.continue_at_end = (rx_payload_buffer[13] & (1 << 2)) != 0;
					input.direct_input.flags.stop_at_contact = (rx_payload_buffer[13] & (1 << 3)) != 0;
					input.direct_input.flags.use_aggressive_contact_detection = (rx_payload_buffer[13] & (1 << 4)) != 0;
					
					main_control_set_input(CONTROL_INPUT_SOURCE_USART, &input);
					printf("Executing Finger %d - %x\r\n", input.direct_finger_index, *((uint8_t*) &input.direct_input.flags));
				}
				break;
				case USART_CONTROL_REQ_STOP:{
					input.type = CONTROL_INPUT_TYPE_STOP;
					main_control_set_input(CONTROL_INPUT_SOURCE_USART, &input);
					main_control_set_active_input(CONTROL_INPUT_SOURCE_USART);
				}
				break;
				default:
				success = 0;
				break;
			}
			if(success){
				io_write(usart_io, tx_buffer, *tx_payload_length + 2);
			}
			current_request = USART_CONTROL_REQ_NONE;
			remaining_payload_length = 0;
			received_bytes = 0;
		}
	}
}
