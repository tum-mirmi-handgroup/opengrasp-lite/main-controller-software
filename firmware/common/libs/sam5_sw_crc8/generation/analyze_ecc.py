import itertools
import random
from typing import Tuple, Dict, Iterable

from crc8 import CRC8, algorithms
from representation import Representation

hd3_length: Dict[Representation, int] = {
    Representation.from_normal(0x7f): 119,
    Representation.from_normal(0x2f): 119,
    Representation.from_normal(0xa7): 119,
    Representation.from_normal(0x9b): 119,
    Representation.from_normal(0x07): 119,
    Representation.from_normal(0x31): 119,
    Representation.from_normal(0x49): 97,
    Representation.from_normal(0xd5): 85,
    Representation.from_normal(0x1d): 247,
    Representation.from_normal(0xcf): 247,
    Representation.from_normal(0x4d): 247,
    Representation.from_normal(0x63): 247,
    Representation.from_normal(0x39): 9,
}

hd4_length: Dict[Representation, int] = {
    Representation.from_normal(0x7f): 119,
    Representation.from_normal(0x2f): 119,
    Representation.from_normal(0xa7): 119,
    Representation.from_normal(0x9b): 119,
    Representation.from_normal(0x07): 119,
    Representation.from_normal(0x31): 119,
    Representation.from_normal(0x49): 97,
    Representation.from_normal(0xd5): 85,
    Representation.from_normal(0xcf): 19,
    Representation.from_normal(0x4d): 15,
    Representation.from_normal(0x1d): 13,
    Representation.from_normal(0x63): 12,
    Representation.from_normal(0x39): 9,
}


def analyze_algo(algo: CRC8) -> Tuple[Dict[int, int], Dict[int, str], int]:
    num_bytes = hd3_length[algo.poly] // 8
    counts: Dict[int, int] = {i: 0 for i in range(9)}
    result_entries: Dict[int, str] = {}
    crc_zeros = algo.calc(bytes(num_bytes))
    for i in range(num_bytes * 8):
        byte_num = i // 8
        bit_num = i % 8
        data: bytes = bytes(byte_num) + (1 << bit_num).to_bytes(1, "big") + bytes(num_bytes - byte_num - 1)
        crc_raw = algo.calc(data)
        crc = crc_raw ^ crc_zeros
        count = crc.bit_count()
        counts[count] += 1
        result_entries[i] = f"{crc:08b}"
    assert len(set(result_entries.values())) == len(result_entries.values())
    return counts, result_entries, num_bytes


def calc_hamming_distance(result_entries: Iterable[str]) -> Tuple[int, float]:
    converted = [int(res, 2) for res in result_entries]
    distances = []
    for a, b in itertools.product(converted, converted):
        if a == b:
            continue
        else:
            # The result entries are calculated in such a manner, that the data bitstream will always differ 2 bits
            distance = (a ^ b).bit_count() + 2
            distances.append(distance)
    return min(distances), sum(distances) / len(distances)


def find_best_custom_algo(polynomials: Iterable[Representation]):
    algos: Dict[float, CRC8] = {}
    for poly in polynomials:
        algo = CRC8(
            poly=poly,
            init=0xFF,
            refin=False,
            refout=False,
            xor_out=0x00,
            check=None
        )
        counts, result_entries, _ = analyze_algo(algo)
        if counts[0] == 0 and counts[1] == 0:
            _, average_hamming = calc_hamming_distance(result_entries.values())
            algos[average_hamming] = algo
    key = max(algos.keys())
    print_algorithm(algos[key])


def find_best_custom_algo_hd3():
    filtered_polynomials = {poly for poly, length in hd3_length.items() if length == max(hd3_length.values())}
    find_best_custom_algo(filtered_polynomials)


def find_best_custom_algo_hd4():
    filtered_polynomials = {poly for poly, length in hd4_length.items() if length == max(hd4_length.values())}
    find_best_custom_algo(filtered_polynomials)


def analyze_existing_algorithms():
    for algo_name, algorithm in algorithms.items():
        assert isinstance(algorithm, CRC8)
        assert algorithm.poly in hd3_length.keys(), f"{algorithm.poly:02x}"

        print("#" * 100)
        print(
            f"{algo_name:10s} | 0x1{algorithm.poly:02X} | {hd3_length[algorithm.poly]} | {algorithm.refin} |"
            f" 0x{algorithm.init:02X} | 0x{algorithm.xor_out:02X}")
        counts, _, _ = analyze_algo(algorithm)
        for key, value in counts.items():
            if value > 0:
                print(f"{key:<3d}: {value}")


def print_algorithm(algo: CRC8):
    counts, result_entries, ecc_bytes = analyze_algo(algo)
    min_hamming, average_hamming = calc_hamming_distance(result_entries.values())
    print(f"Algorithm: (HD: min={min_hamming}, avg={average_hamming})")
    print(f"\tPoly: {algo.poly.print_normal_short()}")
    print(f"\tInit: 0x{algo.init:02x}")
    print(f"\t^Out: 0x{algo.xor_out:02x}")
    print(f"\tRefI: {algo.refin}")
    print(f"\tRefO: {algo.refout}")
    print(f"\t#ECC: {ecc_bytes} bytes")
    print("\tBit Counts")
    for key, value in counts.items():
        if value > 0:
            print(f"\t\t{key:d}: {value}")
    algo.print_table(14)
    print_ecc_table(algo, ecc_bytes, 14)


def print_ecc_table(algo: CRC8, ecc_bytes: int, bytes_per_line: int):
    _, result_entries, _ = analyze_algo(algo)
    assert len(result_entries) == len(set(result_entries))
    result_dict = {int(crc, 2): bit_pos for bit_pos, crc in result_entries.items()}
    for possible_crc in range(256):
        if possible_crc not in result_dict.keys():
            if possible_crc.bit_count() <= 1:
                result_dict[possible_crc] = 0xF0
            else:
                result_dict[possible_crc] = 0xFF

    values = [bit_pos for bit_pos in result_dict.values() if bit_pos not in (0xF0, 0xFF)]
    ignored_errors = [bit_pos for bit_pos in result_dict.values() if bit_pos == 0xF0]
    assert len(values) == ecc_bytes * 8
    assert all([i in values for i in range(1, ecc_bytes * 8)])
    assert len(ignored_errors) == 9

    print("const uint8_t sam5_sw_crc8_EccTable[256] = {\r\n\t", end="")
    for i, (key, value) in enumerate(sorted(result_dict.items())):
        assert i == key
        if i > 0 and i % bytes_per_line == 0:
            print("\r\n\t", end="")
        print(f"0x{value:02X}", end="")
        if i != 255:
            print(", ", end="")
    print("\r\n};")
    random.seed(1)
    for _ in range(1000):
        length = ecc_bytes
        bit = random.randrange(0, length * 8)
        crc_pre = algo.calc(bytes(length))
        flipped: bytes = bytes(bit // 8) + (1 << (bit % 8)).to_bytes(1, "big") + bytes(length - bit // 8 - 1)
        crc_post = algo.calc(flipped)
        assert len(flipped) == length
        assert crc_post != crc_pre
        assert result_dict[crc_post ^ crc_pre] == bit, \
            f"{crc_post ^ crc_pre} -> {result_dict[crc_post ^ crc_pre]} != {bit}"


if __name__ == '__main__':
    print("Created CRC8 algorithm specifically for this use case")
    find_best_custom_algo_hd4()
    find_best_custom_algo_hd3()
