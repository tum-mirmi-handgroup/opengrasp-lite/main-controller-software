import math
import re
from dataclasses import dataclass
from typing import Union


@dataclass(frozen=True)
class Representation:
    all_bits: int
    size: int

    def __xor__(self, other):
        if isinstance(other, int):
            return (self.all_bits % 256) ^ other
        elif isinstance(other, Representation):
            return (self.all_bits ^ other.all_bits) % 256
        else:
            raise TypeError("Can't XOR representation")

    def __rxor__(self, other):
        return self.__xor__(other)

    @staticmethod
    def _data_to_int(data: Union[str, int]) -> int:
        if isinstance(data, int):
            return data

        data = data.strip().lower()
        if re.match(r"^0b[01]+$", data):
            return int(data[2:], 2)
        if re.match(r"^\d+$", data):
            return int(data)
        if re.match(r"^0?x[0123456789abcdef]+$", data):
            return int(data[2:], 16)
        raise ValueError

    @staticmethod
    def from_all_bits(data: Union[str, int], size: int) -> "Representation":
        val = Representation._data_to_int(data)
        assert len(bin(val)[2:]) == size + 1
        return Representation(val, size)

    @staticmethod
    def from_normal(data: Union[str, int], size: int = 8):
        data = bin(Representation._data_to_int(data))[2:]
        return Representation.from_all_bits(
            "0b1" + "0" * (size - len(data)) + data,
            size
        )

    @staticmethod
    def from_koopman(data: Union[str, int], size: int = 8):
        return Representation.from_all_bits(
            bin(Representation._data_to_int(data)) + "1",
            size
        )

    @staticmethod
    def from_reversed(data: Union[str, int], size: int = 8):
        return Representation.from_all_bits(
            "0b1" + bin(Representation._data_to_int(data))[2:][::-1],
            size
        )

    @staticmethod
    def from_reciprocal(data: Union[str, int], size: int = 8):
        data = bin(Representation._data_to_int(data))[2:][::-1]
        return Representation.from_all_bits(
            "0b" + data + "0" * (size - len(data)) + "1",
            size
        )

    def _all_bits(self) -> str:
        return bin(self.all_bits)[2:]

    def _print(self, name, bit_representation):
        num_bits = len(self._all_bits())
        num_hex = math.ceil(num_bits / 4)
        return "{{:<10s}} | {{:>{}s}} | {{:0{}x}}" \
            .format(num_bits, num_hex) \
            .format(name, bit_representation, int(bit_representation, 2))

    def print_all_bits(self) -> str:
        return self._print("All bits", self._all_bits())

    def print_normal(self) -> str:
        return self._print("Normal", self._all_bits()[1:])

    def print_normal_short(self) -> str:
        return "0x" + f"{self.all_bits:03x}"[1:]

    def print_koopman(self) -> str:
        return self._print("Koopman", self._all_bits()[:-1])

    def print_reversed(self) -> str:
        return self._print("Reversed", self._all_bits()[1:][::-1])

    def print_reciprocal(self) -> str:
        return self._print("Reciprocal", self._all_bits()[:-1][::-1])

    def __str__(self):
        return "\r\n".join([
            self.print_all_bits(),
            self.print_normal(),
            self.print_koopman(),
            self.print_reversed(),
            self.print_reciprocal(),
            ""
        ])


if __name__ == '__main__':
    print(Representation.from_normal(0x07, size=8))
    print(Representation.from_normal(0x4d, size=8))
