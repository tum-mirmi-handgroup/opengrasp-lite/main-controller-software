#ifndef _SAM5_SW_CRC8_H_
#define _SAM5_SW_CRC8_H_

#include <stdint.h>

extern const uint8_t sam5_sw_crc8_CrcTable[256];
extern const uint8_t sam5_sw_crc8_EccTable[256];

/**
 * @brief Caclulate the CRC of a transmitted buffer apply ECC on it
 *
 * This will calculate the CRC of data using sam5_sw_crc8_CRC() and after that
 * get the flipped bit with sam5_sw_crc8_ECC().
 * If this is a fixable error apply the fix to data
 *
 * @param[in,out] uint8_t* data Pointer to the memory area of transmitted MemoryArea
 * (Excluding the CRC
 * @param[in] uint32_t length The length of the transmitted MemoryArea (Excluding the CRC)
 * @param[in] uint8_t the transmitted CRC
 * @return uint8_t Mask of the bits that were correct i the transmitted CRC if data is usable, 0 if not
 */
uint8_t sam5_sw_crc8_Applied(volatile uint8_t *data, volatile uint32_t length, uint8_t crcTransmitted);

/**
 * @brief Calculate the 8-bit cyclic redundancy check (CRC8)
 *
 * Use a table based approach to efficiently calculate the CRC8 in Software.
 * The CRC8 algorithm is fully described using the following parameters
 *
 * Polynom: 0xa6 (Koopman), 0x4d (Normal)
 * Init:    0xFF
 * Reflect: No (Input), No (Output)
 * XOR-Out: 0x00
 * Check:   0x06
 *
 * @param src Pointer to the memory area where the crc shall be calculated of
 * @param length Length of the memory area where the crc shall be calculated of
 */
uint8_t sam5_sw_crc8_CRC(volatile const uint8_t *src, volatile uint32_t length);

/**
 * @brief Calculate the CRC8 with a custom init value
 *
 * This can be used to continue a previous calculation with an additional memory area
 *
 * @param src Pointer to the memory area where the crc shall be calculated of
 * @param length Length of the memory area where the crc shall be calculated of
 * @param lastCRC The custom init value (Most likely the crc of the last memory area)
 */
uint8_t sam5_sw_crc8_CRC_continue(volatile const uint8_t *src, volatile uint32_t length, uint8_t lastCRC);

#define SAM5_CRC8_MAX_DATA_LENGTH 30

#endif //_SAM5_SW_CRC8_H_
