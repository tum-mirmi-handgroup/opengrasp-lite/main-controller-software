/**
* \file samd5_bmp3_wrapper.c
*
* samd_bmp3_wrapper for interfacing with the API of the family of Bosch BMP3XX sensors
* via the TCA9548a multiplexer
* Copyright (C) 2023 Michal Ciebielski, Malte Kaiser, Michael Ratzel

* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.

* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.

* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#include "sam5_bmp3_wrapper_impl.h"
#include "sam5_ws2812.h"
#include <atmel_start.h>
#include <stdlib.h>
#include <stdio.h>


static int8_t index_of_current_sensor = -1;						///< Global variable that keeps track of which sensor should be read next
static uint32_t sensor_comm_error_counter = 0;					///< Triggered if the sensor data is not finished reading during one cycle (max sensor frequency is 200 Hz)
static struct{
	uint8_t print_requested;
	uint8_t search_requested;
	uint8_t search_ongoing;
	uint8_t readout_ongoing;
} flags;
static void (*search_finished_callback)() = NULL;
static void (*print_finished_callback)() = NULL;

struct bmp3_data data = { 0 };				///< BMP3 data struct
struct bmp3_settings settings = { 0 };		///< BMP3 settings struct
struct bmp3_status status = { { 0 } };		///< BMP3 status struct

static const uint8_t ledErrorColor[3] = {0, 0, 127}; // LED colored Green, Red, Blue
static const uint8_t bmp3_reg_data = BMP3_REG_DATA;


/**
* \brief Global array for sensor structs to store data for each sensor.
*/
/**
* \brief Global arrays for the configuration data for each sensor
*
* For now these values shall be hardcoded into this struct by the user
*/
#if BMP3_MULTIPLEXER_AVAILABLE
bmp3_sensor_t sensor_array[SAM5_BMP3_MAX_NUMBER_SENSORS] = {
	{	// Channel 0 - 0x76
		.pressure_newton_conv_a0 = BMP3_SENSOR_CH0_76_CONV_A0,
		.pressure_newton_conv_a1 = BMP3_SENSOR_CH0_76_CONV_A1,
		.pressure_newton_conv_a2 = BMP3_SENSOR_CH0_76_CONV_A2,
		.led_pin = BMP3_SENSOR_CH0_76_LED_PIN
	},
	{	// Channel 0 - 0x77
		.pressure_newton_conv_a0 = BMP3_SENSOR_CH0_77_CONV_A0,
		.pressure_newton_conv_a1 = BMP3_SENSOR_CH0_77_CONV_A1,
		.pressure_newton_conv_a2 = BMP3_SENSOR_CH0_77_CONV_A2,
		.led_pin = BMP3_SENSOR_CH0_77_CONV_A2
	},
	{	// Channel 1 - 0x76
		.pressure_newton_conv_a0 = BMP3_SENSOR_CH1_76_CONV_A0,
		.pressure_newton_conv_a1 = BMP3_SENSOR_CH1_76_CONV_A1,
		.pressure_newton_conv_a2 = BMP3_SENSOR_CH1_76_CONV_A2,
		.led_pin = BMP3_SENSOR_CH1_76_LED_PIN
	},
	{	// Channel 1 - 0x77
		.pressure_newton_conv_a0 = BMP3_SENSOR_CH1_77_CONV_A0,
		.pressure_newton_conv_a1 = BMP3_SENSOR_CH1_77_CONV_A1,
		.pressure_newton_conv_a2 = BMP3_SENSOR_CH1_77_CONV_A2,
		.led_pin = BMP3_SENSOR_CH1_77_CONV_A2
	},
	{	// Channel 2 - 0x76
		.pressure_newton_conv_a0 = BMP3_SENSOR_CH2_76_CONV_A0,
		.pressure_newton_conv_a1 = BMP3_SENSOR_CH2_76_CONV_A1,
		.pressure_newton_conv_a2 = BMP3_SENSOR_CH2_76_CONV_A2,
		.led_pin = BMP3_SENSOR_CH2_76_LED_PIN
	},
	{	// Channel 2 - 0x77
		.pressure_newton_conv_a0 = BMP3_SENSOR_CH2_77_CONV_A0,
		.pressure_newton_conv_a1 = BMP3_SENSOR_CH2_77_CONV_A1,
		.pressure_newton_conv_a2 = BMP3_SENSOR_CH2_77_CONV_A2,
		.led_pin = BMP3_SENSOR_CH2_77_CONV_A2
	},
	{	// Channel 3 - 0x76
		.pressure_newton_conv_a0 = BMP3_SENSOR_CH3_76_CONV_A0,
		.pressure_newton_conv_a1 = BMP3_SENSOR_CH3_76_CONV_A1,
		.pressure_newton_conv_a2 = BMP3_SENSOR_CH3_76_CONV_A2,
		.led_pin = BMP3_SENSOR_CH3_76_LED_PIN
	},
	{	// Channel 3 - 0x77
		.pressure_newton_conv_a0 = BMP3_SENSOR_CH3_77_CONV_A0,
		.pressure_newton_conv_a1 = BMP3_SENSOR_CH3_77_CONV_A1,
		.pressure_newton_conv_a2 = BMP3_SENSOR_CH3_77_CONV_A2,
		.led_pin = BMP3_SENSOR_CH3_77_CONV_A2
	},
	{	// Channel 4 - 0x76
		.pressure_newton_conv_a0 = BMP3_SENSOR_CH4_76_CONV_A0,
		.pressure_newton_conv_a1 = BMP3_SENSOR_CH4_76_CONV_A1,
		.pressure_newton_conv_a2 = BMP3_SENSOR_CH4_76_CONV_A2,
		.led_pin = BMP3_SENSOR_CH4_76_LED_PIN
	},
	{	// Channel 4 - 0x77
		.pressure_newton_conv_a0 = BMP3_SENSOR_CH4_77_CONV_A0,
		.pressure_newton_conv_a1 = BMP3_SENSOR_CH4_77_CONV_A1,
		.pressure_newton_conv_a2 = BMP3_SENSOR_CH4_77_CONV_A2,
		.led_pin = BMP3_SENSOR_CH4_77_CONV_A2
	},
	{	// Channel 5 - 0x76
		.pressure_newton_conv_a0 = BMP3_SENSOR_CH5_76_CONV_A0,
		.pressure_newton_conv_a1 = BMP3_SENSOR_CH5_76_CONV_A1,
		.pressure_newton_conv_a2 = BMP3_SENSOR_CH5_76_CONV_A2,
		.led_pin = BMP3_SENSOR_CH5_76_LED_PIN
	},
	{	// Channel 5 - 0x77
		.pressure_newton_conv_a0 = BMP3_SENSOR_CH5_77_CONV_A0,
		.pressure_newton_conv_a1 = BMP3_SENSOR_CH5_77_CONV_A1,
		.pressure_newton_conv_a2 = BMP3_SENSOR_CH5_77_CONV_A2,
		.led_pin = BMP3_SENSOR_CH5_77_CONV_A2
	},
	{	// Channel 6 - 0x76
		.pressure_newton_conv_a0 = BMP3_SENSOR_CH6_76_CONV_A0,
		.pressure_newton_conv_a1 = BMP3_SENSOR_CH6_76_CONV_A1,
		.pressure_newton_conv_a2 = BMP3_SENSOR_CH6_76_CONV_A2,
		.led_pin = BMP3_SENSOR_CH6_76_LED_PIN
	},
	{	// Channel 6 - 0x77
		.pressure_newton_conv_a0 = BMP3_SENSOR_CH6_77_CONV_A0,
		.pressure_newton_conv_a1 = BMP3_SENSOR_CH6_77_CONV_A1,
		.pressure_newton_conv_a2 = BMP3_SENSOR_CH6_77_CONV_A2,
		.led_pin = BMP3_SENSOR_CH6_77_CONV_A2
	},
	{	// Channel 7 - 0x76
		.pressure_newton_conv_a0 = BMP3_SENSOR_CH7_76_CONV_A0,
		.pressure_newton_conv_a1 = BMP3_SENSOR_CH7_76_CONV_A1,
		.pressure_newton_conv_a2 = BMP3_SENSOR_CH7_76_CONV_A2,
		.led_pin = BMP3_SENSOR_CH7_76_LED_PIN
	},
	{	// Channel 7 - 0x77
		.pressure_newton_conv_a0 = BMP3_SENSOR_CH7_77_CONV_A0,
		.pressure_newton_conv_a1 = BMP3_SENSOR_CH7_77_CONV_A1,
		.pressure_newton_conv_a2 = BMP3_SENSOR_CH7_77_CONV_A2,
		.led_pin = BMP3_SENSOR_CH7_77_CONV_A2
	},
};
#else
bmp3_sensor_t sensor_array[SAM5_BMP3_MAX_NUMBER_SENSORS] = {
	{	// 0x76
		.pressure_newton_conv_a0 = 0.0,
		.pressure_newton_conv_a1 = 1.0,
		.pressure_newton_conv_a2 = 0.0,
		.led_pin = LED_UNUSED
	},
	{	// 0x77
		.pressure_newton_conv_a0 = 0.0,
		.pressure_newton_conv_a1 = 1.0,
		.pressure_newton_conv_a2 = 0.0,
		.led_pin = LED_UNUSED
	},

};
#endif


static void bmp3_delay_us(uint32_t period, void *intf_ptr);
static void sensor_reg_address_tx_complete_cb(uint8_t success);
static void sensor_data_rx_complete_cb(uint8_t success);

static void bmp3_configure_dev_struct(struct bmp3_dev *dev, struct bmp3_interface_desc_t *sensor);
static uint32_t bmp3_configure_dev_settings(struct bmp3_settings *settings);

static void start_next_active_sensor();
static void sensor_failed(int8_t index, uint8_t within_init);
static void print_status(bool on_init);
static void search_sensors(bool on_init, uint32_t num_search_rounds);


/**
* \brief Function to configure all sensors
*/
void sam5_bmp3_wrapper_init()
{
	bmp3_i2c_driver_initialize_peripherals();
	bmp3_i2c_driver_initialize_state_machine();
	
	///* Enable APB TC units APB communication
	hri_mclk_set_APBCMASK_TC4_bit(MCLK);
	hri_gclk_write_PCHCTRL_reg(GCLK, TC4_GCLK_ID, GCLK_PCHCTRL_GEN_GCLK0_Val | (1 << GCLK_PCHCTRL_CHEN_Pos));
	
	// Check if no software reset is ongoing
	if(!hri_tc_is_syncing(TC4, TC_SYNCBUSY_SWRST)){
		// If TC4 is enable -> disable it
		if(hri_tc_get_CTRLA_ENABLE_bit(TC4)){
			hri_tc_clear_CTRLA_ENABLE_bit(TC4);
		}
		// Start software reset
		hri_tc_set_CTRLA_SWRST_bit(TC4);
	}
	// Wait for software to finish
	hri_tc_wait_for_sync(TC4, TC_SYNCBUSY_SWRST);
	
	hri_tc_write_CTRLA_reg(TC4,
	0 << TC_CTRLA_CAPTEN0_Pos
	| 0 << TC_CTRLA_CAPTEN1_Pos
	| SAM5_BMP3_TC_PRESCALER << TC_CTRLA_PRESCALER_Pos
	| 0x0 << TC_CTRLA_MODE_Pos);
	
	hri_tc_write_CTRLB_reg(TC4,
	0 << TC_CTRLBSET_ONESHOT_Pos
	| 0 << TC_CTRLBSET_DIR_Pos);
	
	hri_tc_write_INTEN_reg(TC4,
	0 << TC_INTENSET_MC0_Pos
	| 0 << TC_INTENSET_MC1_Pos
	| 1 << TC_INTENSET_OVF_Pos);	// Enable Overflow interrupt
	
	hri_tc_write_WAVE_reg(TC4, 0x1 << TC_WAVE_WAVEGEN_Pos);	// Set wavegen to Match frequency (Overflow on match CC0)
	
	hri_tccount16_set_CC_CC_bf(TC4, 0, SAM5_BMP3_TC_CC0);
	
	NVIC_SetPriority(TC4_IRQn, NVIC_PRIORITY_BMP3_TC);
}

/**
* \brief Function that initializes timer task in Microchip API Master Asynchronous Driver.
*/
void sam5_bmp3_wrapper_start()
{
	flags.search_ongoing = 0;
	flags.search_requested = 0;
	flags.readout_ongoing = 0;
	flags.print_requested = 0;
	
	search_sensors(true, 5);
	
	NVIC_DisableIRQ(TC4_IRQn);
	NVIC_ClearPendingIRQ(TC4_IRQn);
	NVIC_EnableIRQ(TC4_IRQn);
	hri_tc_set_CTRLA_ENABLE_bit(TC4);
}

void sam5_bmp3_wrapper_loop(){
	if(!flags.readout_ongoing){
		if(flags.search_requested){
			search_sensors(false, 1);
			if(search_finished_callback != NULL){
				search_finished_callback();
				search_finished_callback = NULL;
			}
		}
		if(flags.print_requested){
			printf("Total Errors since start: %ld\r\n", sensor_comm_error_counter);
			print_status(false);
			if(print_finished_callback != NULL){
				print_finished_callback();
				print_finished_callback = NULL;
			}
		}
	}
}

void sam5_bmp3_wrapper_request_search_sensors(void (*finished_callback)()){
	if(flags.search_ongoing == 0){
		search_finished_callback = finished_callback;
		flags.search_requested = 1;
	}
}

void sam5_bmp3_wrapper_request_print_status(void (*finished_callback)()){
	flags.print_requested = 1;
	print_finished_callback = finished_callback;
}


/**
* \brief Wrapper function for BMP3 API delay
*/
static void bmp3_delay_us(uint32_t period, void *intf_ptr)
{
	delay_us(period);
}


/**
* \brief Function to configure BMP3 API sensor device struct
*
* \param dev BMP3 API sensor device struct
* \param interface_descriptor Struct containing multiplexer channel and sensor address of a singular sensor
*/
static void bmp3_configure_dev_struct(struct bmp3_dev *dev, struct bmp3_interface_desc_t *interface_descriptor)
{
	dev -> read = bmp3_i2c_driver_read_wrapper;
	dev -> write = bmp3_i2c_driver_write_wrapper;
	dev -> intf = BMP3_I2C_INTF;
	dev -> delay_us = bmp3_delay_us;
	dev -> intf_ptr = interface_descriptor;
}

/**
* \brief Function to configure the sensor settings, see BMP3 manual for details of each setting
*/
static uint32_t bmp3_configure_dev_settings(struct bmp3_settings *settings)
{
	uint32_t settings_sel;

	settings->int_settings.drdy_en = BMP3_ENABLE;
	settings->int_settings.latch = BMP3_ENABLE;
	settings->press_en = BMP3_ENABLE;
	settings->temp_en = BMP3_ENABLE;

	settings->odr_filter.press_os = BMP3_OVERSAMPLING_2X;
	settings->odr_filter.temp_os = BMP3_OVERSAMPLING_2X;
	settings->odr_filter.odr = BMP3_READOUT_FREQUENCY;

	settings_sel = BMP3_SEL_PRESS_EN | BMP3_SEL_TEMP_EN | BMP3_SEL_PRESS_OS | BMP3_SEL_TEMP_OS | BMP3_SEL_ODR |
	BMP3_SEL_DRDY_EN | BMP3_SEL_LATCH;
	
	return settings_sel;
}

/**
*	Function that runs the next active sensor
*/
static void start_next_active_sensor(){
	for(int8_t i = index_of_current_sensor + 1; i < SAM5_BMP3_MAX_NUMBER_SENSORS; ++i){
		if(sensor_array[i].available){
			index_of_current_sensor = i;
			flags.readout_ongoing = 1;
			bmp3_i2c_driver_write_async(&bmp3_reg_data, sizeof(bmp3_reg_data), &sensor_array[index_of_current_sensor].bmp3_interface_desc, sensor_reg_address_tx_complete_cb, 1);
			return;
		}
	}
	flags.readout_ongoing = 0;
	index_of_current_sensor = SAM5_BMP3_MAX_NUMBER_SENSORS;
}

/**
* \brief Transaction complete callback function for writing sensor address to I2C bus.
*
* \param i2c I2C descriptor structure from Microchip API Master Asynchronous Driver. Microchip Documentation: ASF4 API Reference Manual
*/
static void sensor_reg_address_tx_complete_cb(uint8_t success)
{
	if(!success){
		sensor_failed(index_of_current_sensor, 0);
		return;
	}
	bmp3_i2c_driver_read_async(sensor_array[index_of_current_sensor].reg_data, BMP3_LEN_P_T_DATA, &sensor_array[index_of_current_sensor].bmp3_interface_desc, sensor_data_rx_complete_cb, 0);
}

/**
* \brief Read complete callback function for reading sensor data. Alternates between sensor address write and sensor data read until all sensors are read.
*
* \param i2c I2C descriptor structure from Microchip API Master Asynchronous Driver. Microchip Documentation: ASF4 API Reference Manual
*/
static void sensor_data_rx_complete_cb(uint8_t success)
{
	if(!success){
		sensor_failed(index_of_current_sensor, 0);
		return;
	}
	// Store current sensor index as this changes after the call to start_next_active_sensor
	const int8_t index = index_of_current_sensor;
	start_next_active_sensor();
	parse_sensor_data((const uint8_t *) &sensor_array[index].reg_data, &sensor_array[index].uncomp_data);
	compensate_data(BMP3_PRESS_TEMP, &sensor_array[index].uncomp_data, &sensor_array[index].comp_data,
	&sensor_array[index].dev.calib_data);
	const double p = sensor_array[index].comp_data.pressure / 100; // The comp_data.pressure value is in pascal, For the calibration hectopascal was used
	const double a0 = sensor_array[index].pressure_newton_conv_a0;
	const double a1 = sensor_array[index].pressure_newton_conv_a1;
	const double a2 = sensor_array[index].pressure_newton_conv_a2;
	sensor_array[index].force_newton = a0 + (p * a1) + (p * p * a2);;
	
	if(sensor_array[index].led_pin != LED_UNUSED){
		static uint8_t ledData[3]; // LED colored Green, Red, Blue
		if(sensor_array[index].force_newton <= SAM5_BMP3_FORCE_CONTACT){
			ledData[1] = 0;
			}else if(sensor_array[index].force_newton >= SAM5_BMP3_FORCE_MAX){
			ledData[1] = 255;
			}else{
			ledData[1] = (uint8_t) (255 * (sensor_array[index].force_newton - SAM5_BMP3_FORCE_CONTACT)
			/ (SAM5_BMP3_FORCE_MAX - SAM5_BMP3_FORCE_CONTACT));
		}
		ledData[0] = 255 - ledData[1];
		ledData[2] = 0;
		sam5_ws2812_sendarray(ledData, 3, sensor_array[index].led_pin);
	}
}

/**
* This function shall be called if a sensor failed to communicate
*/
static void sensor_failed(int8_t index, uint8_t within_init)
{
	if(!within_init && flags.search_ongoing){
		return;
	}
	sensor_array[index].available = false;
	if(!within_init){
	sensor_comm_error_counter++;
	printf("Sensor %d failed\r\n", index);
	}
	if(sensor_array[index].led_pin != LED_UNUSED){
		sam5_ws2812_sendarray(ledErrorColor, 3, sensor_array[index].led_pin);
	}
}

static void print_status(bool on_init){
	flags.print_requested = 0;
	for(uint8_t i = 0; i < SAM5_BMP3_MAX_NUMBER_SENSORS; ++i){
		switch(sensor_array[i].bmp3_interface_desc.multiplexer_channel_number){
			case 1 << 0:
			printf("\tChannel 0 - Address 0x%02x", sensor_array[i].bmp3_interface_desc.bmp3_i2c_address);
			break;
			case 1 << 1:
			printf("\tChannel 1 - Address 0x%02x", sensor_array[i].bmp3_interface_desc.bmp3_i2c_address);
			break;
			case 1 << 2:
			printf("\tChannel 2 - Address 0x%02x", sensor_array[i].bmp3_interface_desc.bmp3_i2c_address);
			break;
			case 1 << 3:
			printf("\tChannel 3 - Address 0x%02x", sensor_array[i].bmp3_interface_desc.bmp3_i2c_address);
			break;
			case 1 << 4:
			printf("\tChannel 4 - Address 0x%02x", sensor_array[i].bmp3_interface_desc.bmp3_i2c_address);
			break;
			case 1 << 5:
			printf("\tChannel 5 - Address 0x%02x", sensor_array[i].bmp3_interface_desc.bmp3_i2c_address);
			break;
			case 1 << 6:
			printf("\tChannel 6 - Address 0x%02x", sensor_array[i].bmp3_interface_desc.bmp3_i2c_address);
			break;
			case 1 << 7:
			printf("\tChannel 7 - Address 0x%02x", sensor_array[i].bmp3_interface_desc.bmp3_i2c_address);
			break;
			default:
			break;
		}
		if(sensor_array[i].available){
			printf(" - Available");
			if(on_init){
				printf("\r\n");
			}
			else{
				printf(" - %ld mN\r\n", (int32_t) (sensor_array[i].force_newton * 1000));
			}
		}
		else{
			printf(" - Not Available\r\n");
		}
	}
	
}

void search_sensors(bool on_init, uint32_t num_search_rounds){
	flags.search_ongoing = 1;
	flags.search_requested = 0;
	while(flags.readout_ongoing){}
	uint8_t success_counter = 0;
	#if defined(__SAMD51J19A__)
	if(on_init){
		gpio_set_pin_direction(PIN_PA18, GPIO_DIRECTION_OUT);
		gpio_set_pin_function(PIN_PA18, GPIO_PIN_FUNCTION_OFF);
		gpio_set_pin_level(PIN_PA18, 0);
		delay_ms(10);
		gpio_set_pin_level(PIN_PA18, 1);
		delay_ms(10);
		for(uint8_t i = 0; i < SAM5_BMP3_MAX_NUMBER_SENSORS; ++i)
		{
			sensor_array[i].available = false;
		}
	}
	#endif
	for(uint32_t round = 0; round < num_search_rounds; ++round){
		for(uint8_t i = 0; i < SAM5_BMP3_MAX_NUMBER_SENSORS; ++i)
		{
			if(sensor_array[i].available == true){
				// It's already available, we do not need to search for it
				continue;
			}
			delay_us(25);
			uint8_t sensor_channel = 1 << (i / 2);
			uint8_t sensor_address = (i%2 == 0) ? BMP3_ADDR_I2C_PRIM : BMP3_ADDR_I2C_SEC;
			sensor_array[i].bmp3_interface_desc.multiplexer_channel_number = sensor_channel;
			sensor_array[i].bmp3_interface_desc.bmp3_i2c_address = sensor_address;
			sensor_array[i].force_newton = 0.0;
			sensor_array[i].available = true;
			
			bmp3_configure_dev_struct(&(sensor_array[i].dev), &(sensor_array[i].bmp3_interface_desc));
			// Initialize BMP Device
			BMP3_INTF_RET_TYPE ret = bmp3_init(&(sensor_array[i].dev));//must follow directly after configuring device struct
			if(ret != BMP3_OK){
				sensor_failed(i, 1);
				continue;
			}
			
			// Write settings to the device
			uint32_t settings_sel = bmp3_configure_dev_settings(&settings);
			ret = bmp3_set_sensor_settings(settings_sel, &settings, &(sensor_array[i].dev));
			if(ret != BMP3_OK){
				sensor_failed(i, 1);
				continue;
			}
			
			// Activate device
			settings.op_mode = BMP3_MODE_NORMAL;
			ret = bmp3_set_op_mode(&settings, &(sensor_array[i].dev));
			if(ret != BMP3_OK){
				sensor_failed(i, 1);
				continue;
			}
			sam5_ws2812_init(sensor_array[i].led_pin);
			++success_counter;
		}
	}
	printf("Found %d %ssensor%s%s\r\n", success_counter, on_init ? "" : "new ", success_counter == 1 ? "" : "s", success_counter > 0 ? ":" : "");
	if(success_counter > 0){
		// Handle printing as if sensors were found on init
		print_status(true);
	}
	flags.search_ongoing = 0;
}

/**
* Timer overflow interrupt handler -> Start sequential async read of all sensors
*/
void TC4_Handler()
{
	if(!flags.search_ongoing){
		bmp3_i2c_driver_timeout();
		index_of_current_sensor = -1;
		start_next_active_sensor();
	}
	hri_tc_clear_INTFLAG_OVF_bit(TC4);
}
