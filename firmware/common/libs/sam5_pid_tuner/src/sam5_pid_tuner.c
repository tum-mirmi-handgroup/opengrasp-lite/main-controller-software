#include "sam5_pid_tuner_impl.h"
#include "sam5_i2c_master.h"
#include "main_control.h"

#include <stdio.h>
#include <string.h>

static sam5_pid_tuner_t tuner;
static sam5_control_settings_t* settings_to_receive;
static int8_t receive_success;

static void settings_received_callback(bool success, const sam5_i2c_master_com_job_t* job);
static float32_t amplitude_factor();

void sam5_pid_tuner_init(){
	sam5_pid_tuner_set_mode(SAM5_PID_TUNER_MODE_OFF);
	sam5_pid_tuner_set_amplitude(SAM5_PID_TUNER_AMPLITUDE_SMALL);
	sam5_pid_tuner_set_active_motor(UINT8_MAX, UINT8_MAX);
}

void sam5_pid_tuner_loop(){
	if(tuner.mode == SAM5_PID_TUNER_MODE_OFF || tuner.motor == NULL){
		tuner.state = SAM5_PID_TUNER_STATE_PAUSE_NEXT_POS;
		return;
	}
	// Stop main_control loop
	main_control_set_grasp(CONTROL_INPUT_TYPE_MANUAL, 0, g_main_control.manual_speed);
	main_control_set_active_input(CONTROL_INPUT_TYPE_MANUAL);
	dc_motor_controller_inputs_t inputs = {
		.angle = 0,
		.current = 0,
		.velocity = 0,
		.voltage = 0,
		.flags = {
			.control_mode = MOTOR_CM_STOPPED,
			.continue_at_end = 0,
			.stop_at_contact = 1,
		},
	};
	
	// Apply input based on state
	switch(tuner.mode){
		case SAM5_PID_TUNER_MODE_CURRENT:{
			inputs.flags.control_mode = MOTOR_CM_CURRENT;
			switch(tuner.state){
				case SAM5_PID_TUNER_STATE_PAUSE_NEXT_NEG:
				case SAM5_PID_TUNER_STATE_PAUSE_NEXT_POS:
				inputs.current = 0;
				break;
				case SAM5_PID_TUNER_STATE_POS:
				inputs.current = 0.01 * amplitude_factor();
				break;
				case SAM5_PID_TUNER_STATE_NEG:
				inputs.current = -0.01 * amplitude_factor();
				break;
			}
			break;
		}
		case SAM5_PID_TUNER_MODE_POSITION:{
			inputs.flags.control_mode = MOTOR_CM_POSITION;
			inputs.velocity = 5 * amplitude_factor();
			inputs.current = 0.05 * amplitude_factor();
			switch(tuner.state){
				case SAM5_PID_TUNER_STATE_PAUSE_NEXT_NEG:
				case SAM5_PID_TUNER_STATE_PAUSE_NEXT_POS:
				inputs.velocity = 0;
				break;
				case SAM5_PID_TUNER_STATE_POS:
				inputs.angle = 5 * amplitude_factor();
				break;
				case SAM5_PID_TUNER_STATE_NEG:
				inputs.angle = -5 * amplitude_factor();
				break;
			}
			break;
		}
		case SAM5_PID_TUNER_MODE_OFF:{
			break;
		}
		break;
	}
	
	sam5_i2c_master_com_job_t job = {
		.command = MOTOR_PROTOCOL_BUILD_COMMAND(tuner.motor->motor_index, MOTOR_PROTOCOL_WRITE_INPUT),
		.slave_address = tuner.motor->i2c_address,
		.direction = I2C_MASTER_DIR_MASTER_TO_SLAVE,
		.num_bytes_to_transfer = sizeof(dc_motor_controller_inputs_t),
		.callback = NULL,
		.period_ms = 0
	};
	memcpy(job.buffer, &inputs, sizeof(dc_motor_controller_inputs_t));
	sam5_i2c_master_register_com_job(&job);
	
	// Transition state
	switch(tuner.state){
		case SAM5_PID_TUNER_STATE_PAUSE_NEXT_POS: tuner.state = SAM5_PID_TUNER_STATE_POS; break;
		case SAM5_PID_TUNER_STATE_POS: tuner.state = SAM5_PID_TUNER_STATE_PAUSE_NEXT_NEG; break;
		case SAM5_PID_TUNER_STATE_PAUSE_NEXT_NEG: tuner.state = SAM5_PID_TUNER_STATE_NEG; break;
		case SAM5_PID_TUNER_STATE_NEG: tuner.state = SAM5_PID_TUNER_STATE_PAUSE_NEXT_POS; break;
	}
}

void sam5_pid_tuner_set_mode(sam5_pid_tuner_mode_t mode){
	tuner.mode = mode;
}

void sam5_pid_tuner_set_amplitude(sam5_pid_tuner_amplitude_t amplitude){
	tuner.amplitude = amplitude;
}

uint8_t sam5_pid_tuner_set_active_motor(uint8_t motor_controller_index, uint8_t motor_index){
	tuner.motor = NULL;
	uint8_t motor_pos = UINT8_MAX;
	for(uint8_t i = 0; i < MAIN_CONTROL_NUM_MOTORS; ++i){
		if(g_main_control.motors[i].controller_index == motor_controller_index && g_main_control.motors[i].motor_index == motor_index){
			motor_pos = i;
		}
	}
	
	if(motor_pos >= MAIN_CONTROL_NUM_MOTORS){
		return 1;
	}
	const main_control_motor_t* motor = &g_main_control.motors[motor_pos];
	sam5_i2c_master_com_job_t job = {
		.slave_address = motor->i2c_address,
		.direction = I2C_MASTER_DIR_SLAVE_TO_MASTER,
		.num_bytes_to_transfer = sizeof(sam5_control_settings_t),
		.callback = &settings_received_callback,
		.period_ms = 0
	};
	receive_success = 0;
	settings_to_receive = &tuner.current;
	job.command = MOTOR_PROTOCOL_BUILD_COMMAND(motor->motor_index, MOTOR_PROTOCOL_READ_CURRENT_CONTROLLER);
	sam5_i2c_master_register_com_job(&job);
	for(uint8_t i = 0; i < 50; ++i){
		if(receive_success != 0){
			if(receive_success != -1){
				printf("Failed reading of current controller settings\r\n");
				return 2;
			}
			break;
		}
		delay_ms(1);
	}

	receive_success = 0;
	settings_to_receive = &tuner.cascaded_velocity;
	job.command = MOTOR_PROTOCOL_BUILD_COMMAND(motor->motor_index, MOTOR_PROTOCOL_READ_VELOCITY_CONTROLLER);
	sam5_i2c_master_register_com_job(&job);
	for(uint8_t i = 0; i < 50; ++i){
		if(receive_success != 0){
			if(receive_success != -1){
				printf("Failed reading of velocity controller settings\r\n");
				return 4;
			}
			break;
		}
		delay_ms(1);
	}
	
	receive_success = 0;
	settings_to_receive = &tuner.cascaded_current;
	job.command = MOTOR_PROTOCOL_BUILD_COMMAND(motor->motor_index, MOTOR_PROTOCOL_READ_POSITION_CONTROLLER);
	sam5_i2c_master_register_com_job(&job);
	for(uint8_t i = 0; i < 50; ++i){
		if(receive_success != 0){
			if(receive_success != -1){
				printf("Failed reading of position controller settings\r\n");
				return 5;
			}
			break;
		}
		delay_ms(1);
	}
	tuner.motor = motor;
	return 0;
}

static void settings_received_callback(bool success, const sam5_i2c_master_com_job_t* job){
	if(success){
		memcpy(settings_to_receive, job->buffer, sizeof(sam5_control_settings_t));
		receive_success = 1;
	}
	else{
		receive_success = -1;
	}
}

static float32_t amplitude_factor(){
	switch(tuner.amplitude){
		case SAM5_PID_TUNER_AMPLITUDE_SMALL:
		return 1;
		case SAM5_PID_TUNER_AMPLITUDE_MEDIUM:
		return 2;
		case SAM5_PID_TUNER_AMPLITUDE_BIG:
		return 4;
		default:
		return 0;
	}
}