#ifndef SAM5_PID_TUNER_IMPL_H_
#define SAM5_PID_TUNER_IMPL_H_

#include "sam5_pid_tuner.h"
#include "motor_protocol.h"
#include "main_control.h"

typedef enum {
	SAM5_PID_TUNER_STATE_PAUSE_NEXT_POS,
	SAM5_PID_TUNER_STATE_POS,
	SAM5_PID_TUNER_STATE_PAUSE_NEXT_NEG,
	SAM5_PID_TUNER_STATE_NEG,
} sam5_pid_tuner_state_t;


typedef struct {
	sam5_control_settings_t current;
	sam5_control_settings_t velocity;
	sam5_control_settings_t cascaded_velocity;
	sam5_control_settings_t cascaded_current;
	
	sam5_pid_tuner_mode_t mode;
	sam5_pid_tuner_state_t state;
	sam5_pid_tuner_amplitude_t amplitude;
	const main_control_motor_t* motor;
} sam5_pid_tuner_t;

#endif //SAM5_PID_TUNER_IMPL_H_