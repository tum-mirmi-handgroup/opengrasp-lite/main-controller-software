#ifndef SAM5_PID_TUNER_H_
#define SAM5_PID_TUNER_H_

#include <atmel_start.h>

typedef enum {
	SAM5_PID_TUNER_MODE_OFF,
	SAM5_PID_TUNER_MODE_CURRENT,
	SAM5_PID_TUNER_MODE_POSITION,
} sam5_pid_tuner_mode_t;

typedef enum {
	SAM5_PID_TUNER_AMPLITUDE_SMALL,
	SAM5_PID_TUNER_AMPLITUDE_MEDIUM,
	SAM5_PID_TUNER_AMPLITUDE_BIG,
} sam5_pid_tuner_amplitude_t;

void sam5_pid_tuner_init();

void sam5_pid_tuner_loop();

uint8_t sam5_pid_tuner_set_active_motor(uint8_t motor_controller_index, uint8_t motor_index);

void sam5_pid_tuner_set_mode(sam5_pid_tuner_mode_t mode);

void sam5_pid_tuner_set_amplitude(sam5_pid_tuner_amplitude_t amplitude);

#endif //SAM5_PID_TUNER_H_