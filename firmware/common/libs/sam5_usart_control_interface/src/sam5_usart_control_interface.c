#include "sam5_usart_control_interface_impl.h"
#include "motor_protocol.h"
#include "main_control.h"
#include "sam5_i2c_master.h"
#include "sam5_bmp3_wrapper.h"

#include <atmel_start.h>
#include <string.h>
#include <stdio.h>

static void cb_rx_usart_control(const struct usart_async_descriptor *const io_descr);

static struct io_descriptor *usart_io;
static sam5_usart_control_request_type_e current_req;

void sam5_usart_control_interface_init(){
	USART_CONTROL_init();
	
	usart_async_register_callback(&USART_CONTROL, USART_ASYNC_RXC_CB, cb_rx_usart_control);
	usart_async_get_io_descriptor(&USART_CONTROL, &usart_io);

	for(uint8_t i= 0; i < MAIN_CONTROL_NUM_MOTOR_CONTROLLER; ++i){
		const sam5_i2c_master_com_job_t job = {
			.command = MOTOR_PROTOCOL_COMMON_READ_COMBINED_STATE,
			.slave_address = MOTOR_PROTOCOL_BASE_ADDRESS + i,
			.direction = I2C_MASTER_DIR_SLAVE_TO_MASTER,
			.num_bytes_to_transfer = sizeof(dc_motor_controller_combined_state_t),
			.callback = &main_control_motor_i2c_callback_read_combined_state,
			.period_ms = 50,
		};
		sam5_i2c_master_register_com_job(&job);
	}

	current_req = CONTROL_REQ_NONE;
	
}

void sam5_usart_control_interface_start(){
	usart_async_enable(&USART_CONTROL);
}

static uint8_t tx_buffer[2 + (16 * 4)]; // Start, Stop byte + Up to 16 force sensors
static void cb_rx_usart_control(const struct usart_async_descriptor *const io_descr)
{
	uint8_t data;
	if(io_read(usart_io, &data, 1) > 0){
		// Received something
		if(current_req == CONTROL_REQ_NONE){
			switch(data){
				case CONTROL_REQ_RX(CONTROL_REQ_READ_TACTILE): {
					tx_buffer[0] = CONTROL_REQ_TX(CONTROL_REQ_READ_TACTILE);
					CRITICAL_SECTION_ENTER()
					for(uint8_t i = 0; i < SAM5_BMP3_MAX_NUMBER_SENSORS; ++i){
						const uint8_t idx = 1 + i * sizeof(float32_t);
						memcpy(&tx_buffer[idx], (uint8_t*)&sensor_array[i].force_newton, sizeof(float32_t));
					}
					CRITICAL_SECTION_LEAVE()
					tx_buffer[1 + SAM5_BMP3_MAX_NUMBER_SENSORS * sizeof(float32_t)] = CONTROL_REQ_TX(CONTROL_REQ_READ_TACTILE);
					io_write(usart_io, tx_buffer, 2 + SAM5_BMP3_MAX_NUMBER_SENSORS * sizeof(float32_t));
				}
				break;
				case CONTROL_REQ_RX(CONTROL_REQ_READ_CURRENT):{
					tx_buffer[0] = CONTROL_REQ_TX(CONTROL_REQ_READ_CURRENT);
					CRITICAL_SECTION_ENTER()
					for(uint8_t i = 0; i < MAIN_CONTROL_NUM_MOTORS; ++i){
						const main_control_motor_t* motor = &g_main_control.motors[i];
						const uint8_t idx = 1 + i * sizeof(float32_t);
						memcpy(&tx_buffer[idx], &motor->current, sizeof(float32_t));
					}
					CRITICAL_SECTION_LEAVE()
					tx_buffer[1 + MAIN_CONTROL_NUM_MOTORS * sizeof(float32_t)] = CONTROL_REQ_TX(CONTROL_REQ_READ_CURRENT);
					io_write(usart_io, tx_buffer, 2 + MAIN_CONTROL_NUM_MOTORS * sizeof(float32_t));
				}
				break;
				case CONTROL_REQ_RX(CONTROL_REQ_READ_ANGLE):{
					tx_buffer[0] = CONTROL_REQ_TX(CONTROL_REQ_READ_ANGLE);
					CRITICAL_SECTION_ENTER()
					for(uint8_t i = 0; i < MAIN_CONTROL_NUM_MOTORS; ++i){
						const main_control_motor_t* motor = &g_main_control.motors[i];
						const uint8_t idx = 1 + i * sizeof(float32_t);
						memcpy(&tx_buffer[idx], &motor->angle, sizeof(float32_t));
					}
					CRITICAL_SECTION_LEAVE()
					tx_buffer[1 + MAIN_CONTROL_NUM_MOTORS * sizeof(float32_t)] = CONTROL_REQ_TX(CONTROL_REQ_READ_ANGLE);
					io_write(usart_io, tx_buffer, 2 + MAIN_CONTROL_NUM_MOTORS * sizeof(float32_t));
				}
				break;
				default:{
					if(data >= CONTROL_REQ_RX(CONTROL_REQ_GRASP_BASE)){
						current_req = data;
					}
					else{
						// Do nothing here
						printf("Unrecognized control request received via USART: 0x%x\r\n", data);
					}
				}
				break;
			}
		}
		else {
			ASSERT(current_req >= CONTROL_REQ_RX(CONTROL_REQ_GRASP_BASE));
			tx_buffer[0] = current_req + (CONTROL_REQ_TX(0) - CONTROL_REQ_RX(0));
			tx_buffer[2] = current_req + (CONTROL_REQ_TX(0) - CONTROL_REQ_RX(0));
			if(current_req == CONTROL_REQ_RX(CONTROL_REQ_OPEN_ALL)){
				printf("Set Grasp OPEN_ALL %d\r\n", data);
				main_control_set_grasp(CONTROL_INPUT_TYPE_USART, 1, data);
				tx_buffer[1] = g_main_control.flags.all_stopped_or_reached ? 1 : 0;
			}
			else{
				// Grasp 1 -> 2
				// Grasp 2 -> 4
				// Grasp 3 -> 6
				// ...
				printf("Set Grasp %d %d\r\n", 2 * (1 + current_req - CONTROL_REQ_RX(CONTROL_REQ_GRASP_BASE)), data);
				main_control_set_grasp(CONTROL_INPUT_TYPE_USART, 2 * (1 + current_req - CONTROL_REQ_RX(CONTROL_REQ_GRASP_BASE)), data);
				tx_buffer[1] = g_main_control.flags.all_stopped_or_reached ? 1 : 0;
			}
			io_write(usart_io, tx_buffer, 3);
			current_req = CONTROL_REQ_NONE;
		}
	}
}
