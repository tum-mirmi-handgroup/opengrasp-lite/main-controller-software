/*
* sam5_i2c_master.c
*
* Created: 29.11.2022 18:23:52
*  Author: Michael Ratzel
*/
#include "sam5_i2c_master_impl.h"
#include "simple_state_machine.h"
#include <string.h>

i2c_master_t g_i2c_master;

void sam5_i2c_master_loop(){
	i2c_master_process_next_job();
}

void i2c_master_initialize_peripherals(void)
{
	gpio_set_pin_direction(I2C_MASTER_SCL_PIN, GPIO_DIRECTION_OUT);
	gpio_set_pin_function(I2C_MASTER_SCL_PIN, GPIO_PIN_FUNCTION_OFF);
	for(uint16_t i = 0; i < 500; ++i){
		delay_us(25);
		gpio_set_pin_level(I2C_MASTER_SCL_PIN, 1);
		delay_us(25);
		gpio_set_pin_level(I2C_MASTER_SCL_PIN, 0);
	}
	
	///* Initialize pins
	///* Deactivate GPIO_PULL_UP and GPIO_PULL_DOWN on the SCL pin
	gpio_set_pin_pull_mode(I2C_MASTER_SCL_PIN, GPIO_PULL_OFF);
	///* Deactivate GPIO_PULL_UP and GPIO_PULL_DOWN on the SDA pin
	gpio_set_pin_pull_mode(I2C_MASTER_SDA_PIN, GPIO_PULL_OFF);
	///* Set function of I2C_MASTER_SCL_PIN to I2C_MASTER_SCL_PIN_FUNCTION
	gpio_set_pin_function(I2C_MASTER_SCL_PIN, I2C_MASTER_SCL_PIN_FUNCTION);
	///* Set function of I2C_MASTER_SDA_PIN to I2C_MASTER_SDA_PIN_FUNCTION
	gpio_set_pin_function(I2C_MASTER_SDA_PIN, I2C_MASTER_SDA_PIN_FUNCTION);
	
	
	
	///* Clock settings
	//* Enable APB SERCOM units APB communication
	I2C_MASTER_APB_MCLK_MASK_SETTER(MCLK);
	//* Set GCLKs
	hri_gclk_write_PCHCTRL_reg(GCLK, I2C_MASTER_SERCOM_GCLK_ID_CORE, GCLK_PCHCTRL_GEN_GCLK0_Val | (1 << GCLK_PCHCTRL_CHEN_Pos));
	hri_gclk_write_PCHCTRL_reg(GCLK, I2C_MASTER_SERCOM_GCLK_ID_SLOW, I2C_MASTER_GLCK_SLOW_GEN | (1 << GCLK_PCHCTRL_CHEN_Pos));
	
	///* Initialize I2C_MASTER_SERCOM as I2C master
	///* Check if no software reset of I2C_MASTER_SERCOM is in progress
	if (!hri_sercomi2cm_is_syncing(I2C_MASTER_SERCOM, SERCOM_I2CM_SYNCBUSY_SWRST))
	{
		///* Perform a software reset of I2C_MASTER_SERCOM
		hri_sercomi2cm_write_CTRLA_reg(I2C_MASTER_SERCOM, SERCOM_I2CM_CTRLA_SWRST);
	}
	///* Wait until the software reset is finished
	hri_sercomi2cm_wait_for_sync(I2C_MASTER_SERCOM, SERCOM_I2CM_SYNCBUSY_SWRST);
	
	hri_sercomi2cm_write_CTRLA_reg(I2C_MASTER_SERCOM,
	0 << SERCOM_I2CM_CTRLA_LOWTOUTEN_Pos    // Time-out disabled
	| 0x3 << SERCOM_I2CM_CTRLA_INACTOUT_Pos	// Inactive time-out set for 20-21�s for 1 MHz speed
	| 0 << SERCOM_I2CM_CTRLA_SCLSM_Pos      // SCL stretch before ACK bit for software interaction
	| 0x0 << SERCOM_I2CM_CTRLA_SPEED_Pos    // Fast-mode Plus (Fm+) up to 1 MHz
	| 0 << SERCOM_I2CM_CTRLA_SEXTTOEN_Pos   // Cumulative time-out disabled
	| 0 << SERCOM_I2CM_CTRLA_MEXTTOEN_Pos	// Host SCL Low Extended Time-Out disabled
	| 0x2 << SERCOM_I2CM_CTRLA_SDAHOLD_Pos  // For sending a start SDA is hold low for 300-600 ns before SCL gets pulled low
	| 0 << SERCOM_I2CM_CTRLA_PINOUT_Pos     // 4-wire operation disabled
	| 0 << SERCOM_I2CM_CTRLA_RUNSTDBY_Pos   // Run in standby disabled, all reception is dropped
	| 0x5 << SERCOM_I2CM_CTRLA_MODE_Pos		// Set SERCOM mode to I2C master
	| 0 );
	hri_sercomi2cm_write_CTRLB_reg(I2C_MASTER_SERCOM,
	0 << SERCOM_I2CM_CTRLB_QCEN_Pos			// Quick command mode is enabled -> direct sw interaction after ADDR ack from slave
	| 0 << SERCOM_I2CM_CTRLB_SMEN_Pos       // Smart mode is disabled, data is not acknowledged automatically when DATA.DATA is read
	| 0 );
	hri_sercomi2cm_write_CTRLC_reg(I2C_MASTER_SERCOM,
	0 << SERCOM_I2CM_CTRLC_DATA32B_Pos      // Data transaction to/from DATA are 8-bit in size
	| 0 );
	hri_sercomi2cm_write_BAUD_reg(I2C_MASTER_SERCOM,
	I2C_MASTER_BAUD_HIGH << SERCOM_I2CM_BAUD_BAUD_Pos
	| I2C_MASTER_BAUD_LOW << SERCOM_I2CM_BAUD_BAUDLOW_Pos
	| 0 );
	hri_sercomi2cm_write_INTEN_reg(I2C_MASTER_SERCOM,
	1 << SERCOM_I2CM_INTENSET_ERROR_Pos     // Error interrupt is enabled.
	| 1 << SERCOM_I2CM_INTENSET_MB_Pos		// The Master on Bus interrupt is enabled.
	| 1 << SERCOM_I2CM_INTENSET_SB_Pos		// The Slave on Bus interrupt is enabled.
	| 0 );
	hri_sercomi2cm_write_ADDR_reg(I2C_MASTER_SERCOM,
	0 << SERCOM_I2CM_ADDR_TENBITEN_Pos		// Disable 10-bit addressing
	| 0 );

	NVIC_SetPriority(I2C_MASTER_MB_IRQn, NVIC_PRIORITY_I2C_SERCOM);
	NVIC_SetPriority(I2C_MASTER_SB_IRQn, NVIC_PRIORITY_I2C_SERCOM);
	NVIC_SetPriority(I2C_MASTER_ERROR_IRQn, NVIC_PRIORITY_I2C_SERCOM);
}

void i2c_master_enable(void)
{
	///* Enable MB interrupt
	NVIC_DisableIRQ(I2C_MASTER_MB_IRQn);
	NVIC_ClearPendingIRQ(I2C_MASTER_MB_IRQn);
	NVIC_EnableIRQ(I2C_MASTER_MB_IRQn);
	///* Enable SB interrupt
	NVIC_DisableIRQ(I2C_MASTER_SB_IRQn);
	NVIC_ClearPendingIRQ(I2C_MASTER_SB_IRQn);
	NVIC_EnableIRQ(I2C_MASTER_SB_IRQn);
	///* Enable ERROR interrupt
	NVIC_DisableIRQ(I2C_MASTER_ERROR_IRQn);
	NVIC_ClearPendingIRQ(I2C_MASTER_ERROR_IRQn);
	NVIC_EnableIRQ(I2C_MASTER_ERROR_IRQn);
	
	hri_sercomi2cm_set_CTRLA_ENABLE_bit(I2C_MASTER_SERCOM);
}


void I2C_MASTER_MB_HANDLER(void)
{
	simple_state_machine_event_t this_event;
	if(hri_sercomi2cm_get_STATUS_reg(I2C_MASTER_SERCOM, SERCOM_I2CM_STATUS_BUSERR | SERCOM_I2CM_STATUS_ARBLOST)){
		this_event.signal_ = I2C_MASTER_EVT_SIG_ERROR;
	}
	else if(hri_sercomi2cm_get_STATUS_RXNACK_bit(I2C_MASTER_SERCOM))
	{
		// Received NACK
		this_event.signal_ = I2C_MASTER_EVT_SIG_NACK;
	}
	else
	{
		// Received ACK
		this_event.signal_ = I2C_MASTER_EVT_SIG_ACK;
	}
	simpleStateMachineDispatch_(&g_i2c_master.state_machine, &this_event, NULL);
	i2c_master_execute_finish_action(SERCOM_I2CM_INTFLAG_MB);
}

void I2C_MASTER_SB_HANDLER(void)
{
	simple_state_machine_event_t this_event = {
		.signal_ = I2C_MASTER_EVT_SIG_SB
	};
	simpleStateMachineDispatch_(&g_i2c_master.state_machine, &this_event, NULL);
	i2c_master_execute_finish_action(SERCOM_I2CM_INTFLAG_SB);
}

void I2C_MASTER_ERROR_HANDLER(void)
{
	simple_state_machine_event_t this_event =
	{
		.signal_ = I2C_MASTER_EVT_SIG_ERROR
	};
	simpleStateMachineDispatch_(&g_i2c_master.state_machine, &this_event, NULL);
	i2c_master_execute_finish_action(SERCOM_I2CM_INTFLAG_ERROR);
}

void i2c_master_finish_action_command(uint8_t command){
	hri_sercomi2cm_write_CTRLB_CMD_bf(I2C_MASTER_SERCOM, command);
}

void i2c_master_finish_action_command_ack(uint8_t command){
	hri_sercomi2cm_clear_CTRLB_ACKACT_bit(I2C_MASTER_SERCOM);
	hri_sercomi2cm_write_CTRLB_CMD_bf(I2C_MASTER_SERCOM, command);
}

void i2c_master_finish_action_command_nack(uint8_t command){
	hri_sercomi2cm_set_CTRLB_ACKACT_bit(I2C_MASTER_SERCOM);
	hri_sercomi2cm_write_CTRLB_CMD_bf(I2C_MASTER_SERCOM, command);
}

void i2c_master_finish_action_data(uint8_t data){
	hri_sercomi2cm_write_DATA_DATA_bf(I2C_MASTER_SERCOM, data);
}

void i2c_master_finish_action_address(uint8_t address){
	hri_sercomi2cm_write_ADDR_ADDR_bf(I2C_MASTER_SERCOM, address);
}

void i2c_master_register_finish_action(void(*volatile finish_action)(uint8_t data), uint8_t data){
	g_i2c_master.finish_action = finish_action;
	g_i2c_master.finish_data = data;
}

void i2c_master_execute_finish_action(uint8_t default_flag){
	if(g_i2c_master.finish_action == 0){
		((Sercom *)I2C_MASTER_SERCOM)->I2CM.INTFLAG.reg = default_flag;
	}
	else{
		g_i2c_master.finish_action(g_i2c_master.finish_data);
		g_i2c_master.finish_action = 0;
	}
	g_i2c_master.finish_data = 0;
	i2c_master_process_next_job();
}

void i2c_master_state_init(volatile simple_state_machine_t *fsm, simple_state_machine_event_t const *event, volatile void * user_pointer)
{
	switch(event->signal_)
	{
		case ENTRY_SIG:
		{
			i2c_master_initialize_peripherals();
			i2c_master_enable();
			simpleStateMachineTran_(fsm, i2c_master_state_idle, NULL);
			break;
		}
		case EXIT_SIG:
		{
			break;
		}
		default:
		{
			break;
		}
	}
}

void i2c_master_state_idle(volatile simple_state_machine_t *fsm, simple_state_machine_event_t const *event, volatile void * user_pointer)
{
	switch(event->signal_)
	{
		case ENTRY_SIG:
		{
			g_i2c_master.processed_bytes = 0;
			g_i2c_master.num_bytes_to_transfer = 0;
			g_i2c_master.direction = I2C_MASTER_DIR_INACTIVE;
			break;
		}
		case EXIT_SIG:
		{
			break;
		}
		case I2C_MASTER_EVT_SIG_START:
		{
			simpleStateMachineTran_(fsm, i2c_master_state_addressed, NULL);
			i2c_master_register_finish_action(i2c_master_finish_action_address, g_i2c_master.slave_address << 1 | I2C_MASTER_DIR_MASTER_TO_SLAVE);
			break;
		}
		default:
		{
			break;
		}
	}
}

void i2c_master_state_addressed(volatile simple_state_machine_t *fsm, simple_state_machine_event_t const *event, volatile void * user_pointer)
{
	switch(event->signal_)
	{
		case ENTRY_SIG:
		{
			break;
		}
		case EXIT_SIG:
		{
			break;
		}
		case I2C_MASTER_EVT_SIG_ACK:
		{
			i2c_master_register_finish_action(i2c_master_finish_action_data, g_i2c_master.command);
			simpleStateMachineTran_(fsm, i2c_master_state_command_send, NULL);
			break;
		}
		case I2C_MASTER_EVT_SIG_ERROR:
		{
			i2c_master_transmission_nosuccess();
			simpleStateMachineTran_(fsm, i2c_master_state_idle, NULL);
			break;
		}
		default:
		{
			// Send Stop and abort
			i2c_master_transmission_nosuccess();
			i2c_master_register_finish_action(i2c_master_finish_action_command, 0x3);
			simpleStateMachineTran_(fsm, i2c_master_state_idle, NULL);
			break;
		}
	}
}

void i2c_master_state_command_send(volatile simple_state_machine_t *fsm, simple_state_machine_event_t const *event, volatile void * user_pointer)
{
	switch(event->signal_)
	{
		case ENTRY_SIG:
		{
			break;
		}
		case EXIT_SIG:
		{
			break;
		}
		case I2C_MASTER_EVT_SIG_ACK:
		{
			i2c_master_register_finish_action(i2c_master_finish_action_address, g_i2c_master.slave_address << 1 | g_i2c_master.direction);
			if (g_i2c_master.direction == I2C_MASTER_DIR_MASTER_TO_SLAVE){
				simpleStateMachineTran_(fsm, i2c_master_state_sending, NULL);
			}
			else{
				simpleStateMachineTran_(fsm, i2c_master_state_receiving, NULL);
			}
			break;
		}
		case I2C_MASTER_EVT_SIG_ERROR:
		{
			i2c_master_transmission_nosuccess();
			simpleStateMachineTran_(fsm, i2c_master_state_idle, NULL);
			break;
		}
		default:
		{
			// Send Stop and abort
			i2c_master_transmission_nosuccess();
			i2c_master_register_finish_action(i2c_master_finish_action_command, 0x3);
			simpleStateMachineTran_(fsm, i2c_master_state_idle, NULL);
			break;
		}
	}
}

void i2c_master_state_sending(volatile simple_state_machine_t *fsm, simple_state_machine_event_t const *event, volatile void * user_pointer){
	switch(event->signal_)
	{
		case ENTRY_SIG:
		{
			break;
		}
		case EXIT_SIG:
		{
			break;
		}
		case I2C_MASTER_EVT_SIG_ACK:
		{
			if(g_i2c_master.processed_bytes <= g_i2c_master.num_bytes_to_transfer){
				i2c_master_register_finish_action(i2c_master_finish_action_data, g_i2c_master.transfer_buffer[g_i2c_master.processed_bytes++]);
			}
			else{
				// Send Stop and Finish
				i2c_master_transmission_success();
				i2c_master_register_finish_action(i2c_master_finish_action_command, 0x3);
				simpleStateMachineTran_(fsm, i2c_master_state_idle, NULL);
			}
			break;
		}
		case I2C_MASTER_EVT_SIG_ERROR:
		{
			i2c_master_transmission_nosuccess();
			simpleStateMachineTran_(fsm, i2c_master_state_idle, NULL);
			break;
		}
		default:
		{
			// Send Stop and abort
			i2c_master_transmission_nosuccess();
			i2c_master_register_finish_action(i2c_master_finish_action_command, 0x3);
			simpleStateMachineTran_(fsm, i2c_master_state_idle, NULL);
			break;
		}
	}
}

void i2c_master_state_receiving(volatile simple_state_machine_t *fsm, simple_state_machine_event_t const *event, volatile void * user_pointer){
	switch(event->signal_)
	{
		case ENTRY_SIG:
		{
			break;
		}
		case EXIT_SIG:
		{
			break;
		}
		case I2C_MASTER_EVT_SIG_SB:
		{
			if(g_i2c_master.processed_bytes < g_i2c_master.num_bytes_to_transfer){
				// We received byte and want to get more (Process one byte extra for CRC)
				// Read data, send ACK, and wait for next byte
				g_i2c_master.transfer_buffer[g_i2c_master.processed_bytes++] = (uint8_t) hri_sercomi2cm_read_DATA_DATA_bf(I2C_MASTER_SERCOM);
				i2c_master_register_finish_action(i2c_master_finish_action_command_ack, 0x2);
			}
			else if (g_i2c_master.processed_bytes == g_i2c_master.num_bytes_to_transfer){
				// We received byte and are done now (Process one byte extra for CRC)
				// Read data, send ACK, and send Stop
				g_i2c_master.transfer_buffer[g_i2c_master.processed_bytes++] = (uint8_t) hri_sercomi2cm_read_DATA_DATA_bf(I2C_MASTER_SERCOM);
				uint8_t crc = sam5_sw_crc8_Applied(g_i2c_master.transfer_buffer, g_i2c_master.num_bytes_to_transfer, g_i2c_master.transfer_buffer[g_i2c_master.num_bytes_to_transfer]);
				if(crc == 0){
					i2c_master_transmission_nosuccess();
				}
				else{
					i2c_master_transmission_success();
				}
				i2c_master_register_finish_action(i2c_master_finish_action_command_ack, 0x3);
				simpleStateMachineTran_(fsm, i2c_master_state_idle, NULL);
			}
			else{
				// This should be NEVER reached
				// Send NACK, Stop and abort
				i2c_master_transmission_nosuccess();
				i2c_master_register_finish_action(i2c_master_finish_action_command_nack, 0x3);
				simpleStateMachineTran_(fsm, i2c_master_state_idle, NULL);
			}
			break;
		}
		case I2C_MASTER_EVT_SIG_ERROR:
		{
			i2c_master_transmission_nosuccess();
			simpleStateMachineTran_(fsm, i2c_master_state_idle, NULL);
			break;
		}
		default:
		{
			// Send Stop and abort
			i2c_master_transmission_nosuccess();
			i2c_master_register_finish_action(i2c_master_finish_action_command, 0x3);
			simpleStateMachineTran_(fsm, i2c_master_state_idle, NULL);
			break;
		}
	}
}


void sam5_i2c_master_init()
{
	g_i2c_master.num_active_jobs = 0;
	for(uint8_t i = 0; i < I2C_MASTER_MAX_NUMBER_JOBS; ++i){
		g_i2c_master.active_jobs[i] = false;
	}
	simpleStateMachineInit_(&g_i2c_master.state_machine, i2c_master_state_init, NULL);
}
