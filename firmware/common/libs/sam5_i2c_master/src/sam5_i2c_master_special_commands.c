#include "sam5_i2c_master_impl.h"

#include <string.h>
#include <stdio.h>


static void i2c_master_special_command_callback_internal(bool success, const sam5_i2c_master_com_job_t* job);
static void check_identification_callback(bool success, const sam5_i2c_master_com_job_t* job);
static void test_connection_callback_1(bool success, const sam5_i2c_master_com_job_t* job);
static void test_connection_callback_2(bool success, const sam5_i2c_master_com_job_t* job);
static uint8_t test_value = 0;

uint8_t sam5_i2c_master_check_protocol_identification(uint8_t i2c_address, const sam5_i2c_protocol_identification_t* identification){
	g_i2c_master.identification_success = 0;
	g_i2c_master.protocol_id = identification;
	g_i2c_master.address_to_identify = i2c_address;
	sam5_i2c_master_com_job_t com_job = {
		.direction = I2C_MASTER_DIR_SLAVE_TO_MASTER,
		.buffer = {0},
		.num_bytes_to_transfer = 3,
		.slave_address = i2c_address,
		.command = 251,
		.period_ms = 0,
		.callback = check_identification_callback,
	};
	if(sam5_i2c_master_register_com_job(&com_job) != 0){
		return 1;
	}
	while(g_i2c_master.identification_success == 0){}
	
	if(g_i2c_master.identification_success == 1){
		return 0;
	}
	else{
		return 1;
	}
}

uint8_t sam5_i2c_master_change_slave_address(uint8_t old_address, uint8_t new_address, void (*const special_command_callback)(bool, const uint8_t*))
{
	if(new_address <= 0xF || new_address >= 0x78)
	{
		printf("New I2C address %d is not in the valid range between 16 (0x10) and 119 (0x77)\r\n", new_address);
		return 1;
	}
	g_i2c_master.special_command_callback = special_command_callback;
	sam5_i2c_master_com_job_t com_job = {
		.direction = I2C_MASTER_DIR_MASTER_TO_SLAVE,
		.buffer = {new_address},
		.num_bytes_to_transfer = 1,
		.slave_address = old_address,
		.command = 255,
		.period_ms = 0,
		.callback = i2c_master_special_command_callback_internal,
	};
	return sam5_i2c_master_register_com_job(&com_job);
}

uint8_t sam5_i2c_master_query_num_commands(uint8_t i2c_address, void (*const special_command_callback)(bool, const uint8_t*))
{

	g_i2c_master.special_command_callback = special_command_callback;
	sam5_i2c_master_com_job_t com_job = {
		.direction = I2C_MASTER_DIR_SLAVE_TO_MASTER,
		.buffer = {0},
		.num_bytes_to_transfer = 1,
		.slave_address = i2c_address,
		.command = 254,
		.period_ms = 0,
		.callback = i2c_master_special_command_callback_internal,
	};
	return sam5_i2c_master_register_com_job(&com_job);
}

uint8_t sam5_i2c_master_test_connection(uint8_t i2c_address, void (*const special_command_callback)(bool, const uint8_t*))
{
	g_i2c_master.special_command_callback = special_command_callback;
	sam5_i2c_master_com_job_t com_job = {
		.direction = I2C_MASTER_DIR_MASTER_TO_SLAVE,
		.buffer = {++test_value},
		.num_bytes_to_transfer = 1,
		.slave_address = i2c_address,
		.command = 252,
		.period_ms = 0,
		.callback = test_connection_callback_1,
	};
	return sam5_i2c_master_register_com_job(&com_job);
}

static void i2c_master_special_command_callback_internal(bool success, const sam5_i2c_master_com_job_t* job)
{
	if(g_i2c_master.special_command_callback != NULL){
		g_i2c_master.special_command_callback(success, job->buffer);
	}
}

static void check_identification_callback(bool success, const sam5_i2c_master_com_job_t* job){
	ASSERT(g_i2c_master.protocol_id != NULL);
	if(!success){
		printf("%d - Failed receiving protocol identification\r\n", g_i2c_master.address_to_identify);
		g_i2c_master.identification_success = 2;
		return;
	}
	sam5_i2c_protocol_identification_t* received_id = (sam5_i2c_protocol_identification_t*) job->buffer;
	g_i2c_master.identification_success = 1;
	if(g_i2c_master.protocol_id->type != received_id->type){
		printf("%d - Protocol has wrong type: %d (%d expected)\r\n", g_i2c_master.address_to_identify, received_id->type, g_i2c_master.protocol_id->type);
		g_i2c_master.identification_success = 2;
	}
	if(g_i2c_master.protocol_id->major_version != received_id->major_version){
		printf("%d - CRITICAL WARNING - protocol major version not matching: v%d.%d (v%d.%d expected)\r\n", g_i2c_master.address_to_identify, received_id->major_version, received_id->minor_version, g_i2c_master.protocol_id->major_version, g_i2c_master.protocol_id->minor_version);
	}
	else if(g_i2c_master.protocol_id->minor_version != received_id->minor_version){
		printf("%d - WARNING - protocol minor version not matching: v%d.%d (v%d.%d expected)\r\n", g_i2c_master.address_to_identify, received_id->major_version, received_id->minor_version, g_i2c_master.protocol_id->major_version, g_i2c_master.protocol_id->minor_version);
	}
}

static void test_connection_callback_1(bool success, const sam5_i2c_master_com_job_t* job)
{
	if(!success){
		printf("Failed Sending\r\n");
		g_i2c_master.special_command_callback(false, NULL);
		return;
	}
	sam5_i2c_master_com_job_t com_job = {
		.direction = I2C_MASTER_DIR_SLAVE_TO_MASTER,
		.buffer = {0},
		.num_bytes_to_transfer = 1,
		.slave_address = job->slave_address,
		.command = 253,
		.period_ms = 0,
		.callback = test_connection_callback_2,
	};
	sam5_i2c_master_register_com_job(&com_job);
}

static void test_connection_callback_2(bool success, const sam5_i2c_master_com_job_t* job)
{
	if(!success || job->buffer[0] != test_value){
		printf("Failed Receiving\r\n");
		g_i2c_master.special_command_callback(false, NULL);
	}
	else {
		g_i2c_master.special_command_callback(true, job->buffer);
	}
}
