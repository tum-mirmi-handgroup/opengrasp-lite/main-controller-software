import math
from typing import Optional
import numpy as np

from decimal import localcontext, Decimal, ROUND_HALF_UP


def baud_from_freq(glck: int, t_rise: int, freq: int) -> int:
    total = ((glck - (freq * 10) - (t_rise * (freq // 100) * (glck // 10000) // 1000)) * 10 + 5 * freq) // (freq * 10)
    with localcontext() as ctx:
        ctx.rounding = ROUND_HALF_UP
        g = Decimal(glck)
        f = Decimal(freq)
        t = Decimal(t_rise)
        precise = (g - (f * Decimal(10)) - (t * (f / Decimal(100)) * (g / Decimal(10000)) / Decimal(1000))) / f
        assert total == precise.to_integral_value(), f"{total} != {precise.to_integral_value()} ({precise})"

    if total < 0:
        total = 0
    elif total > 255:
        total = 255

    if total % 3 == 1:
        total -= 1
    elif total % 3 == 2:
        total += 1

    return total // 3


def freq_from_bauds(glck: int, t_rise: int, baud: int, baud_low: Optional[int] = None):
    t_high = (baud + 5) / glck
    t_low = t_high if baud_low is None else (baud_low + 5) / glck
    return 1 / (t_high + t_low + t_rise * 1e-9)


if __name__ == '__main__':
    I2CM = int(1e6)  # Hz
    TRISE = int(60)  # ns
    for GLCK_SRC_MHZ in range(60, 121):
        GLCK_SRC = int(GLCK_SRC_MHZ * 1e6)  # Hz
        baud_by_divisor_and_error = {}
        for divisor in range(1, 11):
            GLCK = GLCK_SRC // divisor
            for baud in range(128):
                freq = freq_from_bauds(GLCK, TRISE, baud, baud * 2)
                error = abs(I2CM - freq)
                if divisor not in baud_by_divisor_and_error:
                    baud_by_divisor_and_error[divisor] = {}
                baud_by_divisor_and_error[divisor][error] = baud

        for divisor in baud_by_divisor_and_error.keys():
            error = min(baud_by_divisor_and_error[divisor].keys())
            baud = baud_by_divisor_and_error[divisor][error]
            GLCK = GLCK_SRC // divisor
            if GLCK_SRC_MHZ == 120:
                print(f"Div: {divisor:1d}  BAUD: {baud:2d}  BAUDLOW: {baud * 2:2d} -> "
                      f"{freq_from_bauds(GLCK, TRISE, baud, baud * 2):10.2f} Hz | {error / I2CM * 100:.3f} %",
                      end=" | ")
                print(f"{baud_from_freq(GLCK, TRISE, I2CM)}")

        for divisor in baud_by_divisor_and_error.keys():
            error = min(baud_by_divisor_and_error[divisor].keys())
            baud = baud_by_divisor_and_error[divisor][error]
            GLCK = GLCK_SRC // divisor
            res = baud_from_freq(GLCK, TRISE, I2CM)
