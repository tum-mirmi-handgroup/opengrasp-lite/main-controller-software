/*
* sam5_i2c_master.h
*
* Created: 29.11.2022 18:23:31
*  Author: Michael Ratzel
*/


#ifndef SAM5_I2C_MASTER_H_
#define SAM5_I2C_MASTER_H_

#include <atmel_start.h>
#include <Config/hpl_gclk_config.h>
#include "simple_state_machine.h"
#include "sam5_sw_crc8.h"
#include "hardware_defines.h"


/**
* \brief Definition of the events that can be dispatched to the I2C master state machine
*/
enum{
	I2C_MASTER_EVT_SIG_START = EVENT_BASE,	///< The I2C master starts a transmission
	I2C_MASTER_EVT_SIG_SB,					///< The I2C slave has send a byte
	I2C_MASTER_EVT_SIG_ACK,					///< The I2C master has send byte and received an ACK
	I2C_MASTER_EVT_SIG_NACK,				///< The I2C master has send byte and received a NACK
	I2C_MASTER_EVT_SIG_RECEIVE_TIMEOUT,		///< The I2C master has detected a timeout while receiving
	I2C_MASTER_EVT_SIG_ERROR,				///< The I2C master detected an error
};
enum{
	I2C_MASTER_DIR_MASTER_TO_SLAVE = 0,		///< Must be 0 as this is the I2C direction value for Master to Slave
	I2C_MASTER_DIR_SLAVE_TO_MASTER = 1,		///< Must be 1 as this is the I2C direction value for Slave to Master
	I2C_MASTER_DIR_INACTIVE = 2,
};

#define I2C_MASTER_MAX_NUMBER_JOBS 255

struct sam5_i2c_master_com_job_t;

typedef struct sam5_i2c_master_com_job_t{
	uint8_t buffer[SAM5_CRC8_MAX_DATA_LENGTH + 1]; // Make space for the CRC uint8_t
	uint8_t direction;
	uint8_t num_bytes_to_transfer;
	uint8_t slave_address;
	uint8_t command;
	void (*callback)(bool, const struct sam5_i2c_master_com_job_t*);
	
	uint32_t period_ms;
	uint32_t last_executed;
} sam5_i2c_master_com_job_t;

typedef struct {
	uint8_t type;
	uint8_t major_version;
	uint8_t minor_version;
} sam5_i2c_protocol_identification_t;

#define I2C_SLAVE_PROTOCOL_TYPE_EXAMPLE	1
#define I2C_SLAVE_PROTOCOL_TYPE_MOTOR	2
#define I2C_SLAVE_PROTOCOL_TYPE_EMG		3

/**
* \brief Initializes the I2C master peripherals and state machine
*/
void sam5_i2c_master_init();

void sam5_i2c_master_loop();

uint8_t sam5_i2c_master_is_available();

uint8_t sam5_i2c_master_register_com_job(const sam5_i2c_master_com_job_t *job);

// Special commands
uint8_t sam5_i2c_master_check_protocol_identification(uint8_t i2c_address, const sam5_i2c_protocol_identification_t* identification);
uint8_t sam5_i2c_master_change_slave_address(uint8_t old_address, uint8_t new_address, void (*const special_command_callback)(bool, const uint8_t*));
uint8_t sam5_i2c_master_test_connection(uint8_t i2c_address, void (*const special_command_callback)(bool, const uint8_t*));
uint8_t sam5_i2c_master_query_num_commands(uint8_t i2c_address, void (*const special_command_callback)(bool, const uint8_t*));

#endif /* SAM5_I2C_MASTER_H_ */