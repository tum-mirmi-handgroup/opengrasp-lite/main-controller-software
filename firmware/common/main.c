#include <atmel_start.h>
#include <hal_delay.h>
#include <string.h>
#include <stdio.h>

#include "sam5_i2c_master.h"

#include "sam5_ws2812.h"
#include "main_control.h"
#include "job_system.h"

#if BAT_VLTG_AVAILABLE
#include "sam5_adc_batvlt.h"
#endif

#if BMP3_AVAILABLE
#include "bmp3.h"
#include "sam5_bmp3_wrapper.h"
#endif

#if FEEDBACK_MOTOR_AVAILABLE
#include "sam5_feedback_motor.h"
#endif

#if USART_CONSOLE_AVAILABLE
#include "sam5_usart_console.h"
#endif

#if MAIN_CONTROL_USART_AVAILABLE
#include "sam5_control_interface_usart.h"
#endif

static void status_led(uint8_t led, uint8_t level);

int main(void)
{
	/* Set floating point coprosessor access mode. */
	SCB->CPACR |= ((3UL << 10*2) | /* set CP10 Full Access */
	(3UL << 11*2) ); /* set CP11 Full Access */
	
	// Setup output of the GCLK 0 to PB14
	gpio_set_pin_direction(PIN_PB14, GPIO_DIRECTION_OUT);
	gpio_set_pin_function(PIN_PB14, PINMUX_PB14M_GCLK_IO0);
	
	// Setup output of the GCLK 1 (frequency of 1/100th of GCLK1) to PB15
	 gpio_set_pin_direction(PIN_PB15, GPIO_DIRECTION_OUT);
	 gpio_set_pin_function(PIN_PB15, PINMUX_PB15M_GCLK_IO1);
	
	//###########################################################
	// Clock setup and debug USART provided by ATMEL Start
	atmel_start_init();
	
	// Activate system timer: 24bit wide with 100 MHz -> ~ 167 ms maximum time span measureable
	SysTick->LOAD = 0x00FFFFFF;
	SysTick->VAL = 0;
	SysTick->CTRL = (1 << 2) | (0<<1) | (1 << 0); // CLKSOURCE -> Processor Clock | TICKINT -> No interrupt at overflow | ENABLE
	
	// Start status LEDs
	status_led(0, true);
	status_led(1, true);
	status_led(2, true);
	status_led(3, true);
	status_led(4, true);
	status_led(5, true);
	status_led(6, true);
	status_led(7, true);
	
	
	delay_ms(50);
	status_led(0, false);
	status_led(1, false);
	status_led(2, false);
	status_led(3, false);
	status_led(4, false);
	status_led(5, false);
	status_led(6, false);
	status_led(7, false);
	
	//###########################################################
	// Activate the USART console printing
	#if USART_CONSOLE_AVAILABLE
	sam5_usart_console_output_init();
	printf("\r\nInitializing\r\n");
	#endif
	status_led(0, true);

	
	//###########################################################
	// Force Feedback Sensors
	#if BMP3_AVAILABLE
	sam5_bmp3_wrapper_init();
	sam5_bmp3_wrapper_start();
	#endif
	status_led(1, true);
	
	//###########################################################
	// Init ADC for Battery voltage
	#if BAT_VLTG_AVAILABLE
    sam5_bat_vltg_conf();
	#endif
	
	//###########################################################
	// I2C communication to slave boards
	sam5_i2c_master_init();
	status_led(2, true);
	
	
	//###########################################################
	// Human Feedback system
	//Initialize feedback vibration motor
	#if FEEDBACK_MOTOR_AVAILABLE
	initialize_feedback_motor();
	#endif
	
	
	//###########################################################
	// Main Control
	main_control_init();
	status_led(3, true);
	
	
	//###########################################################
	// Start USART console
	#if USART_CONSOLE_AVAILABLE
	sam5_usart_console_input_init();
	sam5_usart_console_await_next_command();
	#endif
	status_led(4, true);
	
	
	//###########################################################
	// Start USART control interface
	#if MAIN_CONTROL_USART_AVAILABLE
	sam5_usart_control_interface_init();
	sam5_usart_control_interface_start();
	#endif
	status_led(5, true);
	
	//###########################################################
	// Job System
	job_system_init();
	status_led(7, true);
	
	//###########################################################
	// Wait for first full loop of the current measuremnt is finished
	#if BAT_VLTG_AVAILABLE
	while(!sam5_bat_vltg_is_measurement_valid());
	#else
	delay_ms(200);
	#endif
	//###########################################################
	// Main Loop
	status_led(0, false);
	status_led(1, false);
	status_led(2, false);
	status_led(3, false);
	status_led(4, false);
	status_led(5, false);
	status_led(6, false);
	status_led(7, false);
	while(1){
		main_control_loop_low_priority();
		#if BMP3_AVAILABLE
		sam5_bmp3_wrapper_loop();
		#endif
	}
}


static void status_led(uint8_t led, uint8_t level){
	switch(led){
		#ifdef STATUS_LED_PIN_0
		case 0: {
			gpio_set_pin_direction(STATUS_LED_PIN_0, GPIO_DIRECTION_OUT);
			gpio_set_pin_function(STATUS_LED_PIN_0, GPIO_PIN_FUNCTION_OFF);
			gpio_set_pin_level(STATUS_LED_PIN_0, level);
		} break;
		#endif
		#ifdef STATUS_LED_PIN_1
		case 1: {
			gpio_set_pin_direction(STATUS_LED_PIN_1, GPIO_DIRECTION_OUT);
			gpio_set_pin_function(STATUS_LED_PIN_1, GPIO_PIN_FUNCTION_OFF);
			gpio_set_pin_level(STATUS_LED_PIN_1, level);
		} break;
		#endif
		#ifdef STATUS_LED_PIN_2
		case 2: {
			gpio_set_pin_direction(STATUS_LED_PIN_2, GPIO_DIRECTION_OUT);
			gpio_set_pin_function(STATUS_LED_PIN_2, GPIO_PIN_FUNCTION_OFF);
			gpio_set_pin_level(STATUS_LED_PIN_2, level);
		} break;
		#endif
		#ifdef STATUS_LED_PIN_3
		case 3: {
			gpio_set_pin_direction(STATUS_LED_PIN_3, GPIO_DIRECTION_OUT);
			gpio_set_pin_function(STATUS_LED_PIN_3, GPIO_PIN_FUNCTION_OFF);
			gpio_set_pin_level(STATUS_LED_PIN_3, level);
		} break;
		#endif
		#ifdef STATUS_LED_PIN_4
		case 4: {
			gpio_set_pin_direction(STATUS_LED_PIN_4, GPIO_DIRECTION_OUT);
			gpio_set_pin_function(STATUS_LED_PIN_4, GPIO_PIN_FUNCTION_OFF);
			gpio_set_pin_level(STATUS_LED_PIN_4, level);
		} break;
		#endif
		#ifdef STATUS_LED_PIN_5
		case 5: {
			gpio_set_pin_direction(STATUS_LED_PIN_5, GPIO_DIRECTION_OUT);
			gpio_set_pin_function(STATUS_LED_PIN_5, GPIO_PIN_FUNCTION_OFF);
			gpio_set_pin_level(STATUS_LED_PIN_5, level);
		} break;
		#endif
		#ifdef STATUS_LED_PIN_6
		case 6: {
			gpio_set_pin_direction(STATUS_LED_PIN_6, GPIO_DIRECTION_OUT);
			gpio_set_pin_function(STATUS_LED_PIN_6, GPIO_PIN_FUNCTION_OFF);
			gpio_set_pin_level(STATUS_LED_PIN_6, level);
		} break;
		#endif
		#ifdef STATUS_LED_PIN_7
		case 7: {
			gpio_set_pin_direction(STATUS_LED_PIN_7, GPIO_DIRECTION_OUT);
			gpio_set_pin_function(STATUS_LED_PIN_7, GPIO_PIN_FUNCTION_OFF);
			gpio_set_pin_level(STATUS_LED_PIN_7, level);
		} break;
		#endif
		
	}
}

void debugHardfault(uint32_t *sp)
{
	uint32_t r0  = sp[0];
	uint32_t r1  = sp[1];
	uint32_t r2  = sp[2];
	uint32_t r3  = sp[3];
	uint32_t r12 = sp[4];
	uint32_t lr  = sp[5];
	uint32_t pc  = sp[6];
	uint32_t psr = sp[7];
	
	uint32_t cfsr = SCB->CFSR;
	uint32_t hfsr = SCB->HFSR;
	uint32_t mmfar = SCB->MMFAR;
	uint32_t bfar = SCB->BFAR;
	
	status_led(0, 1);
	status_led(1, 0);
	status_led(2, 1);
	status_led(3, 0);
	status_led(4, 1);
	status_led(5, 0);
	status_led(6, 1);
	status_led(7, 0);

	printf("HardFault Handler...\r\n");
	printf("System Control Block (SCB):\r\n");
	printf("\tCFSR   0x%08lx\r\n", cfsr);
	printf("\tHFSR   0x%08lx\r\n", hfsr);
	printf("\tMMFAR  0x%08lx\r\n", mmfar);
	printf("\tBFAR   0x%08lx\r\n", bfar);
	printf("Stack before error: 0x%08lx\r\n", (uint32_t)sp);
	printf("\tR0     0x%08lx\r\n", r0);
	printf("\tR1     0x%08lx\r\n", r1);
	printf("\tR2     0x%08lx\r\n", r2);
	printf("\tR3     0x%08lx\r\n", r3);
	printf("\tR12    0x%08lx\r\n", r12);
	printf("\tLR     0x%08lx\r\n", lr);
	printf("\tPC     0x%08lx\r\n", pc);
	printf("\tPSR    0x%08lx\r\n", psr);
	
	while(1);
}

__attribute__( (naked) )
void HardFault_Handler(void)
{
	__asm volatile
	(
	"tst lr, #4                                    \n"
	"ite eq                                        \n"
	"mrseq r0, msp                                 \n"
	"mrsne r0, psp                                 \n"
	"ldr r1, debugHardfault_address                \n"
	"bx r1                                         \n"
	"debugHardfault_address: .word debugHardfault  \n"
	);
}

