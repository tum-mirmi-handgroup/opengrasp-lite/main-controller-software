/*
 * hardware_defines.h
 *
 * Platform_SAMD51J19A
 *
 */ 


#ifndef HARDWARE_DEFINES_H_
#define HARDWARE_DEFINES_H_

#include "interrupt_levels.h"
#include "motor_protocol.h"
#include <atmel_start.h>
#include <peripheral_clk_config.h>

//###############################################################################
//*******************************************************************************
#define MAIN_CONTROL_USART_AVAILABLE			1
#define MAIN_CONTROL_EMG_AVAILABLE				0
#define MAIN_CONTROL_NUM_MOTOR_CONTROLLER		2

#define BAT_VLTG_AVAILABLE						1

#define FEEDBACK_MOTOR_AVAILABLE				1
#define USART_CONSOLE_AVAILABLE					1
#define BMP3_AVAILABLE							1
//###############################################################################



//*******************************************************************************
// Feedback setup
#define FEEDBACK_MOTOR_PIN						PIN_PA06


//*******************************************************************************
// Status LED setup
#define STATUS_LED_PIN_0						PIN_PA08
#define STATUS_LED_PIN_1						PIN_PA09
#define STATUS_LED_PIN_2						PIN_PA10
#define STATUS_LED_PIN_3						PIN_PA11
#define STATUS_LED_PIN_4						PIN_PB10
#define STATUS_LED_PIN_5						PIN_PB11
#define STATUS_LED_PIN_6						PIN_PB12
#define STATUS_LED_PIN_7						PIN_PB13


//*******************************************************************************
// Motor configuration setup
#define MAIN_CONTROL_THUMB_MOTOR_CONTROLLER		1									///< Start counting at 1 to match the prints on the PCBs. 0 -> not available
#define MAIN_CONTROL_THUMB_MOTOR_INDEX			1									///< Start counting at 1 to match the prints on the PCBs. 0 -> not available
#define MAIN_CONTROL_THUMB_OPEN_AT_POS			0									///< Set to 1 if open position is at positive end of the movement range
#define MAIN_CONTROL_THUMB_CALIB_GROUP			0									///< Fingers will be opened and then calibrated one in order of the calibration group
#define MAIN_CONTROL_THUMB_GEAR_RATIO			MOTOR_GEAR_RATIO_100									

#define MAIN_CONTROL_INDEX_MOTOR_CONTROLLER		1									///< Start counting at 1 to match the prints on the PCBs. 0 -> not available
#define MAIN_CONTROL_INDEX_MOTOR_INDEX			3									///< Start counting at 1 to match the prints on the PCBs. 0 -> not available
#define MAIN_CONTROL_INDEX_OPEN_AT_POS			0									///< Set to 1 if open position is at positive end of the movement range
#define MAIN_CONTROL_INDEX_CALIB_GROUP			1									///< Fingers will be opened and then calibrated one in order of the calibration group
#define MAIN_CONTROL_INDEX_GEAR_RATIO			MOTOR_GEAR_RATIO_100

#define MAIN_CONTROL_MIDDLE_MOTOR_CONTROLLER	2									///< Start counting at 1 to match the prints on the PCBs. 0 -> not available
#define MAIN_CONTROL_MIDDLE_MOTOR_INDEX			3									///< Start counting at 1 to match the prints on the PCBs. 0 -> not available
#define MAIN_CONTROL_MIDDLE_OPEN_AT_POS			0									///< Set to 1 if open position is at positive end of the movement range
#define MAIN_CONTROL_MIDDLE_CALIB_GROUP			2									///< Fingers will be opened and then calibrated one in order of the calibration group
#define MAIN_CONTROL_MIDDLE_GEAR_RATIO			MOTOR_GEAR_RATIO_100

#define MAIN_CONTROL_RING_MOTOR_CONTROLLER		2									///< Start counting at 1 to match the prints on the PCBs. 0 -> not available
#define MAIN_CONTROL_RING_MOTOR_INDEX			2									///< Start counting at 1 to match the prints on the PCBs. 0 -> not available
#define MAIN_CONTROL_RING_OPEN_AT_POS			0									///< Set to 1 if open position is at positive end of the movement range
#define MAIN_CONTROL_RING_CALIB_GROUP			1									///< Fingers will be opened and then calibrated one in order of the calibration group
#define MAIN_CONTROL_RING_GEAR_RATIO			MOTOR_GEAR_RATIO_75

#define MAIN_CONTROL_PINKY_MOTOR_CONTROLLER		2									///< Start counting at 1 to match the prints on the PCBs. 0 -> not available
#define MAIN_CONTROL_PINKY_MOTOR_INDEX			1									///< Start counting at 1 to match the prints on the PCBs. 0 -> not available
#define MAIN_CONTROL_PINKY_OPEN_AT_POS			0									///< Set to 1 if open position is at positive end of the movement range
#define MAIN_CONTROL_PINKY_CALIB_GROUP			2									///< Fingers will be opened and then calibrated one in order of the calibration group
#define MAIN_CONTROL_PINKY_GEAR_RATIO			MOTOR_GEAR_RATIO_75

#define MAIN_CONTROL_MISC_MOTOR_CONTROLLER		1									///< Start counting at 1 to match the prints on the PCBs. 0 -> not available
#define MAIN_CONTROL_MISC_MOTOR_INDEX			2									///< Start counting at 1 to match the prints on the PCBs. 0 -> not available
#define MAIN_CONTROL_MISC_OPEN_AT_POS			1									///< Set to 1 if open position is at positive end of the movement range
#define MAIN_CONTROL_MISC_CALIB_GROUP			1									///< Fingers will be opened and then calibrated one in order of the calibration group
#define MAIN_CONTROL_MISC_GEAR_RATIO			MOTOR_GEAR_RATIO_75


#define MAIN_CONTROL_MC_1_SWDIO					PIN_PB31							///< GPIO pin that is connected to the SWDIO of the motor controller 1
#define MAIN_CONTROL_MC_1_SWCLK					PIN_PB30							///< GPIO pin that is connected to the SWCLK of the motor controller 1
#define MAIN_CONTROL_MC_1_RESET					PIN_PB00							///< GPIO pin that is connected to the reset line of the motor controller 1

#define MAIN_CONTROL_MC_2_SWDIO					PIN_PB01							///< GPIO pin that is connected to the SWDIO of the motor controller 2
#define MAIN_CONTROL_MC_2_SWCLK					PIN_PB02							///< GPIO pin that is connected to the SWCLK of the motor controller 2
#define MAIN_CONTROL_MC_2_RESET					PIN_PB03							///< GPIO pin that is connected to the reset line of the motor controller 2

#define MAIN_CONTROL_EMG_RESET					PIN_PA06							///< GPIO pin that is connected to the reset line of the emg controller


//*******************************************************************************
// Usart console setup
#define USART_CONSOLE_SERCOM_UNIT				0									///< Selected SERCOM unit

#define USART_CONSOLE_RX_PIN					GPIO(GPIO_PORTA, 5)					///< GPIO pin to which the RX signal is connected
#define USART_CONSOLE_RX_PIN_FUNCTION			PINMUX_PA05D_SERCOM0_PAD1			///< Function value that connects the USART_CONSOLE_RX_PIN to PAD1 of the selected SERCOM unit
#define USART_CONSOLE_TX_PIN					GPIO(GPIO_PORTA, 4)					///< GPIO pin to which the TX signal is connected
#define USART_CONSOLE_TX_PIN_FUNCTION			PINMUX_PA04D_SERCOM0_PAD0			///< Function value that connects the USART_CONSOLE_TX_PIN to PAD0 of the selected SERCOM unit

#define USART_CONSOLE_GLCK_CORE_GEN				GCLK_PCHCTRL_GEN_GCLK0_Val			///< Use GCLK0 as source frequency generator


//*******************************************************************************
// I2C main bus communication setup 
#define I2C_MASTER_SERCOM_UNIT					3									///< Selected SERCOM unit

#define I2C_MASTER_SCL_PIN						GPIO(GPIO_PORTA, 23)				///< GPIO pin to which the SCL signal is connected
#define I2C_MASTER_SCL_PIN_FUNCTION				PINMUX_PA23C_SERCOM3_PAD1			///< Function value that connects the I2C_MASTER_SCL_PIN to PAD1 of the selected SERCOM unit
#define I2C_MASTER_SDA_PIN						GPIO(GPIO_PORTA, 22)				///< GPIO pin to which the SDA signal is connected
#define I2C_MASTER_SDA_PIN_FUNCTION				PINMUX_PA22C_SERCOM3_PAD0			///< Function value that connects the I2C_MASTER_SDA_PIN to PAD0 of the selected SERCOM unit

#define I2C_MASTER_GLCK_SLOW_GEN				GCLK_PCHCTRL_GEN_GCLK2_Val			///< Use GCLK2 as source frequency generator

//#define I2C_MASTER_BAUDRATE					1U * 1000U * 1000U					///< [Hz] Bus Baudrate Fast Mode Plus (FM+) -> 1 MHz (Currently not usable due to hardware issues)
#define I2C_MASTER_BAUDRATE						400U * 1000U						///< [Hz] Bus Baudrate Fast Mode FM -> 400 kHz
#define I2C_MASTER_TRISE						60									///< [ns] Time it takes for the lines to go back to high


//*******************************************************************************
// ADC setup
#define BAT_VLTG_ADC_REFERENCE					ADC_REFCTRL_REFSEL_INTVCC1_Val
#define ADC_RESOLUTION							4096.0
#define BAT_VLTG_DIVIDER_RATIO					12.19
#define BAT_VLTG_ZERO_OFFSET					-0.164
#define BAT_VLTG_PIN							GPIO(GPIO_PORTB, 6)					//Pin for the ADC input channel for Battery voltgae measurement
#define BAT_VLTG_ADCn							1									//Analog to digital conversion (ADC) unit used for Battery Voltage Measurement
#define BAT_VLTG_ADC_AINn						8									//ADC channel used for Battery voltage measurement

//*******************************************************************************
// BMP3 sensor and communication setup
#define BMP3_MULTIPLEXER_AVAILABLE				1
#define BMP3_CALIBRATION						0
#define BMP3_READOUT_FREQUENCY					BMP3_ODR_25_HZ

#define BMP3_I2C_DRIVER_SERCOM_UNIT				1									///< Selected SERCOM unit

#define BMP3_I2C_DRIVER_SCL_PIN					GPIO(GPIO_PORTA, 17)				///< GPIO pin to which the SCL signal is connected
#define BMP3_I2C_DRIVER_SCL_PIN_FUNCTION		PINMUX_PA17C_SERCOM1_PAD1			///< Function value that connects the BMP3_I2C_DRIVER_SCL_PIN to PAD1 of the selected SERCOM unit
#define BMP3_I2C_DRIVER_SDA_PIN					GPIO(GPIO_PORTA, 16)				///< GPIO pin to which the SDA signal is connected
#define BMP3_I2C_DRIVER_SDA_PIN_FUNCTION		PINMUX_PA16C_SERCOM1_PAD0			///< Function value that connects the BMP3_I2C_DRIVER_SDA_PIN to PAD0 of the selected SERCOM unit

#define BMP3_I2C_DRIVER_MUX_RESET_PIN			GPIO(GPIO_PORTA, 18)				///< Pin connected to the multiplexer reset input

#define BMP3_I2C_DRIVER_GLCK_SLOW_GEN			GCLK_PCHCTRL_GEN_GCLK2_Val			///< Use GCLK2 as source frequency generator
#define BMP3_I2C_DRIVER_TRISE					60									///< [ns] Time it takes for the lines to go back to high

#endif /* HARDWARE_DEFINES_H_ */
