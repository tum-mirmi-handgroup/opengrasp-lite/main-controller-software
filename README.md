# Cybathlon Main Firmware

This project contains the firmware for the main controller of the CyberTUM arm prosthesis. It is aimed to interact with different kinds of system like [motor controllers](https://gitlab.lrz.de/cybertum_arm_electronics/motor_controller_software), an EMG system, BMP390 based force sensors, or WS2812 LEDs.

The Microchip Studio Solution contains currently 2 different projects:
 - `Platform_SAMD51J19A` works on the Mainboards 1.0.0
 - `Platform_SAME54XPro` works on a SAME54 Xplained PRO Evaluation Kit